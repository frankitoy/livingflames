SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `living_assess`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_tokens`
--

CREATE TABLE IF NOT EXISTS `access_tokens` (
  `oauth_token` varchar(40) NOT NULL,
  `client_id` char(36) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `expires` int(11) NOT NULL,
  `scope` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`oauth_token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) unsigned DEFAULT NULL,
  `alias` varchar(255) DEFAULT '',
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `aros`
--

CREATE TABLE IF NOT EXISTS `aros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) unsigned DEFAULT NULL,
  `alias` varchar(255) DEFAULT '',
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `aros_acos`
--

CREATE TABLE IF NOT EXISTS `aros_acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) unsigned NOT NULL,
  `aco_id` int(10) unsigned NOT NULL,
  `_create` char(2) NOT NULL DEFAULT '0',
  `_read` char(2) NOT NULL DEFAULT '0',
  `_update` char(2) NOT NULL DEFAULT '0',
  `_delete` char(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `assessments`
--

CREATE TABLE IF NOT EXISTS `assessments` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `modules_id` int(10) NOT NULL,
  `question` text NOT NULL,
  `description` text,
  `is_active` smallint(1) NOT NULL,
  `dte_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `module_id` (`modules_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `assessments`
--

INSERT INTO `assessments` (`id`, `modules_id`, `question`, `description`, `is_active`, `dte_created`) VALUES
(1, 1, 'What is the minimum recommended clearance between the outer flue liner and any part of the roofline at 500mm below the cowl? ', NULL, 1, '2014-02-18 01:16:03'),
(2, 1, 'Why is an approved gas cowl necessary? ', NULL, 1, '2014-02-18 01:16:19'),
(3, 1, 'In what is flue draught normally measured? ', NULL, 1, '2014-02-18 01:16:29'),
(4, 1, 'What is the minimum recommended rise from the top of the appliance before any change of direction in flue? ', NULL, 1, '2014-02-18 01:16:39'),
(5, 1, 'Why does a flue need to be securely bracketed? ', NULL, 1, '2014-02-18 01:16:48'),
(6, 1, 'What is the minimum clearance between the outer flue liner and any combustible material? ', NULL, 1, '2014-02-18 01:16:58'),
(7, 1, 'Why is it recommended to keep bends to a minimum? ', NULL, 1, '2014-02-18 01:17:06'),
(8, 1, 'Name three checks that should be made prior to installing a gas appliance into an existing masonry chimney?', NULL, 1, '2014-02-18 01:17:20'),
(9, 1, 'If a fireplace is fitted with a flue damper, what action should be taken before installing a gas appliance? ', NULL, 1, '2014-02-18 01:17:31'),
(10, 1, 'What is the minimum rise for a twin skin flue above a flat roof or parapet wall? ', NULL, 1, '2014-02-18 01:17:40'),
(11, 2, 'What should the gap be between the top and bottom plates on the pilot head?', NULL, 1, '2014-08-27 00:25:59'),
(12, 2, 'At what height should the ignition rod be trimmed to in relation to the top plate of the pilot head on a natural gas manual control?', NULL, 1, '2014-08-27 00:26:47'),
(13, 2, 'How tight should the pilot injector be screwed into the pilot body?', NULL, 1, '2014-08-27 00:27:02'),
(14, 2, 'Where is the injector spring located?', NULL, 1, '2014-08-27 00:27:51'),
(15, 2, 'How should the air slide sit on an LPG pilot assembly?', NULL, 1, '2014-08-27 00:27:39'),
(16, 2, 'On an electronic control what is used to sense the flame?', NULL, 1, '2014-08-27 00:28:11'),
(17, 2, 'Once a pilot body and head have been selected what is the first check to be carried out?', NULL, 1, '2014-08-27 00:28:52'),
(18, 2, 'How do you access the pilot injector?', NULL, 1, '2014-08-27 00:28:52'),
(19, 2, 'What is the correct injector size for a natural gas pilot?', NULL, 1, '2014-08-27 00:29:24'),
(20, 2, 'The gap of the pilot head should be?', NULL, 1, '2014-08-27 00:29:24');

-- --------------------------------------------------------

--
-- Table structure for table `assessments_choices`
--

CREATE TABLE IF NOT EXISTS `assessments_choices` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `assessments_id` int(10) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `is_correct_answer` smallint(1) NOT NULL DEFAULT '0',
  `is_active` smallint(1) NOT NULL DEFAULT '0',
  `sort_order` smallint(1) NOT NULL DEFAULT '1',
  `dte_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dte_modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assessments_id` (`assessments_id`),
  KEY `assessments` (`assessments_id`,`is_correct_answer`,`is_active`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `assessments_choices`
--

INSERT INTO `assessments_choices` (`id`, `assessments_id`, `answer`, `is_correct_answer`, `is_active`, `sort_order`, `dte_created`, `dte_modified`) VALUES
(1, 1, '1.0 metre ', 0, 1, 1, '2014-02-18 01:18:39', NULL),
(2, 1, '2.5 metres ', 1, 1, 2, '2014-02-18 01:18:47', NULL),
(3, 1, '1.5 metres', 0, 1, 3, '2014-02-18 01:18:35', NULL),
(4, 2, 'To keep out the weather, birds and other foreign objects ', 1, 1, 1, '2014-02-18 01:20:03', NULL),
(5, 2, 'To restrict the flue draw ', 0, 1, 2, '2014-02-18 01:30:15', NULL),
(6, 2, 'To make the flue look nice ', 0, 1, 3, '2014-02-18 01:30:17', NULL),
(7, 3, 'Pounds per square inch ', 0, 1, 1, '2014-02-18 01:20:42', NULL),
(8, 3, 'Volume of air per metre ', 0, 1, 2, '2014-02-18 01:30:32', NULL),
(9, 3, 'Millimetres water gauge', 1, 1, 3, '2014-02-18 01:30:35', NULL),
(10, 4, '1200mm ', 1, 1, 1, '2014-02-18 01:21:22', NULL),
(11, 4, '0600mm', 0, 1, 2, '2014-02-18 01:30:46', NULL),
(12, 4, '1400mm', 0, 1, 3, '2014-02-18 01:30:49', NULL),
(13, 5, 'So there is no weight resting on the appliance ', 1, 1, 1, '2014-02-18 01:22:27', NULL),
(14, 5, 'To keep it straight ', 0, 1, 2, '2014-02-18 01:30:58', NULL),
(15, 5, 'So it looks professional ', 0, 1, 3, '2014-02-18 01:30:59', NULL),
(16, 6, '100mm ', 0, 1, 1, '2014-02-18 01:23:25', NULL),
(17, 6, '50mm', 0, 1, 2, '2014-02-18 01:31:25', NULL),
(18, 6, '25mm', 1, 1, 3, '2014-02-18 01:31:27', NULL),
(19, 7, 'To save on cost ', 0, 1, 1, '2014-02-18 01:24:08', NULL),
(20, 7, 'To create less resistance ', 1, 1, 2, '2014-02-18 01:31:38', NULL),
(21, 7, 'To save on installation time ', 0, 1, 3, '2014-02-18 01:31:41', NULL),
(22, 8, 'The ash pit is open for ventilation, \n\n    You can see up and out of the chimney \n\n    The chimney pot has been removed ', 0, 1, 1, '2014-02-18 01:27:37', NULL),
(23, 8, 'The fire bricks have been removed, \n\n    The hearth is fully supported \n\n     The appliance fits ', 0, 1, 2, '2014-02-18 01:31:54', NULL),
(24, 8, 'That it is constructed of non combustible materials,     The chimney has been swept and is clean,      The chimney is sound. ', 1, 1, 3, '2014-02-18 01:31:57', NULL),
(25, 9, 'That it is well lubricated and in good order ', 0, 1, 1, '2014-02-18 01:27:02', NULL),
(26, 9, 'That it is fixed in open operational position and cannot be closed ', 1, 1, 2, '2014-02-18 01:32:07', NULL),
(27, 9, ' Talk to the customer and tell them to close it when they need more heat ', 0, 1, 3, '2014-02-18 01:32:09', NULL),
(28, 10, '1 metre', 1, 1, 1, '2014-02-18 01:28:25', NULL),
(29, 10, '500mm', 0, 1, 2, '2014-02-18 01:32:16', NULL),
(30, 10, '1.5 metres ', 0, 1, 3, '2014-02-18 01:32:22', NULL),
(31, 11, '1mm.', 1, 1, 1, '2014-08-27 02:16:52', NULL),
(32, 11, '1.5 mm.', 0, 1, 2, '2014-08-27 00:33:31', NULL),
(33, 11, '2mm.', 0, 1, 3, '2014-08-27 00:35:02', NULL),
(34, 12, '5mm above.', 0, 1, 1, '2014-08-27 00:36:06', NULL),
(35, 12, '3m below.', 0, 1, 2, '2014-08-27 00:35:56', NULL),
(36, 12, '2mm above.', 1, 1, 3, '2014-08-27 02:16:59', NULL),
(37, 13, 'Tightened and the backed off two turns.', 0, 1, 1, '2014-08-27 01:37:57', NULL),
(38, 13, 'Tightened and then backed off a crack.', 1, 1, 2, '2014-08-27 02:17:07', NULL),
(39, 13, 'Tightened and then backed off one turn.', 0, 1, 3, '2014-08-27 01:38:58', NULL),
(40, 14, 'In the pilot head near the air slide.', 0, 1, 1, '2014-08-27 01:38:58', NULL),
(41, 0, 'In the pilot body around the injector.', 0, 1, 2, '2014-08-27 01:39:42', NULL),
(42, 14, 'Next to the pilot body on the mounting plate.', 0, 1, 3, '2014-08-27 01:39:42', NULL),
(43, 14, 'In the pilot body around the injector.', 1, 1, 2, '2014-08-27 02:17:12', NULL),
(45, 15, 'Open one eighth.', 0, 1, 1, '2014-08-27 01:42:02', NULL),
(46, 15, 'Fully open.', 1, 1, 2, '2014-08-27 02:17:36', NULL),
(47, 15, 'Open half way.', 0, 1, 3, '2014-08-27 01:42:42', NULL),
(48, 16, 'Thermocouple.', 0, 1, 1, '2014-08-27 01:42:42', NULL),
(49, 16, 'Magnetic strips.', 0, 1, 2, '2014-08-27 01:43:19', NULL),
(50, 16, 'Flame sensor rod.', 1, 1, 3, '2014-08-27 02:17:43', NULL),
(51, 17, 'Check for any physical damage.', 1, 1, 1, '2014-08-27 02:17:46', NULL),
(52, 17, 'Check it fits in the mounting bracket.', 0, 1, 2, '2014-08-27 01:54:19', NULL),
(53, 17, 'Check the air slide is adjusted.', 0, 1, 3, '2014-08-27 01:56:01', NULL),
(54, 18, 'By removing the cap at the base of the pilot body.', 1, 1, 1, '2014-08-27 02:17:53', NULL),
(55, 18, 'By pulling off the pilot head.', 0, 1, 2, '2014-08-27 01:58:27', NULL),
(56, 18, 'By undoing the pilot pipe.', 0, 1, 3, '2014-08-27 01:58:27', NULL),
(57, 19, '0.45mm.', 0, 1, 1, '2014-08-27 01:59:04', NULL),
(58, 19, '0.25mm.', 0, 1, 2, '2014-08-27 01:59:04', NULL),
(59, 19, '0.35mm.', 1, 1, 3, '2014-08-27 02:18:03', NULL),
(60, 20, 'A level gap.', 1, 1, 1, '2014-08-27 02:18:07', NULL),
(61, 20, 'Wider on the right hand side.', 0, 1, 2, '2014-08-27 02:00:37', NULL),
(62, 20, 'Narrower of the right hand side.', 0, 1, 3, '2014-08-27 02:00:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_codes`
--

CREATE TABLE IF NOT EXISTS `auth_codes` (
  `code` varchar(40) NOT NULL,
  `client_id` char(36) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `redirect_uri` varchar(200) NOT NULL,
  `expires` int(11) NOT NULL,
  `scope` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cake_sessions`
--

CREATE TABLE IF NOT EXISTS `cake_sessions` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `data` text,
  `expires` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cake_sessions`
--

INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES
('1f519c6095207ce748ca00cac724a664', 'Config|a:3:{s:9:"userAgent";s:32:"4b2c3ed8490a18b3703bcd30b8b31f4a";s:4:"time";i:1409135037;s:9:"countdown";i:10;}_Token|a:5:{s:3:"key";s:40:"898f3f1b85a5b7c21da5e1ceef5719f6fe05fea1";s:18:"allowedControllers";a:0:{}s:14:"allowedActions";a:0:{}s:14:"unlockedFields";a:0:{}s:10:"csrfTokens";a:1:{s:40:"898f3f1b85a5b7c21da5e1ceef5719f6fe05fea1";i:1409122437;}}', 1409120637),
('2e73a34ba553ed8ea8c63f8e71ec1972', 'Config|a:3:{s:9:"userAgent";s:32:"8523b281006cece14384de8236105340";s:4:"time";i:1409115722;s:9:"countdown";i:10;}', 1409101322),
('6749ed8aee1e21e85cccded3e4f74397', 'Config|a:3:{s:9:"userAgent";s:32:"67ef584a1c2e0821bc2c94fa28baa746";s:4:"time";i:1409121636;s:9:"countdown";i:10;}', 1409107236),
('6974e3aa6dca96e914b91ea7224dd7a4', 'Config|a:3:{s:9:"userAgent";s:32:"8523b281006cece14384de8236105340";s:4:"time";i:1409177510;s:9:"countdown";i:10;}', 1409163110),
('6dfc7effb0180ce6e26db2f560486a58', 'Config|a:3:{s:9:"userAgent";s:32:"e674a892f8fb4779a09f5a8691ec484d";s:4:"time";i:1409107502;s:9:"countdown";i:10;}', 1409093102),
('9d0096c9550613098f26b96f133c3432', 'Config|a:3:{s:9:"userAgent";s:32:"bed4fe248fe367a719f75b6ad6aec285";s:4:"time";i:1409122067;s:9:"countdown";i:10;}_Token|a:5:{s:3:"key";s:40:"12445bf9e0088c5a93a6ec658713923d9512cab6";s:18:"allowedControllers";a:0:{}s:14:"allowedActions";a:0:{}s:14:"unlockedFields";a:0:{}s:10:"csrfTokens";a:5:{s:40:"d94a278cae465c921afc7b1082a0ef34e5cc82a2";i:1409107702;s:40:"d8eaf21c81491aeb9412260a80f10d3089c6568f";i:1409107741;s:40:"4c953d4c502badad455f55e4dfe2b3bcb9eb4eff";i:1409109463;s:40:"f4e12b57a757d0e68282ea77cc8a4b3d4acac70f";i:1409109466;s:40:"12445bf9e0088c5a93a6ec658713923d9512cab6";i:1409109467;}}Auth|a:1:{s:4:"User";a:32:{s:2:"id";s:36:"52d91359-efdc-4568-b3ae-09667a3d4a97";s:10:"modules_id";s:1:"1";s:8:"username";s:5:"admin";s:4:"slug";s:5:"admin";s:14:"password_token";s:40:"f696bfa15b615c64562afa34400f326de0b905c4";s:10:"first_name";s:5:"Admin";s:9:"last_name";s:5:"Admin";s:12:"company_name";N;s:7:"address";N;s:19:"registration_number";s:0:"";s:6:"mobile";s:10:"0211234567";s:5:"phone";s:7:"1234567";s:5:"email";s:20:"gn@livingflame.co.nz";s:14:"email_verified";b:1;s:11:"email_token";N;s:19:"email_token_expires";N;s:3:"tos";b:1;s:6:"active";b:1;s:8:"has_paid";s:1:"0";s:12:"has_surveyed";s:1:"0";s:20:"has_generated_result";s:1:"0";s:10:"last_login";s:19:"2014-08-27 09:19:05";s:11:"last_action";N;s:8:"is_admin";b:1;s:18:"assessment_attempt";s:1:"0";s:16:"assessment_score";s:1:"0";s:20:"assessment_link_sent";s:1:"0";s:4:"role";s:15:"user_registered";s:9:"date_paid";N;s:7:"created";s:19:"2014-01-18 00:26:17";s:8:"modified";s:19:"2014-08-27 09:19:05";s:7:"Modules";a:6:{s:2:"id";s:1:"1";s:4:"name";s:9:"Module 16";s:4:"slug";s:23:"flueing-and-downdraught";s:11:"description";s:23:"Flueing and Downdraught";s:9:"is_active";s:1:"1";s:11:"dte_created";s:19:"2014-01-17 13:15:49";}}}', 1409107667),
('a892e6f1974b2d43885ecf84f6fc5089', 'Config|a:3:{s:9:"userAgent";s:32:"67ef584a1c2e0821bc2c94fa28baa746";s:4:"time";i:1409121637;s:9:"countdown";i:10;}', 1409107237),
('c003395bd5b3c951b3ac6cd739dc5b57', 'Config|a:3:{s:9:"userAgent";s:32:"0be4201a2a4d9409f33d8b68e6152e4e";s:4:"time";i:1409163490;s:9:"countdown";i:10;}', 1409149090),
('c65f8d629cd8d6a2de8b728db51d18e5', 'Config|a:3:{s:9:"userAgent";s:32:"bed4fe248fe367a719f75b6ad6aec285";s:4:"time";i:1409124982;s:9:"countdown";i:10;}_Token|a:5:{s:3:"key";s:40:"afbe33b2ebec8cc7e1bcb6cb25204e7ca47851b8";s:18:"allowedControllers";a:0:{}s:14:"allowedActions";a:0:{}s:14:"unlockedFields";a:0:{}s:10:"csrfTokens";a:14:{s:40:"fb10748d8278a596de97071f063f19371e587058";i:1409111882;s:40:"a0f7e52bddfe2601d6071f4d412c80e1ef04f784";i:1409111882;s:40:"0f8dd6ea61479a522d2675b167d9e9f3eab10ded";i:1409111886;s:40:"59aaba51e1280b77088c718d874bcc7c7a5971a5";i:1409111889;s:40:"fb45c6c7a9fad243bfa18cf91d076e7dcf5a766f";i:1409111891;s:40:"324114fb9a7aca2700ad13352cc23f7fafec2182";i:1409112263;s:40:"6735850c3543c03bb001b36c8402b6ac9befa0c3";i:1409112266;s:40:"bf49cdd4c4e5029e0d104dde942e465168135c94";i:1409112268;s:40:"89cd2977b8aa29eafcf1d382633fb44292b55e67";i:1409112270;s:40:"58b8b94fbb293e497b598758d11619ec5e9acbff";i:1409112272;s:40:"cf7cc8f5068d9bccf2b361499f1b4d4bb70da630";i:1409112274;s:40:"1447cbd929e2abbff94922eee484c4376c308510";i:1409112378;s:40:"979cc15c4d0b4cbef60e4324e50e9e54f7dbe5de";i:1409112381;s:40:"afbe33b2ebec8cc7e1bcb6cb25204e7ca47851b8";i:1409112382;}}Auth|a:1:{s:4:"User";a:32:{s:2:"id";s:36:"52d91359-efdc-4568-b3ae-09667a3d4a97";s:10:"modules_id";s:1:"1";s:8:"username";s:5:"admin";s:4:"slug";s:5:"admin";s:14:"password_token";s:40:"f696bfa15b615c64562afa34400f326de0b905c4";s:10:"first_name";s:5:"Admin";s:9:"last_name";s:5:"Admin";s:12:"company_name";N;s:7:"address";N;s:19:"registration_number";s:0:"";s:6:"mobile";s:10:"0211234567";s:5:"phone";s:7:"1234567";s:5:"email";s:20:"gn@livingflame.co.nz";s:14:"email_verified";b:1;s:11:"email_token";N;s:19:"email_token_expires";N;s:3:"tos";b:1;s:6:"active";b:1;s:8:"has_paid";s:1:"0";s:12:"has_surveyed";s:1:"0";s:20:"has_generated_result";s:1:"0";s:10:"last_login";s:19:"2014-08-27 15:24:33";s:11:"last_action";N;s:8:"is_admin";b:1;s:18:"assessment_attempt";s:1:"0";s:16:"assessment_score";s:1:"0";s:20:"assessment_link_sent";s:1:"0";s:4:"role";s:15:"user_registered";s:9:"date_paid";N;s:7:"created";s:19:"2014-01-18 00:26:17";s:8:"modified";s:19:"2014-08-27 15:24:33";s:7:"Modules";a:6:{s:2:"id";s:1:"1";s:4:"name";s:9:"Module 16";s:4:"slug";s:23:"flueing-and-downdraught";s:11:"description";s:23:"Flueing and Downdraught";s:9:"is_active";s:1:"1";s:11:"dte_created";s:19:"2014-08-27 15:20:11";}}}', 1409110582),
('fda8acbf7cf5134be42392b58c73d7fb', 'Config|a:3:{s:9:"userAgent";s:32:"4b2c3ed8490a18b3703bcd30b8b31f4a";s:4:"time";i:1409134904;s:9:"countdown";i:10;}', 1409120504);

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `client_id` char(20) NOT NULL,
  `client_secret` char(40) NOT NULL,
  `redirect_uri` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE IF NOT EXISTS `emails` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `emails_type_id` smallint(1) NOT NULL DEFAULT '1',
  `from_email` varchar(100) NOT NULL,
  `to_email` varchar(100) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `dte_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `emails_type_id`, `from_email`, `to_email`, `subject`, `content`, `dte_created`) VALUES
(1, 1, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame on-line course. We hope you enjoy the course and learn from its content.</p>\n\n</body>\n</html>\n', '2014-02-17 21:25:26'),
(2, 2, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame on-line course. We hope you enjoy the course and learn from its content. Please click the link to start the assessment <a href="http://assessment.livingflame.co.nz/assessments?token=53027e45-2b98-4c2c-87d5-6fc567061d02" target="_blank">http://assessment.livingflame.co.nz/assessments?token=53027e45-2b98-4c2c-87d5-6fc567061d02</a>.</p>\n\n</body>\n</html>\n', '2014-02-17 22:12:28'),
(3, 1, 'gn@livingflame.co.nz', 'geetz13@msn.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame on-line course. We hope you enjoy the course and learn from its content.</p>\n\n</body>\n</html>\n', '2014-02-18 00:22:06'),
(4, 1, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame on-line course. We hope you enjoy the course and learn from its content.</p>\n\n</body>\n</html>\n', '2014-02-18 01:11:53'),
(5, 2, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame on-line course. We hope you enjoy the course and learn from its content. Please click the link to start the assessment <a href="http://assessment.livingflame.co.nz/assessments?token=5302b359-8254-4f48-93cf-65ce67061d02" target="_blank">http://assessment.livingflame.co.nz/assessments?token=5302b359-8254-4f48-93cf-65ce67061d02</a>.</p>\n\n</body>\n</html>\n', '2014-02-18 01:12:52'),
(6, 1, 'gn@livingflame.co.nz', 'sweetgeetos@yahoo.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame on-line course. We hope you enjoy the course and learn from its content.</p>\n\n</body>\n</html>\n', '2014-02-18 01:22:27'),
(7, 1, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame on-line course. We hope you enjoy the course and learn from its content.</p>\n\n</body>\n</html>\n', '2014-02-18 01:45:50'),
(8, 2, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame on-line course. We hope you enjoy the course and learn from its content. Please click to start the assessment <br><a href="http://assessment.livingflame.co.nz/assessments?token=5302bb4e-13bc-4cf7-8eeb-167d67061d02" target="_blank">http://assessment.livingflame.co.nz/assessments?token=5302bb4e-13bc-4cf7-8eeb-167d67061d02</a>.</p>\n\n</body>\n</html>\n', '2014-02-18 01:46:01'),
(9, 1, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame on-line course. We hope you enjoy the course and learn from its content.</p>\n\n</body>\n</html>\n', '2014-02-18 02:02:39'),
(10, 2, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame on-line course. We hope you enjoy the course and learn from its content. Please click to start the assessment <br><a href="http://assessment.livingflame.co.nz/assessments?token=5302bf3f-7678-4c0d-b29d-2db967061d02" target="_blank">http://assessment.livingflame.co.nz/assessments?token=5302bf3f-7678-4c0d-b29d-2db967061d02</a>.</p>\n\n</body>\n</html>\n', '2014-02-18 02:02:55'),
(11, 2, 'gn@livingflame.co.nz', 'sweetgeetos@yahoo.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame on-line course. We hope you enjoy the course and learn from its content. Please click to start the assessment <br><a href="http://assessment.livingflame.co.nz/assessments?token=5302b5d3-3f9c-4c29-a720-72e967061d02" target="_blank">http://assessment.livingflame.co.nz/assessments?token=5302b5d3-3f9c-4c29-a720-72e967061d02</a>.</p>\n\n</body>\n</html>\n', '2014-02-18 02:05:25'),
(12, 3, 'gn@livingflame.co.nz', 'gn@livingflame.co.nz', 'New survey entered.', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	Geet&nbsp;Phanse has entered the survey. You can review the survey <a href="/surveys" target="_blank">here</a></body>\n</html>\n', '2014-02-18 03:04:06'),
(13, 1, 'gn@livingflame.co.nz', 'geet@agm.co.nz', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame on-line course. We hope you enjoy the course and learn from its content.</p>\n\n</body>\n</html>\n', '2014-02-18 03:26:29'),
(14, 1, 'gn@livingflame.co.nz', 'sweetgeetos@yahoo.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame on-line course. We hope you enjoy the course and learn from its content.</p>\n\n</body>\n</html>\n', '2014-02-19 08:00:15'),
(15, 1, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. We will be in touch with you shortly.</p>\n<p>Team,<br />\n  Living Flame</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-02-19 08:07:19'),
(16, 1, 'gn@livingflame.co.nz', 'sweetgeetos@yahoo.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. We will be in touch with you shortly.</p>\n<p>Team,<br />\n  Living Flame</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-02-19 08:07:25'),
(17, 1, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. <br />\n  We will be in touch with you shortly.<br />\n  <br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-02-19 08:09:49'),
(18, 1, 'gn@livingflame.co.nz', 'sweeetgeetos@yahoo.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. <br />\n  We will be in touch with you shortly.<br />\n  <br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-02-19 08:10:03'),
(19, 1, 'gn@livingflame.co.nz', 'sweetgeetos@yahoo.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. <br />\n  We will be in touch with you shortly.<br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-02-19 08:11:48'),
(20, 1, 'gn@livingflame.co.nz', 'sweetgeetos@yahoo.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. <br />\n  We will be in touch with you shortly.<br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-02-19 08:16:28'),
(21, 1, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. <br />\n  We will be in touch with you shortly.<br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-02-19 08:16:46'),
(22, 1, 'gn@livingflame.co.nz', 'sweetgeetos@yahoo.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. <br />\n  We will be in touch with you shortly.<br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-02-19 08:17:42'),
(23, 2, 'gn@livingflame.co.nz', 'sweetgeetos@yahoo.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame on-line course. We hope you enjoy the course and learn from its content. Please click to start the assessment <br><a href="http://assessment.livingflame.co.nz/assessments?token=530468a6-3868-4fbd-9114-1d7067061d02" target="_blank">http://assessment.livingflame.co.nz/assessments?token=530468a6-3868-4fbd-9114-1d7067061d02</a>.</p>\n\n</body>\n</html>\n', '2014-02-19 08:19:37'),
(24, 2, 'gn@livingflame.co.nz', 'sweetgeetos@yahoo.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame Online Course. We hope you enjoy the course and learn from its content. <br />\n  Please click on the link to start the assessment <a href="http://assessment.livingflame.co.nz/assessments?token=5304685c-c978-4fb1-87ab-1ef967061d02" target="_blank">http://assessment.livingflame.co.nz/assessments?token=5304685c-c978-4fb1-87ab-1ef967061d02</a> \n  <br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p>\n\n</body>\n</html>\n', '2014-02-19 08:25:51'),
(25, 2, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame Online Course. We hope you enjoy the course and learn from its content. <br />\n  Please click on the link to start the assessment <a href="http://assessment.livingflame.co.nz/assessments?token=53046637-d2f8-486a-8986-1a1967061d02" target="_blank">http://assessment.livingflame.co.nz/assessments?token=53046637-d2f8-486a-8986-1a1967061d02</a> \n  <br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p>\n\n</body>\n</html>\n', '2014-02-19 08:26:55'),
(26, 1, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. <br />\n  We will be in touch with you shortly.<br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-02-19 09:09:15'),
(27, 2, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame Online Course. We hope you enjoy the course and learn from its content. <br />\n  Please click on the link to start the assessment <a href="http://assessment.livingflame.co.nz/assessments?token=530474bb-c968-4a32-9ab7-3e1f67061d02" target="_blank">http://assessment.livingflame.co.nz/assessments?token=530474bb-c968-4a32-9ab7-3e1f67061d02</a> \n  <br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p>\n\n</body>\n</html>\n', '2014-02-19 09:10:03'),
(28, 2, 'gn@livingflame.co.nz', 'sweetgeetos@yahoo.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame Online Course. We hope you enjoy the course and learn from its content. <br />\n  Please click on the link to start the assessment <a href="http://assessment.livingflame.co.nz/assessments?token=53046744-47e4-42d4-a734-1cb267061d02" target="_blank">http://assessment.livingflame.co.nz/assessments?token=53046744-47e4-42d4-a734-1cb267061d02</a> \n  <br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p>\n\n</body>\n</html>\n', '2014-02-19 09:33:18'),
(29, 2, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame Online Course. We hope you enjoy the course and learn from its content. <br />\n  Please click on the link to start the assessment <a href="http://assessment.livingflame.co.nz/assessments?token=5304686e-1118-4cfc-920f-1cf867061d02" target="_blank">http://assessment.livingflame.co.nz/assessments?token=5304686e-1118-4cfc-920f-1cf867061d02</a> \n  <br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p>\n\n</body>\n</html>\n', '2014-02-19 09:41:19'),
(30, 3, 'gn@livingflame.co.nz', 'gn@livingflame.co.nz', 'New survey entered.', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	Geet&nbsp;P has entered the survey. You can review the survey <a href="/surveys" target="_blank">here</a></body>\n</html>\n', '2014-02-19 10:06:21'),
(31, 2, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame Online Course. We hope you enjoy the course and learn from its content. <br />\n  Please click on the link to start the assessment <a href="http://assessment.livingflame.co.nz/assessments?token=530466cd-3de8-458f-b695-1af667061d02" target="_blank">http://assessment.livingflame.co.nz/assessments?token=530466cd-3de8-458f-b695-1af667061d02</a> \n  <br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p>\n\n</body>\n</html>\n', '2014-02-19 10:13:57'),
(32, 1, 'gn@livingflame.co.nz', 'sd@livingflame.co.nz', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. <br />\n  We will be in touch with you shortly.<br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-02-20 20:34:57'),
(33, 1, 'gn@livingflame.co.nz', 'sweetgeetos@yahoo.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. <br />\n  We will be in touch with you shortly.<br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-02-20 20:35:01');
INSERT INTO `emails` (`id`, `emails_type_id`, `from_email`, `to_email`, `subject`, `content`, `dte_created`) VALUES
(34, 2, 'gn@livingflame.co.nz', 'sweetgeetos@yahoo.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame Online Course. We hope you enjoy the course and learn from its content. <br />\n  Please click on the link to start the assessment <a href="http://assessment.livingflame.co.nz/assessments?token=530666f4-d880-4230-bc02-4ecd67061d02" target="_blank">http://assessment.livingflame.co.nz/assessments?token=530666f4-d880-4230-bc02-4ecd67061d02</a> \n  <br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p>\n\n</body>\n</html>\n', '2014-02-20 20:37:50'),
(35, 2, 'gn@livingflame.co.nz', 'sd@livingflame.co.nz', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame Online Course. We hope you enjoy the course and learn from its content. <br />\n  Please click on the link to start the assessment <a href="http://assessment.livingflame.co.nz/assessments?token=530666f1-e648-4d9e-98f4-4e5367061d02" target="_blank">http://assessment.livingflame.co.nz/assessments?token=530666f1-e648-4d9e-98f4-4e5367061d02</a> \n  <br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p>\n\n</body>\n</html>\n', '2014-02-20 20:37:53'),
(36, 3, 'gn@livingflame.co.nz', 'gn@livingflame.co.nz', 'New survey entered.', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	Sudhanshu&nbsp;Dandekar has entered the survey. You can review the survey <a href="http://assessment.livingflame.co.nz/surveys" target="_blank">here</a></body>\n</html>\n', '2014-02-20 20:39:58'),
(37, 2, 'gn@livingflame.co.nz', 'geetz13@msn.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame Online Course. We hope you enjoy the course and learn from its content. <br />\n  Please click on the link to start the assessment <a href="http://assessment.livingflame.co.nz/assessments?token=5302a7ae-1a14-4357-9a72-210b67061d02" target="_blank">http://assessment.livingflame.co.nz/assessments?token=5302a7ae-1a14-4357-9a72-210b67061d02</a> \n  <br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p>\n\n</body>\n</html>\n', '2014-02-20 20:41:18'),
(38, 1, 'gn@livingflame.co.nz', 'sdandekar@livingflame.co.nz', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. <br />\n  We will be in touch with you shortly.<br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-02-20 20:42:04'),
(39, 2, 'gn@livingflame.co.nz', 'sdandekar@livingflame.co.nz', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame Online Course. We hope you enjoy the course and learn from its content. <br />\n  Please click on the link to start the assessment <a href="http://assessment.livingflame.co.nz/assessments?token=5306689c-fce4-4007-9bf2-551567061d02" target="_blank">http://assessment.livingflame.co.nz/assessments?token=5306689c-fce4-4007-9bf2-551567061d02</a> \n  <br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p>\n\n</body>\n</html>\n', '2014-02-20 20:42:14'),
(40, 1, 'gn@livingflame.co.nz', 'gn@livingflame.co.nz', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. <br />\n  We will be in touch with you shortly.<br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-02-20 20:56:46'),
(41, 2, 'gn@livingflame.co.nz', 'gn@livingflame.co.nz', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame Online Course. We hope you enjoy the course and learn from its content. <br />\n  Please click on the link to start the assessment <a href="http://assessment.livingflame.co.nz/assessments?token=53066c0e-4efc-4dcd-adee-5f1467061d02" target="_blank">http://assessment.livingflame.co.nz/assessments?token=53066c0e-4efc-4dcd-adee-5f1467061d02</a> \n  <br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p>\n\n</body>\n</html>\n', '2014-02-20 21:00:15'),
(42, 1, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. <br />\n  We will be in touch with you shortly.<br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-02-24 10:02:05'),
(43, 2, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame Online Course. We hope you enjoy the course and learn from its content. <br />\n  Please click on the link to start the assessment <a href="http://assessment.livingflame.co.nz/assessments?token=530b189d-c4c4-4f85-85e8-0c1567061d02" target="_blank">http://assessment.livingflame.co.nz/assessments?token=530b189d-c4c4-4f85-85e8-0c1567061d02</a> \n  <br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p>\n\n</body>\n</html>\n', '2014-02-24 10:02:26'),
(44, 3, 'gn@livingflame.co.nz', 'gn@livingflame.co.nz', 'New survey entered.', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	Franklin&nbsp;Marcelo has entered the survey. You can review the survey <a href="http://assessment.livingflame.co.nz/surveys" target="_blank">here</a></body>\n</html>\n', '2014-02-24 10:07:15'),
(45, 1, 'gn@livingflame.co.nz', 'geetz13@msn.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. <br />\n  We will be in touch with you shortly.<br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-02-27 23:40:32'),
(46, 2, 'gn@livingflame.co.nz', 'geetz13@msn.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame Online Course. We hope you enjoy the course and learn from its content. <br />\n  Please click on the link to start the assessment <a href="http://assessment.livingflame.co.nz/assessments?token=530fccf0-53d0-4c4a-ba91-164467061d02" target="_blank">http://assessment.livingflame.co.nz/assessments?token=530fccf0-53d0-4c4a-ba91-164467061d02</a> \n  <br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p>\n\n</body>\n</html>\n', '2014-02-27 23:43:31'),
(47, 1, 'gn@livingflame.co.nz', 'sd@livingflame.co.nz', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. <br />\n  We will be in touch with you shortly.<br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-03-26 20:38:07'),
(48, 1, 'gn@livingflame.co.nz', 'SWEETGEETOS@YAHOO.COM', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. <br />\n  We will be in touch with you shortly.<br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-03-27 23:01:18'),
(49, 1, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. </p>\n\n<p>Please call us on 09 6221148 with your credit card details OR pay directly into our bank account with your name as a reference, and online training under particulars .</p>\n\n<p>Bank account number: 02 0240 0271125 00</p>\n\n<p>We will be in touch with you shortly.</p>\n<p> </p>\n\n<p>Team,</p>\n<p>Living Flame</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-04-01 21:00:11'),
(50, 1, 'gn@livingflame.co.nz', 'js@livingflame.co.nz', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. </p>\n\n<p>Please call us on 09 6221148 with your credit card details OR pay directly into our bank account with your name as a reference, and online training under particulars .</p>\n\n<p>Bank account number: 02 0240 0271125 00</p>\n\n<p>We will be in touch with you shortly.</p>\n<p> </p>\n\n<p>Team,</p>\n<p>Living Flame</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-05-06 21:27:31'),
(51, 1, 'gn@livingflame.co.nz', 'shaun@cpgas.co.nz', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. </p>\n\n<p>Please call us on 09 6221148 with your credit card details OR pay directly into our bank account with your name as a reference, and online training under particulars .</p>\n\n<p>Bank account number: 02 0240 0271125 00</p>\n\n<p>We will be in touch with you shortly.</p>\n<p> </p>\n\n<p>Team,</p>\n<p>Living Flame</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-05-07 11:00:57'),
(52, 1, 'gn@livingflame.co.nz', 'geetz13@msn.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. </p>\n\n<p>Please call us on 09 6221148 with your credit card details OR pay directly into our bank account with your name as a reference, and online training under particulars .</p>\n\n<p>Bank account number: 02 0240 0271125 00</p>\n\n<p>We will be in touch with you shortly.</p>\n<p> </p>\n\n<p>Team,</p>\n<p>Living Flame</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-08-25 12:07:21'),
(53, 2, 'gn@livingflame.co.nz', 'geetz13@msn.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame Online Course. We hope you enjoy the course and learn from its content. <br />\n  Please click on the link to start the assessment <a href="http://assessment.livingflame.co.nz/assessments?token=53fb26f9-7ea0-4822-aa01-735867061d02" target="_blank">http://assessment.livingflame.co.nz/assessments?token=53fb26f9-7ea0-4822-aa01-735867061d02</a> \n  <br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p>\n\n</body>\n</html>\n', '2014-08-25 12:08:30'),
(54, 1, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. </p>\n\n<p>Please call us on 09 6221148 with your credit card details OR pay directly into our bank account with your name as a reference, and online training under particulars .</p>\n\n<p>Bank account number: 02 0240 0271125 00</p>\n\n<p>We will be in touch with you shortly.</p>\n<p> </p>\n\n<p>Team,</p>\n<p>Living Flame</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-08-26 06:24:51'),
(55, 1, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for registering with Living Flame Online Training Course. </p>\n\n<p>Please call us on 09 6221148 with your credit card details OR pay directly into our bank account with your name as a reference, and online training under particulars .</p>\n\n<p>Bank account number: 02 0240 0271125 00</p>\n\n<p>We will be in touch with you shortly.</p>\n<p> </p>\n\n<p>Team,</p>\n<p>Living Flame</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p></body>\n</html>\n', '2014-08-26 21:30:07'),
(56, 2, 'gn@livingflame.co.nz', 'frankitoy@gmail.com', 'Living Flame Online Training Course', '<!DOCTYPE html>\n<html lang="en" ng-app>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n    <title>Emails/html</title>\n        <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta name="fragment" content="!" />\n    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">\n    <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/css/styles.css" /><script type="text/javascript" src="/js/jquery.min.js"></script><script type="text/javascript" src="/js/bootstrap.min.js"></script><script type="text/javascript" src="/js/angular.min.js"></script><script type="text/javascript" src="/js/scripts.js"></script></head>\n<body>\n	<p>Thank you for choosing Living Flame Online Course. We hope you enjoy the course and learn from its content. <br />\n  Please click on the link to start the assessment <a href="http://assessment.livingflame.co.nz/assessments?token=53fcfc5f-7760-418c-9263-57fa67061d02" target="_blank">http://assessment.livingflame.co.nz/assessments?token=53fcfc5f-7760-418c-9263-57fa67061d02</a> \n  <br />\n  </p>\n<p>Team,<br />\n  Living Flame<br />\n</p>\n<p>343B Church Street, Penrose, Auckland<br />\n  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />\n  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p>\n\n</body>\n</html>\n', '2014-08-26 21:31:01');

-- --------------------------------------------------------

--
-- Table structure for table `emails_type`
--

CREATE TABLE IF NOT EXISTS `emails_type` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `emails_type`
--

INSERT INTO `emails_type` (`id`, `name`) VALUES
(1, 'Registration'),
(2, 'Assessment');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `created`, `modified`) VALUES
(1, 'administrators', '2012-07-05 17:16:24', '2012-07-05 17:16:24'),
(2, 'managers', '2012-07-05 17:16:34', '2012-07-05 17:16:34'),
(3, 'users', '2012-07-05 17:16:45', '2012-07-05 17:16:45');

-- --------------------------------------------------------

--
-- Table structure for table `i18n`
--

CREATE TABLE IF NOT EXISTS `i18n` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` mediumtext,
  PRIMARY KEY (`id`),
  KEY `locale` (`locale`),
  KEY `model` (`model`),
  KEY `row_id` (`foreign_key`),
  KEY `field` (`field`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `module_slug_id` int(5) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text,
  `is_active` smallint(1) NOT NULL DEFAULT '0',
  `dte_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `module_slug_id`, `name`, `slug`, `description`, `is_active`, `dte_created`) VALUES
(1, 16, 'Module 16', 'flueing-and-downdraught', 'Flueing and Downdraught', 1, '2014-08-27 03:20:11'),
(2, 21, 'Module 21', 'pilot-assembly', 'Pilot Assembly', 1, '2014-08-27 03:20:16');

-- --------------------------------------------------------

--
-- Table structure for table `passing_grades`
--

CREATE TABLE IF NOT EXISTS `passing_grades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grade` varchar(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `passing_grades`
--

INSERT INTO `passing_grades` (`id`, `grade`) VALUES
(1, '7');

-- --------------------------------------------------------

--
-- Table structure for table `refresh_tokens`
--

CREATE TABLE IF NOT EXISTS `refresh_tokens` (
  `refresh_token` varchar(40) NOT NULL,
  `client_id` char(36) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `expires` int(11) NOT NULL,
  `scope` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`refresh_token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `schema_migrations`
--

CREATE TABLE IF NOT EXISTS `schema_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `schema_migrations`
--

INSERT INTO `schema_migrations` (`id`, `class`, `type`, `created`) VALUES
(1, 'InitMigrations', 'Migrations', '2014-01-06 07:00:44'),
(2, 'ConvertVersionToClassNames', 'Migrations', '2014-01-06 07:00:44'),
(3, 'IncreaseClassNameLength', 'Migrations', '2014-01-06 07:00:44'),
(4, 'DbOauth', 'OAuth', '2014-01-06 07:03:34'),
(5, 'M49c3417a54874a9d276811502cedc421', 'Users', '2014-01-07 15:33:04'),
(6, 'M4ef8ba03ff504ab2b415980575f6eb26', 'Users', '2014-01-07 15:33:04');

-- --------------------------------------------------------

--
-- Table structure for table `surveys`
--

CREATE TABLE IF NOT EXISTS `surveys` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `description` text,
  `is_active` smallint(1) NOT NULL,
  `dte_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `surveys`
--

INSERT INTO `surveys` (`id`, `question`, `description`, `is_active`, `dte_created`) VALUES
(1, 'The objective of the training were clearly defined.', NULL, 1, '2014-01-08 09:42:39'),
(2, 'The topics covered were relevant to me.', NULL, 1, '2014-01-08 09:42:47'),
(3, 'The content was organised and easy to follow.', NULL, 1, '2014-02-20 21:10:30'),
(4, 'The material distributed were helpful.', NULL, 1, '2014-01-08 09:43:01'),
(5, 'This training experience will be useful in my work.', NULL, 1, '2014-01-08 09:43:07'),
(6, 'The training objective were met.', NULL, 1, '2014-01-08 09:43:16'),
(7, 'The time alloted for the training was sufficient', NULL, 1, '2014-01-08 09:43:22');

-- --------------------------------------------------------

--
-- Table structure for table `surveys_choices`
--

CREATE TABLE IF NOT EXISTS `surveys_choices` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `choice` varchar(255) NOT NULL,
  `dte_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `surveys_choices`
--

INSERT INTO `surveys_choices` (`id`, `choice`, `dte_created`) VALUES
(1, 'Strongly Agree', '2014-01-07 07:45:06'),
(2, 'Agree', '2014-01-07 07:51:31'),
(3, 'Neither Agree Disagree', '2014-08-26 15:38:18'),
(4, 'Disagree', '2014-01-07 07:51:45'),
(5, 'Strongly Disagree', '2014-01-07 07:51:45'),
(6, 'Does Not Apply', '2014-08-26 15:38:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(36) NOT NULL,
  `modules_id` int(10) NOT NULL DEFAULT '1',
  `username` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `password` varchar(128) DEFAULT NULL,
  `password_token` varchar(128) DEFAULT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `address` text,
  `registration_number` varchar(50) NOT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `email_verified` tinyint(1) DEFAULT '0',
  `email_token` varchar(255) DEFAULT NULL,
  `email_token_expires` datetime DEFAULT NULL,
  `tos` tinyint(1) DEFAULT '0',
  `active` tinyint(1) DEFAULT '0',
  `has_paid` smallint(1) NOT NULL DEFAULT '0',
  `has_surveyed` smallint(1) NOT NULL DEFAULT '0',
  `has_generated_result` smallint(1) NOT NULL DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `last_action` datetime DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT '0',
  `assessment_attempt` smallint(1) NOT NULL DEFAULT '0',
  `assessment_score` smallint(2) NOT NULL DEFAULT '0',
  `assessment_link_sent` smallint(1) NOT NULL DEFAULT '0',
  `role` varchar(255) DEFAULT NULL,
  `date_paid` timestamp NULL DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `BY_USERNAME` (`username`),
  KEY `module_id` (`modules_id`),
  KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `modules_id`, `username`, `slug`, `password`, `password_token`, `first_name`, `last_name`, `company_name`, `address`, `registration_number`, `mobile`, `phone`, `email`, `email_verified`, `email_token`, `email_token_expires`, `tos`, `active`, `has_paid`, `has_surveyed`, `has_generated_result`, `last_login`, `last_action`, `is_admin`, `assessment_attempt`, `assessment_score`, `assessment_link_sent`, `role`, `date_paid`, `created`, `modified`) VALUES
('52d91359-efdc-4568-b3ae-09667a3d4a97', 1, 'admin', 'admin', '736b4e436327e5d5afd4b79056bb0b10ea4a9e08', 'f696bfa15b615c64562afa34400f326de0b905c4', 'Admin', 'Admin', NULL, NULL, '', '0211234567', '1234567', 'gn@livingflame.co.nz', 1, NULL, NULL, 1, 1, 0, 0, 0, '2014-08-27 15:28:02', NULL, 1, 0, 0, 0, 'user_registered', NULL, '2014-01-18 00:26:17', '2014-08-27 15:28:02'),
('530666f1-e648-4d9e-98f4-4e5367061d02', 1, 'sudhanshuy1w927aknm1392928497', 'sudhanshuy1w927aknm1392928497', 'f8b308d79c70ccf055dc35a74c8bb29761ab5879', 'sudhanshuajbg2wkcth1392928497', 'sudhanshu', 'dandekar', 'living flame', '343 church st\r\npenrose\r\nauckland', '.', '0211778439', '6221148', 'sd@livingflame.co.nz', 0, 'idj5r13cvf', '2014-02-22 09:34:57', 0, 0, 1, 1, 1, NULL, NULL, 0, 1, 7, 1, 'user_registered', '2014-02-20 20:37:44', '2014-02-21 09:34:57', '2014-02-21 09:39:58'),
('53066c0e-4efc-4dcd-adee-5f1467061d02', 1, 'Glena3xh9zqfyj1392929806', 'glena3xh9zqfyj1392929806', 'f8acaaeab447c9586ef961aa4855f570b410600c', 'Gleneqp0hgjfyn1392929806', 'Glen', 'Nichols', 'Living Flame,', '343 Church Street\r\nOnehunga\r\n', '.', '021 833 144', '622 1148', 'gn@livingflame.co.nz', 0, 'o312fipjm9', '2014-02-22 09:56:46', 0, 0, 1, 0, 1, NULL, NULL, 0, 2, 6, 1, 'user_registered', '2014-02-20 21:00:13', '2014-02-21 09:56:46', '2014-02-21 12:43:12'),
('536953c3-2e4c-4054-8061-12d567061d02', 1, 'Janlpbo0k9weg1399411651', 'janlpbo0k9weg1399411651', '0ca066f3ce508f847316b63f9af88a7727f18440', 'Janj0ghyidmwl1399411651', 'Jan', 'Stephens', 'Living Flame', '343b Church Street\r\nPenrose', '0001', '64211161628', '64211161628', 'js@livingflame.co.nz', 0, 'x5vqy3a1tz', '2014-05-08 09:27:31', 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 'user_registered', NULL, '2014-05-07 09:27:31', '2014-05-07 09:27:31'),
('536a1269-44d0-4827-8333-2eff67061d02', 1, 'shaungcjbnaftpq1399460457', 'shaungcjbnaftpq1399460457', '283ec5ccdde39f7a6729a50d894a0c5e20430994', 'shaunm6pgjqrsac1399460457', 'shaun ', 'meiklejohn', 'certified plumbing and gas', '22 Connell street\r\nblockhouse bay\r\nAuckland', '12914', '0274972703', '', 'shaun@cpgas.co.nz', 0, 'xwvs92ojy8', '2014-05-08 23:00:57', 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 'user_registered', NULL, '2014-05-07 23:00:57', '2014-05-07 23:00:57'),
('53fb26f9-7ea0-4822-aa01-735867061d02', 1, 'Geet15sfpibdev1408968441', 'geet15sfpibdev1408968441', '0335cb63fcacfda1b27ebb80ec3a2b3d19f4c66d', 'Geetl8gcho3n7p1408968441', 'Geet', 'pHANSE', 'Testing Module 21', 'Testing Module 21', 'Testing Module 21', '25896348', '454987484', 'geetz13@msn.com', 0, '1x3grlknce', '2014-08-27 00:07:21', 0, 0, 1, 0, 0, NULL, NULL, 0, 0, 0, 1, 'user_registered', '2014-08-25 12:08:26', '2014-08-26 00:07:21', '2014-08-26 00:07:21');

-- --------------------------------------------------------

--
-- Table structure for table `users_assessments_choices_answers`
--

CREATE TABLE IF NOT EXISTS `users_assessments_choices_answers` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `assessments_id` int(10) NOT NULL,
  `users_id` varchar(36) NOT NULL,
  `assessments_choices_id` int(10) NOT NULL,
  `modules_id` int(10) NOT NULL,
  `attempt_group` int(1) NOT NULL DEFAULT '1',
  `dte_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assessments_id` (`assessments_id`,`users_id`,`assessments_choices_id`,`modules_id`,`attempt_group`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=281 ;

--
-- Dumping data for table `users_assessments_choices_answers`
--

INSERT INTO `users_assessments_choices_answers` (`id`, `assessments_id`, `users_id`, `assessments_choices_id`, `modules_id`, `attempt_group`, `dte_created`) VALUES
(1, 1, '53027e45-2b98-4c2c-87d5-6fc567061d02', 1, 1, 1, '2014-02-17 22:17:20'),
(2, 2, '53027e45-2b98-4c2c-87d5-6fc567061d02', 4, 1, 1, '2014-02-17 22:17:20'),
(3, 3, '53027e45-2b98-4c2c-87d5-6fc567061d02', 7, 1, 1, '2014-02-17 22:17:20'),
(4, 4, '53027e45-2b98-4c2c-87d5-6fc567061d02', 10, 1, 1, '2014-02-17 22:17:20'),
(5, 5, '53027e45-2b98-4c2c-87d5-6fc567061d02', 15, 1, 1, '2014-02-17 22:17:20'),
(6, 6, '53027e45-2b98-4c2c-87d5-6fc567061d02', 18, 1, 1, '2014-02-17 22:17:20'),
(7, 7, '53027e45-2b98-4c2c-87d5-6fc567061d02', 20, 1, 1, '2014-02-17 22:17:20'),
(8, 8, '53027e45-2b98-4c2c-87d5-6fc567061d02', 23, 1, 1, '2014-02-17 22:17:20'),
(9, 9, '53027e45-2b98-4c2c-87d5-6fc567061d02', 26, 1, 1, '2014-02-17 22:17:20'),
(10, 10, '53027e45-2b98-4c2c-87d5-6fc567061d02', 29, 1, 1, '2014-02-17 22:17:20'),
(11, 1, '53027e45-2b98-4c2c-87d5-6fc567061d02', 2, 1, 2, '2014-02-17 22:51:13'),
(12, 2, '53027e45-2b98-4c2c-87d5-6fc567061d02', 5, 1, 2, '2014-02-17 22:51:13'),
(13, 3, '53027e45-2b98-4c2c-87d5-6fc567061d02', 8, 1, 2, '2014-02-17 22:51:13'),
(14, 4, '53027e45-2b98-4c2c-87d5-6fc567061d02', 11, 1, 2, '2014-02-17 22:51:13'),
(15, 5, '53027e45-2b98-4c2c-87d5-6fc567061d02', 13, 1, 2, '2014-02-17 22:51:13'),
(16, 6, '53027e45-2b98-4c2c-87d5-6fc567061d02', 18, 1, 2, '2014-02-17 22:51:13'),
(17, 7, '53027e45-2b98-4c2c-87d5-6fc567061d02', 20, 1, 2, '2014-02-17 22:51:13'),
(18, 8, '53027e45-2b98-4c2c-87d5-6fc567061d02', 23, 1, 2, '2014-02-17 22:51:13'),
(19, 9, '53027e45-2b98-4c2c-87d5-6fc567061d02', 26, 1, 2, '2014-02-17 22:51:13'),
(20, 10, '53027e45-2b98-4c2c-87d5-6fc567061d02', 29, 1, 2, '2014-02-17 22:51:13'),
(21, 1, '5302b359-8254-4f48-93cf-65ce67061d02', 1, 1, 1, '2014-02-18 01:33:10'),
(22, 2, '5302b359-8254-4f48-93cf-65ce67061d02', 4, 1, 1, '2014-02-18 01:33:10'),
(23, 3, '5302b359-8254-4f48-93cf-65ce67061d02', 7, 1, 1, '2014-02-18 01:33:10'),
(24, 4, '5302b359-8254-4f48-93cf-65ce67061d02', 10, 1, 1, '2014-02-18 01:33:10'),
(25, 5, '5302b359-8254-4f48-93cf-65ce67061d02', 13, 1, 1, '2014-02-18 01:33:10'),
(26, 6, '5302b359-8254-4f48-93cf-65ce67061d02', 16, 1, 1, '2014-02-18 01:33:10'),
(27, 7, '5302b359-8254-4f48-93cf-65ce67061d02', 19, 1, 1, '2014-02-18 01:33:10'),
(28, 8, '5302b359-8254-4f48-93cf-65ce67061d02', 22, 1, 1, '2014-02-18 01:33:10'),
(29, 9, '5302b359-8254-4f48-93cf-65ce67061d02', 25, 1, 1, '2014-02-18 01:33:10'),
(30, 10, '5302b359-8254-4f48-93cf-65ce67061d02', 28, 1, 1, '2014-02-18 01:33:10'),
(31, 1, '5302b359-8254-4f48-93cf-65ce67061d02', 1, 1, 2, '2014-02-18 01:41:43'),
(32, 2, '5302b359-8254-4f48-93cf-65ce67061d02', 4, 1, 2, '2014-02-18 01:41:43'),
(33, 3, '5302b359-8254-4f48-93cf-65ce67061d02', 7, 1, 2, '2014-02-18 01:41:43'),
(34, 4, '5302b359-8254-4f48-93cf-65ce67061d02', 10, 1, 2, '2014-02-18 01:41:43'),
(35, 5, '5302b359-8254-4f48-93cf-65ce67061d02', 13, 1, 2, '2014-02-18 01:41:43'),
(36, 6, '5302b359-8254-4f48-93cf-65ce67061d02', 16, 1, 2, '2014-02-18 01:41:43'),
(37, 7, '5302b359-8254-4f48-93cf-65ce67061d02', 19, 1, 2, '2014-02-18 01:41:43'),
(38, 8, '5302b359-8254-4f48-93cf-65ce67061d02', 22, 1, 2, '2014-02-18 01:41:43'),
(39, 9, '5302b359-8254-4f48-93cf-65ce67061d02', 25, 1, 2, '2014-02-18 01:41:43'),
(40, 10, '5302b359-8254-4f48-93cf-65ce67061d02', 28, 1, 2, '2014-02-18 01:41:43'),
(41, 1, '5302bb4e-13bc-4cf7-8eeb-167d67061d02', 1, 1, 1, '2014-02-18 01:46:25'),
(42, 2, '5302bb4e-13bc-4cf7-8eeb-167d67061d02', 4, 1, 1, '2014-02-18 01:46:25'),
(43, 3, '5302bb4e-13bc-4cf7-8eeb-167d67061d02', 7, 1, 1, '2014-02-18 01:46:25'),
(44, 4, '5302bb4e-13bc-4cf7-8eeb-167d67061d02', 10, 1, 1, '2014-02-18 01:46:25'),
(45, 5, '5302bb4e-13bc-4cf7-8eeb-167d67061d02', 13, 1, 1, '2014-02-18 01:46:25'),
(46, 6, '5302bb4e-13bc-4cf7-8eeb-167d67061d02', 16, 1, 1, '2014-02-18 01:46:25'),
(47, 7, '5302bb4e-13bc-4cf7-8eeb-167d67061d02', 19, 1, 1, '2014-02-18 01:46:25'),
(48, 8, '5302bb4e-13bc-4cf7-8eeb-167d67061d02', 22, 1, 1, '2014-02-18 01:46:25'),
(49, 9, '5302bb4e-13bc-4cf7-8eeb-167d67061d02', 25, 1, 1, '2014-02-18 01:46:25'),
(50, 10, '5302bb4e-13bc-4cf7-8eeb-167d67061d02', 28, 1, 1, '2014-02-18 01:46:25'),
(51, 1, '5302bb4e-13bc-4cf7-8eeb-167d67061d02', 1, 1, 2, '2014-02-18 01:48:44'),
(52, 2, '5302bb4e-13bc-4cf7-8eeb-167d67061d02', 4, 1, 2, '2014-02-18 01:48:44'),
(53, 3, '5302bb4e-13bc-4cf7-8eeb-167d67061d02', 7, 1, 2, '2014-02-18 01:48:44'),
(54, 4, '5302bb4e-13bc-4cf7-8eeb-167d67061d02', 10, 1, 2, '2014-02-18 01:48:44'),
(55, 5, '5302bb4e-13bc-4cf7-8eeb-167d67061d02', 13, 1, 2, '2014-02-18 01:48:44'),
(56, 6, '5302bb4e-13bc-4cf7-8eeb-167d67061d02', 16, 1, 2, '2014-02-18 01:48:44'),
(57, 7, '5302bb4e-13bc-4cf7-8eeb-167d67061d02', 19, 1, 2, '2014-02-18 01:48:44'),
(58, 8, '5302bb4e-13bc-4cf7-8eeb-167d67061d02', 22, 1, 2, '2014-02-18 01:48:44'),
(59, 9, '5302bb4e-13bc-4cf7-8eeb-167d67061d02', 25, 1, 2, '2014-02-18 01:48:44'),
(60, 10, '5302bb4e-13bc-4cf7-8eeb-167d67061d02', 28, 1, 2, '2014-02-18 01:48:44'),
(61, 1, '5302bf3f-7678-4c0d-b29d-2db967061d02', 1, 1, 1, '2014-02-18 02:03:58'),
(62, 2, '5302bf3f-7678-4c0d-b29d-2db967061d02', 4, 1, 1, '2014-02-18 02:03:58'),
(63, 3, '5302bf3f-7678-4c0d-b29d-2db967061d02', 7, 1, 1, '2014-02-18 02:03:58'),
(64, 4, '5302bf3f-7678-4c0d-b29d-2db967061d02', 10, 1, 1, '2014-02-18 02:03:58'),
(65, 5, '5302bf3f-7678-4c0d-b29d-2db967061d02', 13, 1, 1, '2014-02-18 02:03:58'),
(66, 6, '5302bf3f-7678-4c0d-b29d-2db967061d02', 16, 1, 1, '2014-02-18 02:03:58'),
(67, 7, '5302bf3f-7678-4c0d-b29d-2db967061d02', 19, 1, 1, '2014-02-18 02:03:58'),
(68, 8, '5302bf3f-7678-4c0d-b29d-2db967061d02', 22, 1, 1, '2014-02-18 02:03:58'),
(69, 9, '5302bf3f-7678-4c0d-b29d-2db967061d02', 25, 1, 1, '2014-02-18 02:03:58'),
(70, 10, '5302bf3f-7678-4c0d-b29d-2db967061d02', 28, 1, 1, '2014-02-18 02:03:58'),
(71, 1, '5302b5d3-3f9c-4c29-a720-72e967061d02', 2, 1, 1, '2014-02-18 02:57:13'),
(72, 2, '5302b5d3-3f9c-4c29-a720-72e967061d02', 4, 1, 1, '2014-02-18 02:57:13'),
(73, 3, '5302b5d3-3f9c-4c29-a720-72e967061d02', 9, 1, 1, '2014-02-18 02:57:13'),
(74, 4, '5302b5d3-3f9c-4c29-a720-72e967061d02', 10, 1, 1, '2014-02-18 02:57:13'),
(75, 5, '5302b5d3-3f9c-4c29-a720-72e967061d02', 13, 1, 1, '2014-02-18 02:57:13'),
(76, 6, '5302b5d3-3f9c-4c29-a720-72e967061d02', 18, 1, 1, '2014-02-18 02:57:13'),
(77, 7, '5302b5d3-3f9c-4c29-a720-72e967061d02', 20, 1, 1, '2014-02-18 02:57:13'),
(78, 8, '5302b5d3-3f9c-4c29-a720-72e967061d02', 23, 1, 1, '2014-02-18 02:57:13'),
(79, 9, '5302b5d3-3f9c-4c29-a720-72e967061d02', 26, 1, 1, '2014-02-18 02:57:13'),
(80, 10, '5302b5d3-3f9c-4c29-a720-72e967061d02', 28, 1, 1, '2014-02-18 02:57:13'),
(81, 1, '530474bb-c968-4a32-9ab7-3e1f67061d02', 1, 1, 1, '2014-02-19 09:14:35'),
(82, 2, '530474bb-c968-4a32-9ab7-3e1f67061d02', 4, 1, 1, '2014-02-19 09:14:35'),
(83, 3, '530474bb-c968-4a32-9ab7-3e1f67061d02', 7, 1, 1, '2014-02-19 09:14:35'),
(84, 4, '530474bb-c968-4a32-9ab7-3e1f67061d02', 10, 1, 1, '2014-02-19 09:14:35'),
(85, 5, '530474bb-c968-4a32-9ab7-3e1f67061d02', 13, 1, 1, '2014-02-19 09:14:35'),
(86, 6, '530474bb-c968-4a32-9ab7-3e1f67061d02', 17, 1, 1, '2014-02-19 09:14:35'),
(87, 7, '530474bb-c968-4a32-9ab7-3e1f67061d02', 19, 1, 1, '2014-02-19 09:14:35'),
(88, 8, '530474bb-c968-4a32-9ab7-3e1f67061d02', 22, 1, 1, '2014-02-19 09:14:35'),
(89, 9, '530474bb-c968-4a32-9ab7-3e1f67061d02', 25, 1, 1, '2014-02-19 09:14:35'),
(90, 10, '530474bb-c968-4a32-9ab7-3e1f67061d02', 28, 1, 1, '2014-02-19 09:14:35'),
(91, 1, '5304685c-c978-4fb1-87ab-1ef967061d02', 2, 1, 1, '2014-02-19 09:18:14'),
(92, 2, '5304685c-c978-4fb1-87ab-1ef967061d02', 5, 1, 1, '2014-02-19 09:18:14'),
(93, 3, '5304685c-c978-4fb1-87ab-1ef967061d02', 8, 1, 1, '2014-02-19 09:18:14'),
(94, 4, '5304685c-c978-4fb1-87ab-1ef967061d02', 11, 1, 1, '2014-02-19 09:18:14'),
(95, 5, '5304685c-c978-4fb1-87ab-1ef967061d02', 14, 1, 1, '2014-02-19 09:18:14'),
(96, 6, '5304685c-c978-4fb1-87ab-1ef967061d02', 17, 1, 1, '2014-02-19 09:18:14'),
(97, 7, '5304685c-c978-4fb1-87ab-1ef967061d02', 20, 1, 1, '2014-02-19 09:18:14'),
(98, 8, '5304685c-c978-4fb1-87ab-1ef967061d02', 23, 1, 1, '2014-02-19 09:18:14'),
(99, 9, '5304685c-c978-4fb1-87ab-1ef967061d02', 26, 1, 1, '2014-02-19 09:18:14'),
(100, 10, '5304685c-c978-4fb1-87ab-1ef967061d02', 29, 1, 1, '2014-02-19 09:18:14'),
(101, 1, '530474bb-c968-4a32-9ab7-3e1f67061d02', 1, 1, 2, '2014-02-19 09:20:11'),
(102, 2, '530474bb-c968-4a32-9ab7-3e1f67061d02', 4, 1, 2, '2014-02-19 09:20:11'),
(103, 3, '530474bb-c968-4a32-9ab7-3e1f67061d02', 7, 1, 2, '2014-02-19 09:20:11'),
(104, 4, '530474bb-c968-4a32-9ab7-3e1f67061d02', 10, 1, 2, '2014-02-19 09:20:11'),
(105, 5, '530474bb-c968-4a32-9ab7-3e1f67061d02', 13, 1, 2, '2014-02-19 09:20:11'),
(106, 6, '530474bb-c968-4a32-9ab7-3e1f67061d02', 16, 1, 2, '2014-02-19 09:20:11'),
(107, 7, '530474bb-c968-4a32-9ab7-3e1f67061d02', 19, 1, 2, '2014-02-19 09:20:11'),
(108, 8, '530474bb-c968-4a32-9ab7-3e1f67061d02', 22, 1, 2, '2014-02-19 09:20:11'),
(109, 9, '530474bb-c968-4a32-9ab7-3e1f67061d02', 25, 1, 2, '2014-02-19 09:20:11'),
(110, 10, '530474bb-c968-4a32-9ab7-3e1f67061d02', 28, 1, 2, '2014-02-19 09:20:11'),
(111, 1, '5304685c-c978-4fb1-87ab-1ef967061d02', 2, 1, 2, '2014-02-19 09:26:25'),
(112, 2, '5304685c-c978-4fb1-87ab-1ef967061d02', 5, 1, 2, '2014-02-19 09:26:25'),
(113, 3, '5304685c-c978-4fb1-87ab-1ef967061d02', 8, 1, 2, '2014-02-19 09:26:25'),
(114, 4, '5304685c-c978-4fb1-87ab-1ef967061d02', 11, 1, 2, '2014-02-19 09:26:25'),
(115, 5, '5304685c-c978-4fb1-87ab-1ef967061d02', 14, 1, 2, '2014-02-19 09:26:25'),
(116, 6, '5304685c-c978-4fb1-87ab-1ef967061d02', 17, 1, 2, '2014-02-19 09:26:25'),
(117, 7, '5304685c-c978-4fb1-87ab-1ef967061d02', 20, 1, 2, '2014-02-19 09:26:25'),
(118, 8, '5304685c-c978-4fb1-87ab-1ef967061d02', 23, 1, 2, '2014-02-19 09:26:25'),
(119, 9, '5304685c-c978-4fb1-87ab-1ef967061d02', 26, 1, 2, '2014-02-19 09:26:25'),
(120, 10, '5304685c-c978-4fb1-87ab-1ef967061d02', 29, 1, 2, '2014-02-19 09:26:25'),
(121, 1, '5304685c-c978-4fb1-87ab-1ef967061d02', 2, 1, 3, '2014-02-19 09:26:35'),
(122, 2, '5304685c-c978-4fb1-87ab-1ef967061d02', 5, 1, 3, '2014-02-19 09:26:35'),
(123, 3, '5304685c-c978-4fb1-87ab-1ef967061d02', 8, 1, 3, '2014-02-19 09:26:35'),
(124, 4, '5304685c-c978-4fb1-87ab-1ef967061d02', 11, 1, 3, '2014-02-19 09:26:35'),
(125, 5, '5304685c-c978-4fb1-87ab-1ef967061d02', 14, 1, 3, '2014-02-19 09:26:35'),
(126, 6, '5304685c-c978-4fb1-87ab-1ef967061d02', 17, 1, 3, '2014-02-19 09:26:35'),
(127, 7, '5304685c-c978-4fb1-87ab-1ef967061d02', 20, 1, 3, '2014-02-19 09:26:35'),
(128, 8, '5304685c-c978-4fb1-87ab-1ef967061d02', 23, 1, 3, '2014-02-19 09:26:35'),
(129, 9, '5304685c-c978-4fb1-87ab-1ef967061d02', 26, 1, 3, '2014-02-19 09:26:35'),
(130, 10, '5304685c-c978-4fb1-87ab-1ef967061d02', 29, 1, 3, '2014-02-19 09:26:35'),
(131, 1, '530474bb-c968-4a32-9ab7-3e1f67061d02', 1, 1, 3, '2014-02-19 09:26:47'),
(132, 2, '530474bb-c968-4a32-9ab7-3e1f67061d02', 4, 1, 3, '2014-02-19 09:26:47'),
(133, 3, '530474bb-c968-4a32-9ab7-3e1f67061d02', 7, 1, 3, '2014-02-19 09:26:47'),
(134, 4, '530474bb-c968-4a32-9ab7-3e1f67061d02', 10, 1, 3, '2014-02-19 09:26:47'),
(135, 5, '530474bb-c968-4a32-9ab7-3e1f67061d02', 13, 1, 3, '2014-02-19 09:26:47'),
(136, 6, '530474bb-c968-4a32-9ab7-3e1f67061d02', 16, 1, 3, '2014-02-19 09:26:47'),
(137, 7, '530474bb-c968-4a32-9ab7-3e1f67061d02', 19, 1, 3, '2014-02-19 09:26:47'),
(138, 8, '530474bb-c968-4a32-9ab7-3e1f67061d02', 22, 1, 3, '2014-02-19 09:26:47'),
(139, 9, '530474bb-c968-4a32-9ab7-3e1f67061d02', 25, 1, 3, '2014-02-19 09:26:47'),
(140, 10, '530474bb-c968-4a32-9ab7-3e1f67061d02', 28, 1, 3, '2014-02-19 09:26:47'),
(141, 1, '53046744-47e4-42d4-a734-1cb267061d02', 2, 1, 1, '2014-02-19 09:33:51'),
(142, 2, '53046744-47e4-42d4-a734-1cb267061d02', 5, 1, 1, '2014-02-19 09:33:51'),
(143, 3, '53046744-47e4-42d4-a734-1cb267061d02', 8, 1, 1, '2014-02-19 09:33:51'),
(144, 4, '53046744-47e4-42d4-a734-1cb267061d02', 11, 1, 1, '2014-02-19 09:33:51'),
(145, 5, '53046744-47e4-42d4-a734-1cb267061d02', 14, 1, 1, '2014-02-19 09:33:51'),
(146, 6, '53046744-47e4-42d4-a734-1cb267061d02', 17, 1, 1, '2014-02-19 09:33:51'),
(147, 7, '53046744-47e4-42d4-a734-1cb267061d02', 20, 1, 1, '2014-02-19 09:33:51'),
(148, 8, '53046744-47e4-42d4-a734-1cb267061d02', 22, 1, 1, '2014-02-19 09:33:51'),
(149, 9, '53046744-47e4-42d4-a734-1cb267061d02', 27, 1, 1, '2014-02-19 09:33:51'),
(150, 10, '53046744-47e4-42d4-a734-1cb267061d02', 29, 1, 1, '2014-02-19 09:33:51'),
(151, 1, '5304686e-1118-4cfc-920f-1cf867061d02', 1, 1, 1, '2014-02-19 09:42:17'),
(152, 2, '5304686e-1118-4cfc-920f-1cf867061d02', 4, 1, 1, '2014-02-19 09:42:17'),
(153, 3, '5304686e-1118-4cfc-920f-1cf867061d02', 7, 1, 1, '2014-02-19 09:42:17'),
(154, 4, '5304686e-1118-4cfc-920f-1cf867061d02', 10, 1, 1, '2014-02-19 09:42:17'),
(155, 5, '5304686e-1118-4cfc-920f-1cf867061d02', 13, 1, 1, '2014-02-19 09:42:17'),
(156, 6, '5304686e-1118-4cfc-920f-1cf867061d02', 16, 1, 1, '2014-02-19 09:42:17'),
(157, 7, '5304686e-1118-4cfc-920f-1cf867061d02', 19, 1, 1, '2014-02-19 09:42:17'),
(158, 8, '5304686e-1118-4cfc-920f-1cf867061d02', 22, 1, 1, '2014-02-19 09:42:17'),
(159, 9, '5304686e-1118-4cfc-920f-1cf867061d02', 25, 1, 1, '2014-02-19 09:42:17'),
(160, 10, '5304686e-1118-4cfc-920f-1cf867061d02', 28, 1, 1, '2014-02-19 09:42:17'),
(161, 1, '5304686e-1118-4cfc-920f-1cf867061d02', 1, 1, 2, '2014-02-19 09:43:20'),
(162, 2, '5304686e-1118-4cfc-920f-1cf867061d02', 4, 1, 2, '2014-02-19 09:43:20'),
(163, 3, '5304686e-1118-4cfc-920f-1cf867061d02', 7, 1, 2, '2014-02-19 09:43:20'),
(164, 4, '5304686e-1118-4cfc-920f-1cf867061d02', 10, 1, 2, '2014-02-19 09:43:20'),
(165, 5, '5304686e-1118-4cfc-920f-1cf867061d02', 13, 1, 2, '2014-02-19 09:43:20'),
(166, 6, '5304686e-1118-4cfc-920f-1cf867061d02', 16, 1, 2, '2014-02-19 09:43:20'),
(167, 7, '5304686e-1118-4cfc-920f-1cf867061d02', 19, 1, 2, '2014-02-19 09:43:20'),
(168, 8, '5304686e-1118-4cfc-920f-1cf867061d02', 22, 1, 2, '2014-02-19 09:43:20'),
(169, 9, '5304686e-1118-4cfc-920f-1cf867061d02', 25, 1, 2, '2014-02-19 09:43:20'),
(170, 10, '5304686e-1118-4cfc-920f-1cf867061d02', 28, 1, 2, '2014-02-19 09:43:20'),
(171, 1, '53046744-47e4-42d4-a734-1cb267061d02', 2, 1, 2, '2014-02-19 10:02:17'),
(172, 2, '53046744-47e4-42d4-a734-1cb267061d02', 4, 1, 2, '2014-02-19 10:02:17'),
(173, 3, '53046744-47e4-42d4-a734-1cb267061d02', 9, 1, 2, '2014-02-19 10:02:17'),
(174, 4, '53046744-47e4-42d4-a734-1cb267061d02', 10, 1, 2, '2014-02-19 10:02:17'),
(175, 5, '53046744-47e4-42d4-a734-1cb267061d02', 13, 1, 2, '2014-02-19 10:02:17'),
(176, 6, '53046744-47e4-42d4-a734-1cb267061d02', 18, 1, 2, '2014-02-19 10:02:17'),
(177, 7, '53046744-47e4-42d4-a734-1cb267061d02', 20, 1, 2, '2014-02-19 10:02:17'),
(178, 8, '53046744-47e4-42d4-a734-1cb267061d02', 24, 1, 2, '2014-02-19 10:02:17'),
(179, 9, '53046744-47e4-42d4-a734-1cb267061d02', 26, 1, 2, '2014-02-19 10:02:17'),
(180, 10, '53046744-47e4-42d4-a734-1cb267061d02', 28, 1, 2, '2014-02-19 10:02:17'),
(181, 1, '530466cd-3de8-458f-b695-1af667061d02', 2, 1, 1, '2014-02-19 10:15:46'),
(182, 2, '530466cd-3de8-458f-b695-1af667061d02', 4, 1, 1, '2014-02-19 10:15:46'),
(183, 3, '530466cd-3de8-458f-b695-1af667061d02', 9, 1, 1, '2014-02-19 10:15:46'),
(184, 4, '530466cd-3de8-458f-b695-1af667061d02', 10, 1, 1, '2014-02-19 10:15:46'),
(185, 5, '530466cd-3de8-458f-b695-1af667061d02', 13, 1, 1, '2014-02-19 10:15:46'),
(186, 6, '530466cd-3de8-458f-b695-1af667061d02', 18, 1, 1, '2014-02-19 10:15:46'),
(187, 7, '530466cd-3de8-458f-b695-1af667061d02', 20, 1, 1, '2014-02-19 10:15:46'),
(188, 8, '530466cd-3de8-458f-b695-1af667061d02', 24, 1, 1, '2014-02-19 10:15:46'),
(189, 9, '530466cd-3de8-458f-b695-1af667061d02', 26, 1, 1, '2014-02-19 10:15:46'),
(190, 10, '530466cd-3de8-458f-b695-1af667061d02', 30, 1, 1, '2014-02-19 10:15:46'),
(191, 1, '530666f4-d880-4230-bc02-4ecd67061d02', 2, 1, 1, '2014-02-20 20:39:04'),
(192, 2, '530666f4-d880-4230-bc02-4ecd67061d02', 5, 1, 1, '2014-02-20 20:39:04'),
(193, 3, '530666f4-d880-4230-bc02-4ecd67061d02', 8, 1, 1, '2014-02-20 20:39:04'),
(194, 4, '530666f4-d880-4230-bc02-4ecd67061d02', 11, 1, 1, '2014-02-20 20:39:04'),
(195, 5, '530666f4-d880-4230-bc02-4ecd67061d02', 14, 1, 1, '2014-02-20 20:39:04'),
(196, 6, '530666f4-d880-4230-bc02-4ecd67061d02', 17, 1, 1, '2014-02-20 20:39:04'),
(197, 7, '530666f4-d880-4230-bc02-4ecd67061d02', 20, 1, 1, '2014-02-20 20:39:04'),
(198, 8, '530666f4-d880-4230-bc02-4ecd67061d02', 23, 1, 1, '2014-02-20 20:39:04'),
(199, 9, '530666f4-d880-4230-bc02-4ecd67061d02', 26, 1, 1, '2014-02-20 20:39:04'),
(200, 10, '530666f4-d880-4230-bc02-4ecd67061d02', 29, 1, 1, '2014-02-20 20:39:04'),
(201, 1, '530666f1-e648-4d9e-98f4-4e5367061d02', 2, 1, 1, '2014-02-20 20:39:06'),
(202, 2, '530666f1-e648-4d9e-98f4-4e5367061d02', 4, 1, 1, '2014-02-20 20:39:06'),
(203, 3, '530666f1-e648-4d9e-98f4-4e5367061d02', 9, 1, 1, '2014-02-20 20:39:06'),
(204, 4, '530666f1-e648-4d9e-98f4-4e5367061d02', 10, 1, 1, '2014-02-20 20:39:06'),
(205, 5, '530666f1-e648-4d9e-98f4-4e5367061d02', 13, 1, 1, '2014-02-20 20:39:06'),
(206, 6, '530666f1-e648-4d9e-98f4-4e5367061d02', 17, 1, 1, '2014-02-20 20:39:06'),
(207, 7, '530666f1-e648-4d9e-98f4-4e5367061d02', 20, 1, 1, '2014-02-20 20:39:06'),
(208, 8, '530666f1-e648-4d9e-98f4-4e5367061d02', 24, 1, 1, '2014-02-20 20:39:06'),
(209, 9, '530666f1-e648-4d9e-98f4-4e5367061d02', 27, 1, 1, '2014-02-20 20:39:06'),
(210, 10, '530666f1-e648-4d9e-98f4-4e5367061d02', 30, 1, 1, '2014-02-20 20:39:06'),
(211, 1, '5306689c-fce4-4007-9bf2-551567061d02', 2, 1, 1, '2014-02-20 20:42:41'),
(212, 2, '5306689c-fce4-4007-9bf2-551567061d02', 5, 1, 1, '2014-02-20 20:42:41'),
(213, 3, '5306689c-fce4-4007-9bf2-551567061d02', 8, 1, 1, '2014-02-20 20:42:41'),
(214, 4, '5306689c-fce4-4007-9bf2-551567061d02', 11, 1, 1, '2014-02-20 20:42:41'),
(215, 5, '5306689c-fce4-4007-9bf2-551567061d02', 14, 1, 1, '2014-02-20 20:42:41'),
(216, 6, '5306689c-fce4-4007-9bf2-551567061d02', 17, 1, 1, '2014-02-20 20:42:41'),
(217, 7, '5306689c-fce4-4007-9bf2-551567061d02', 20, 1, 1, '2014-02-20 20:42:41'),
(218, 8, '5306689c-fce4-4007-9bf2-551567061d02', 23, 1, 1, '2014-02-20 20:42:41'),
(219, 9, '5306689c-fce4-4007-9bf2-551567061d02', 26, 1, 1, '2014-02-20 20:42:41'),
(220, 10, '5306689c-fce4-4007-9bf2-551567061d02', 29, 1, 1, '2014-02-20 20:42:41'),
(221, 1, '53066c0e-4efc-4dcd-adee-5f1467061d02', 2, 1, 1, '2014-02-20 21:02:31'),
(222, 2, '53066c0e-4efc-4dcd-adee-5f1467061d02', 4, 1, 1, '2014-02-20 21:02:31'),
(223, 3, '53066c0e-4efc-4dcd-adee-5f1467061d02', 9, 1, 1, '2014-02-20 21:02:31'),
(224, 4, '53066c0e-4efc-4dcd-adee-5f1467061d02', 10, 1, 1, '2014-02-20 21:02:31'),
(225, 5, '53066c0e-4efc-4dcd-adee-5f1467061d02', 14, 1, 1, '2014-02-20 21:02:31'),
(226, 6, '53066c0e-4efc-4dcd-adee-5f1467061d02', 17, 1, 1, '2014-02-20 21:02:31'),
(227, 7, '53066c0e-4efc-4dcd-adee-5f1467061d02', 20, 1, 1, '2014-02-20 21:02:31'),
(228, 8, '53066c0e-4efc-4dcd-adee-5f1467061d02', 23, 1, 1, '2014-02-20 21:02:31'),
(229, 9, '53066c0e-4efc-4dcd-adee-5f1467061d02', 27, 1, 1, '2014-02-20 21:02:31'),
(230, 10, '53066c0e-4efc-4dcd-adee-5f1467061d02', 30, 1, 1, '2014-02-20 21:02:31'),
(231, 1, '53066c0e-4efc-4dcd-adee-5f1467061d02', 1, 1, 2, '2014-02-20 23:43:12'),
(232, 2, '53066c0e-4efc-4dcd-adee-5f1467061d02', 6, 1, 2, '2014-02-20 23:43:12'),
(233, 3, '53066c0e-4efc-4dcd-adee-5f1467061d02', 8, 1, 2, '2014-02-20 23:43:12'),
(234, 4, '53066c0e-4efc-4dcd-adee-5f1467061d02', 10, 1, 2, '2014-02-20 23:43:12'),
(235, 5, '53066c0e-4efc-4dcd-adee-5f1467061d02', 14, 1, 2, '2014-02-20 23:43:12'),
(236, 6, '53066c0e-4efc-4dcd-adee-5f1467061d02', 18, 1, 2, '2014-02-20 23:43:12'),
(237, 7, '53066c0e-4efc-4dcd-adee-5f1467061d02', 20, 1, 2, '2014-02-20 23:43:12'),
(238, 8, '53066c0e-4efc-4dcd-adee-5f1467061d02', 24, 1, 2, '2014-02-20 23:43:12'),
(239, 9, '53066c0e-4efc-4dcd-adee-5f1467061d02', 26, 1, 2, '2014-02-20 23:43:12'),
(240, 10, '53066c0e-4efc-4dcd-adee-5f1467061d02', 28, 1, 2, '2014-02-20 23:43:12'),
(241, 1, '530b189d-c4c4-4f85-85e8-0c1567061d02', 1, 1, 1, '2014-02-24 10:02:59'),
(242, 2, '530b189d-c4c4-4f85-85e8-0c1567061d02', 4, 1, 1, '2014-02-24 10:02:59'),
(243, 3, '530b189d-c4c4-4f85-85e8-0c1567061d02', 7, 1, 1, '2014-02-24 10:02:59'),
(244, 4, '530b189d-c4c4-4f85-85e8-0c1567061d02', 10, 1, 1, '2014-02-24 10:02:59'),
(245, 5, '530b189d-c4c4-4f85-85e8-0c1567061d02', 13, 1, 1, '2014-02-24 10:02:59'),
(246, 6, '530b189d-c4c4-4f85-85e8-0c1567061d02', 16, 1, 1, '2014-02-24 10:02:59'),
(247, 7, '530b189d-c4c4-4f85-85e8-0c1567061d02', 19, 1, 1, '2014-02-24 10:02:59'),
(248, 8, '530b189d-c4c4-4f85-85e8-0c1567061d02', 22, 1, 1, '2014-02-24 10:02:59'),
(249, 9, '530b189d-c4c4-4f85-85e8-0c1567061d02', 25, 1, 1, '2014-02-24 10:02:59'),
(250, 10, '530b189d-c4c4-4f85-85e8-0c1567061d02', 28, 1, 1, '2014-02-24 10:02:59'),
(251, 1, '530b189d-c4c4-4f85-85e8-0c1567061d02', 2, 1, 2, '2014-02-24 10:05:33'),
(252, 2, '530b189d-c4c4-4f85-85e8-0c1567061d02', 4, 1, 2, '2014-02-24 10:05:34'),
(253, 3, '530b189d-c4c4-4f85-85e8-0c1567061d02', 9, 1, 2, '2014-02-24 10:05:34'),
(254, 4, '530b189d-c4c4-4f85-85e8-0c1567061d02', 10, 1, 2, '2014-02-24 10:05:34'),
(255, 5, '530b189d-c4c4-4f85-85e8-0c1567061d02', 13, 1, 2, '2014-02-24 10:05:34'),
(256, 6, '530b189d-c4c4-4f85-85e8-0c1567061d02', 18, 1, 2, '2014-02-24 10:05:34'),
(257, 7, '530b189d-c4c4-4f85-85e8-0c1567061d02', 20, 1, 2, '2014-02-24 10:05:34'),
(258, 8, '530b189d-c4c4-4f85-85e8-0c1567061d02', 24, 1, 2, '2014-02-24 10:05:34'),
(259, 9, '530b189d-c4c4-4f85-85e8-0c1567061d02', 25, 1, 2, '2014-02-24 10:05:34'),
(260, 10, '530b189d-c4c4-4f85-85e8-0c1567061d02', 28, 1, 2, '2014-02-24 10:05:34'),
(261, 1, '530fccf0-53d0-4c4a-ba91-164467061d02', 2, 1, 1, '2014-02-27 23:44:15'),
(262, 2, '530fccf0-53d0-4c4a-ba91-164467061d02', 5, 1, 1, '2014-02-27 23:44:15'),
(263, 3, '530fccf0-53d0-4c4a-ba91-164467061d02', 8, 1, 1, '2014-02-27 23:44:15'),
(264, 4, '530fccf0-53d0-4c4a-ba91-164467061d02', 10, 1, 1, '2014-02-27 23:44:15'),
(265, 5, '530fccf0-53d0-4c4a-ba91-164467061d02', 13, 1, 1, '2014-02-27 23:44:15'),
(266, 6, '530fccf0-53d0-4c4a-ba91-164467061d02', 16, 1, 1, '2014-02-27 23:44:15'),
(267, 7, '530fccf0-53d0-4c4a-ba91-164467061d02', 20, 1, 1, '2014-02-27 23:44:15'),
(268, 8, '530fccf0-53d0-4c4a-ba91-164467061d02', 23, 1, 1, '2014-02-27 23:44:15'),
(269, 9, '530fccf0-53d0-4c4a-ba91-164467061d02', 26, 1, 1, '2014-02-27 23:44:15'),
(270, 10, '530fccf0-53d0-4c4a-ba91-164467061d02', 29, 1, 1, '2014-02-27 23:44:15'),
(271, 1, '53fcfc5f-7760-418c-9263-57fa67061d02', 2, 1, 1, '2014-08-26 21:46:20'),
(272, 2, '53fcfc5f-7760-418c-9263-57fa67061d02', 4, 1, 1, '2014-08-26 21:46:20'),
(273, 3, '53fcfc5f-7760-418c-9263-57fa67061d02', 9, 1, 1, '2014-08-26 21:46:20'),
(274, 4, '53fcfc5f-7760-418c-9263-57fa67061d02', 10, 1, 1, '2014-08-26 21:46:20'),
(275, 5, '53fcfc5f-7760-418c-9263-57fa67061d02', 13, 1, 1, '2014-08-26 21:46:20'),
(276, 6, '53fcfc5f-7760-418c-9263-57fa67061d02', 18, 1, 1, '2014-08-26 21:46:20'),
(277, 7, '53fcfc5f-7760-418c-9263-57fa67061d02', 20, 1, 1, '2014-08-26 21:46:20'),
(278, 8, '53fcfc5f-7760-418c-9263-57fa67061d02', 24, 1, 1, '2014-08-26 21:46:20'),
(279, 9, '53fcfc5f-7760-418c-9263-57fa67061d02', 26, 1, 1, '2014-08-26 21:46:20'),
(280, 10, '53fcfc5f-7760-418c-9263-57fa67061d02', 28, 1, 1, '2014-08-26 21:46:20');

-- --------------------------------------------------------

--
-- Table structure for table `users_assessments_results`
--

CREATE TABLE IF NOT EXISTS `users_assessments_results` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `modules_id` int(11) NOT NULL,
  `users_id` varchar(50) NOT NULL,
  `attempt_group` int(1) NOT NULL DEFAULT '1',
  `score` decimal(5,2) NOT NULL DEFAULT '0.00',
  `dte_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `users_assessments_results`
--

INSERT INTO `users_assessments_results` (`id`, `modules_id`, `users_id`, `attempt_group`, `score`, `dte_created`) VALUES
(1, 1, '53027e45-2b98-4c2c-87d5-6fc567061d02', 1, '5.00', '2014-02-17 22:17:20'),
(2, 1, '53027e45-2b98-4c2c-87d5-6fc567061d02', 2, '2.00', '2014-02-17 22:51:14'),
(3, 1, '5302b359-8254-4f48-93cf-65ce67061d02', 1, '4.00', '2014-02-18 01:33:10'),
(5, 1, '5302bb4e-13bc-4cf7-8eeb-167d67061d02', 1, '4.00', '2014-02-18 01:46:25'),
(7, 1, '5302bf3f-7678-4c0d-b29d-2db967061d02', 1, '4.00', '2014-02-18 02:03:58'),
(8, 1, '5302b5d3-3f9c-4c29-a720-72e967061d02', 1, '9.00', '2014-02-18 02:57:13'),
(9, 1, '530474bb-c968-4a32-9ab7-3e1f67061d02', 1, '4.00', '2014-02-19 09:14:35'),
(10, 1, '5304685c-c978-4fb1-87ab-1ef967061d02', 1, '3.00', '2014-02-19 09:18:14'),
(12, 1, '5304685c-c978-4fb1-87ab-1ef967061d02', 2, '3.00', '2014-02-19 09:26:25'),
(13, 1, '5304685c-c978-4fb1-87ab-1ef967061d02', 3, '3.00', '2014-02-19 09:26:35'),
(14, 1, '530474bb-c968-4a32-9ab7-3e1f67061d02', 3, '4.00', '2014-02-19 09:26:47'),
(15, 1, '53046744-47e4-42d4-a734-1cb267061d02', 1, '2.00', '2014-02-19 09:33:51'),
(16, 1, '5304686e-1118-4cfc-920f-1cf867061d02', 1, '4.00', '2014-02-19 09:42:17'),
(17, 1, '5304686e-1118-4cfc-920f-1cf867061d02', 2, '4.00', '2014-02-19 09:43:20'),
(18, 1, '53046744-47e4-42d4-a734-1cb267061d02', 2, '10.00', '2014-02-19 10:02:17'),
(19, 1, '530466cd-3de8-458f-b695-1af667061d02', 1, '9.00', '2014-02-19 10:15:46'),
(20, 1, '530666f4-d880-4230-bc02-4ecd67061d02', 1, '3.00', '2014-02-20 20:39:04'),
(21, 1, '530666f1-e648-4d9e-98f4-4e5367061d02', 1, '7.00', '2014-02-20 20:39:06'),
(22, 1, '5306689c-fce4-4007-9bf2-551567061d02', 1, '3.00', '2014-02-20 20:42:41'),
(23, 1, '53066c0e-4efc-4dcd-adee-5f1467061d02', 1, '5.00', '2014-02-20 21:02:31'),
(24, 1, '53066c0e-4efc-4dcd-adee-5f1467061d02', 2, '6.00', '2014-02-20 23:43:12'),
(25, 1, '530b189d-c4c4-4f85-85e8-0c1567061d02', 1, '4.00', '2014-02-24 10:02:59'),
(26, 1, '530b189d-c4c4-4f85-85e8-0c1567061d02', 2, '9.00', '2014-02-24 10:05:34'),
(27, 1, '530fccf0-53d0-4c4a-ba91-164467061d02', 1, '5.00', '2014-02-27 23:44:15'),
(28, 1, '53fcfc5f-7760-418c-9263-57fa67061d02', 1, '10.00', '2014-08-26 21:46:20');

-- --------------------------------------------------------

--
-- Table structure for table `users_backup`
--

CREATE TABLE IF NOT EXISTS `users_backup` (
  `id` varchar(36) NOT NULL,
  `modules_id` int(10) NOT NULL DEFAULT '1',
  `username` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `password` varchar(128) DEFAULT NULL,
  `password_token` varchar(128) DEFAULT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `address` text,
  `registration_number` varchar(50) NOT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `email_verified` tinyint(1) DEFAULT '0',
  `email_token` varchar(255) DEFAULT NULL,
  `email_token_expires` datetime DEFAULT NULL,
  `tos` tinyint(1) DEFAULT '0',
  `active` tinyint(1) DEFAULT '0',
  `has_paid` smallint(1) NOT NULL DEFAULT '0',
  `has_surveyed` smallint(1) NOT NULL DEFAULT '0',
  `has_generated_result` smallint(1) NOT NULL DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `last_action` datetime DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT '0',
  `assessment_attempt` smallint(1) NOT NULL DEFAULT '0',
  `assessment_score` smallint(2) NOT NULL DEFAULT '0',
  `assessment_link_sent` smallint(1) NOT NULL DEFAULT '0',
  `role` varchar(255) DEFAULT NULL,
  `date_paid` timestamp NULL DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `BY_USERNAME` (`username`),
  KEY `module_id` (`modules_id`),
  KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_backup`
--

INSERT INTO `users_backup` (`id`, `modules_id`, `username`, `slug`, `password`, `password_token`, `first_name`, `last_name`, `company_name`, `address`, `registration_number`, `mobile`, `phone`, `email`, `email_verified`, `email_token`, `email_token_expires`, `tos`, `active`, `has_paid`, `has_surveyed`, `has_generated_result`, `last_login`, `last_action`, `is_admin`, `assessment_attempt`, `assessment_score`, `assessment_link_sent`, `role`, `date_paid`, `created`, `modified`) VALUES
('52d91359-efdc-4568-b3ae-09667a3d4a97', 1, 'admin', 'admin', '736b4e436327e5d5afd4b79056bb0b10ea4a9e08', 'f696bfa15b615c64562afa34400f326de0b905c4', 'Admin', 'Admin', NULL, NULL, '', '0211234567', '1234567', 'gn@livingflame.co.nz', 1, NULL, NULL, 1, 1, 0, 0, 0, '2014-08-27 09:19:05', NULL, 1, 0, 0, 0, 'user_registered', NULL, '2014-01-18 00:26:17', '2014-08-27 09:19:05'),
('530666f1-e648-4d9e-98f4-4e5367061d02', 1, 'sudhanshuy1w927aknm1392928497', 'sudhanshuy1w927aknm1392928497', 'f8b308d79c70ccf055dc35a74c8bb29761ab5879', 'sudhanshuajbg2wkcth1392928497', 'sudhanshu', 'dandekar', 'living flame', '343 church st\r\npenrose\r\nauckland', '.', '0211778439', '6221148', 'sd@livingflame.co.nz', 0, 'idj5r13cvf', '2014-02-22 09:34:57', 0, 0, 1, 1, 1, NULL, NULL, 0, 1, 7, 1, 'user_registered', '2014-02-20 20:37:44', '2014-02-21 09:34:57', '2014-02-21 09:39:58'),
('53066c0e-4efc-4dcd-adee-5f1467061d02', 1, 'Glena3xh9zqfyj1392929806', 'glena3xh9zqfyj1392929806', 'f8acaaeab447c9586ef961aa4855f570b410600c', 'Gleneqp0hgjfyn1392929806', 'Glen', 'Nichols', 'Living Flame,', '343 Church Street\r\nOnehunga\r\n', '.', '021 833 144', '622 1148', 'gn@livingflame.co.nz', 0, 'o312fipjm9', '2014-02-22 09:56:46', 0, 0, 1, 0, 1, NULL, NULL, 0, 2, 6, 1, 'user_registered', '2014-02-20 21:00:13', '2014-02-21 09:56:46', '2014-02-21 12:43:12'),
('536953c3-2e4c-4054-8061-12d567061d02', 1, 'Janlpbo0k9weg1399411651', 'janlpbo0k9weg1399411651', '0ca066f3ce508f847316b63f9af88a7727f18440', 'Janj0ghyidmwl1399411651', 'Jan', 'Stephens', 'Living Flame', '343b Church Street\r\nPenrose', '0001', '64211161628', '64211161628', 'js@livingflame.co.nz', 0, 'x5vqy3a1tz', '2014-05-08 09:27:31', 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 'user_registered', NULL, '2014-05-07 09:27:31', '2014-05-07 09:27:31'),
('536a1269-44d0-4827-8333-2eff67061d02', 1, 'shaungcjbnaftpq1399460457', 'shaungcjbnaftpq1399460457', '283ec5ccdde39f7a6729a50d894a0c5e20430994', 'shaunm6pgjqrsac1399460457', 'shaun ', 'meiklejohn', 'certified plumbing and gas', '22 Connell street\r\nblockhouse bay\r\nAuckland', '12914', '0274972703', '', 'shaun@cpgas.co.nz', 0, 'xwvs92ojy8', '2014-05-08 23:00:57', 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 'user_registered', NULL, '2014-05-07 23:00:57', '2014-05-07 23:00:57'),
('53fb26f9-7ea0-4822-aa01-735867061d02', 1, 'Geet15sfpibdev1408968441', 'geet15sfpibdev1408968441', '0335cb63fcacfda1b27ebb80ec3a2b3d19f4c66d', 'Geetl8gcho3n7p1408968441', 'Geet', 'pHANSE', 'Testing Module 21', 'Testing Module 21', 'Testing Module 21', '25896348', '454987484', 'geetz13@msn.com', 0, '1x3grlknce', '2014-08-27 00:07:21', 0, 0, 1, 0, 0, NULL, NULL, 0, 0, 0, 1, 'user_registered', '2014-08-25 12:08:26', '2014-08-26 00:07:21', '2014-08-26 00:07:21');

-- --------------------------------------------------------

--
-- Table structure for table `users_certificates`
--

CREATE TABLE IF NOT EXISTS `users_certificates` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `users_id` varchar(100) NOT NULL,
  `dte_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_id` (`users_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users_certificates`
--

INSERT INTO `users_certificates` (`id`, `users_id`, `dte_created`) VALUES
(1, '5302b5d3-3f9c-4c29-a720-72e967061d02', '2014-02-18 02:57:13'),
(2, '53046744-47e4-42d4-a734-1cb267061d02', '2014-02-19 10:02:17'),
(3, '530466cd-3de8-458f-b695-1af667061d02', '2014-02-19 10:15:46'),
(4, '530666f1-e648-4d9e-98f4-4e5367061d02', '2014-02-20 20:39:06'),
(5, '530b189d-c4c4-4f85-85e8-0c1567061d02', '2014-02-24 10:05:34'),
(6, '53fcfc5f-7760-418c-9263-57fa67061d02', '2014-08-26 21:46:20');

-- --------------------------------------------------------

--
-- Table structure for table `users_surveyed_choices`
--

CREATE TABLE IF NOT EXISTS `users_surveyed_choices` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `users_id` varchar(100) NOT NULL,
  `surveys_id` int(10) NOT NULL,
  `surveys_choices_id` int(10) NOT NULL,
  `dte_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_id` (`users_id`,`surveys_id`),
  KEY `users_id_2` (`users_id`,`surveys_id`,`surveys_choices_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `users_surveyed_choices`
--

INSERT INTO `users_surveyed_choices` (`id`, `users_id`, `surveys_id`, `surveys_choices_id`, `dte_created`) VALUES
(1, '5302b5d3-3f9c-4c29-a720-72e967061d02', 1, 1, '2014-02-18 03:04:06'),
(2, '5302b5d3-3f9c-4c29-a720-72e967061d02', 2, 2, '2014-02-18 03:04:06'),
(3, '5302b5d3-3f9c-4c29-a720-72e967061d02', 3, 3, '2014-02-18 03:04:06'),
(4, '5302b5d3-3f9c-4c29-a720-72e967061d02', 4, 4, '2014-02-18 03:04:06'),
(5, '5302b5d3-3f9c-4c29-a720-72e967061d02', 5, 5, '2014-02-18 03:04:06'),
(6, '5302b5d3-3f9c-4c29-a720-72e967061d02', 6, 4, '2014-02-18 03:04:06'),
(7, '5302b5d3-3f9c-4c29-a720-72e967061d02', 7, 3, '2014-02-18 03:04:06'),
(8, '53046744-47e4-42d4-a734-1cb267061d02', 1, 1, '2014-02-19 10:06:21'),
(9, '53046744-47e4-42d4-a734-1cb267061d02', 2, 2, '2014-02-19 10:06:21'),
(10, '53046744-47e4-42d4-a734-1cb267061d02', 3, 2, '2014-02-19 10:06:21'),
(11, '53046744-47e4-42d4-a734-1cb267061d02', 4, 3, '2014-02-19 10:06:21'),
(12, '53046744-47e4-42d4-a734-1cb267061d02', 5, 3, '2014-02-19 10:06:21'),
(13, '53046744-47e4-42d4-a734-1cb267061d02', 6, 2, '2014-02-19 10:06:21'),
(14, '53046744-47e4-42d4-a734-1cb267061d02', 7, 2, '2014-02-19 10:06:21'),
(15, '530666f1-e648-4d9e-98f4-4e5367061d02', 1, 1, '2014-02-20 20:39:58'),
(16, '530666f1-e648-4d9e-98f4-4e5367061d02', 2, 2, '2014-02-20 20:39:58'),
(17, '530666f1-e648-4d9e-98f4-4e5367061d02', 3, 2, '2014-02-20 20:39:58'),
(18, '530666f1-e648-4d9e-98f4-4e5367061d02', 4, 3, '2014-02-20 20:39:58'),
(19, '530666f1-e648-4d9e-98f4-4e5367061d02', 5, 2, '2014-02-20 20:39:58'),
(20, '530666f1-e648-4d9e-98f4-4e5367061d02', 6, 3, '2014-02-20 20:39:58'),
(21, '530666f1-e648-4d9e-98f4-4e5367061d02', 7, 5, '2014-02-20 20:39:58'),
(22, '530b189d-c4c4-4f85-85e8-0c1567061d02', 1, 1, '2014-02-24 10:07:14'),
(23, '530b189d-c4c4-4f85-85e8-0c1567061d02', 2, 1, '2014-02-24 10:07:14'),
(24, '530b189d-c4c4-4f85-85e8-0c1567061d02', 3, 1, '2014-02-24 10:07:14'),
(25, '530b189d-c4c4-4f85-85e8-0c1567061d02', 4, 2, '2014-02-24 10:07:14'),
(26, '530b189d-c4c4-4f85-85e8-0c1567061d02', 5, 2, '2014-02-24 10:07:14'),
(27, '530b189d-c4c4-4f85-85e8-0c1567061d02', 6, 2, '2014-02-24 10:07:14'),
(28, '530b189d-c4c4-4f85-85e8-0c1567061d02', 7, 2, '2014-02-24 10:07:14');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE IF NOT EXISTS `user_details` (
  `id` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `position` float NOT NULL DEFAULT '1',
  `field` varchar(60) NOT NULL,
  `value` text,
  `input` varchar(16) NOT NULL,
  `data_type` varchar(16) NOT NULL,
  `label` varchar(128) NOT NULL DEFAULT '',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_PROFILE_PROPERTY` (`field`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
