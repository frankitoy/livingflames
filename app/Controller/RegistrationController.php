<?php
/**
 * Created by IntelliJ IDEA.
 * User: frankitoy
 * Date: 9/01/14
 * Time: 12:07 AM
 * To change this template use File | Settings | File Templates.
 */
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Registration Controller
 *
 * @package       Registration
 * @property	  SessionComponent $Session
 * @property	  CookieComponent $Cookie
 * @property	  PaginatorComponent $Paginator
 * @property	  SecurityComponent $Security
 */
class RegistrationController extends AppController {

    public $name = 'Registration';

    public $uses = array('User','Modules','Emails');
    /**
     * Helpers
     *
     * @var array
     */
    public $helpers = array(
        'Html',
        'Form',
        'Session',
        'Time',
        'Text'
    );

    /**
     * Components
     *
     * @var array
     */
    public $components = array(
        'Session',
        'Cookie',
        'Paginator',
        'Security',
        'Search.Prg'
    );

    /**
     * Preset vars
     *
     * @var array $presetVars
     * @link https://github.com/CakeDC/search
     */
    public $presetVars = true;

    /**
     * Constructor
     *
     * @param CakeRequest $request Request object for this controller. Can be null for testing,
     *  but expect that features that use the request parameters will not work.
     * @param CakeResponse $response Response object for this controller.
     */
    public function __construct($request, $response) {
        $this->_setupComponents();
        parent::__construct($request, $response);
        $this->_reInitControllerName();
    }

    /**
     * Providing backward compatibility to a fix that was just made recently to the core
     * for users that want to upgrade the plugin but not the core
     *
     * @link http://cakephp.lighthouseapp.com/projects/42648-cakephp/tickets/3550-inherited-controllers-get-wrong-property-names
     * @return void
     */
    protected function _reInitControllerName() {
        $name = substr(get_class($this), 0, -10);
        if ($this->name === null) {
            $this->name = $name;
        } elseif ($name !== $this->name) {
            $this->name = $name;
        }
    }

    /**
     * Returns $this->plugin with a dot, used for plugin loading using the dot notation
     *
     * @return mixed string|null
     */
    protected function _pluginDot() {
        if (is_string($this->plugin)) {
            return $this->plugin . '.';
        }
        return $this->plugin;
    }

    /**
     * Wrapper for CakePlugin::loaded()
     *
     * @param string $plugin
     * @param bool $exception
     * @throws MissingPluginException
     * @return boolean
     */
    protected function _pluginLoaded($plugin, $exception = true) {
        $result = CakePlugin::loaded($plugin);
        if ($exception === true && $result === false) {
            throw new MissingPluginException(array('plugin' => $plugin));
        }
        return $result;
    }

    /**
     * Setup components based on plugin availability
     *
     * @return void
     * @link https://github.com/CakeDC/search
     */
    protected function _setupComponents() {
        if ($this->_pluginLoaded('Search', false)) {
            $this->components[] = 'Search.Prg';
        }
    }

    /**
     * beforeFilter callback
     *
     * @return void
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->_setupPagination();
        $this->set('model', $this->modelClass);
    }

    /**
     * Sets the default pagination settings up
     *
     * Override this method or the index action directly if you want to change
     * pagination settings.
     *
     * @return void
     */
    protected function _setupPagination() {
        $this->Paginator->settings = array(
            'limit' => 12,
            'conditions' => array(
                $this->modelClass . '.active' => 1,
                $this->modelClass . '.email_verified' => 1
            )
        );
    }

    /**
     * Sends the verification email
     *
     * This method is protected and not private so that classes that inherit this
     * controller can override this method to change the verification mail sending
     * in any possible way.
     *
     * @param $userData
     * @param array $options EmailComponent options
     * @internal param string $to Receiver email address
     * @return void
     */
    protected function _sendVerificationEmail($userData, $options = array()) {
        $this->layout = 'email';
        $defaults = array(
            'from' => Configure::read('App.defaultEmail'),
            'subject' => Configure::read('App.defaultEmailRegistrationSuccess'),
            'template' => $this->_pluginDot() . 'account_verification',
            'layout' => 'default',
            'emailFormat' => CakeEmail::MESSAGE_HTML
        );
        $options = array_merge($defaults, $options);

        $Email = $this->_getMailInstance();
        $emailMessage = $Email->to($userData[$this->modelClass]['email'])
            ->bcc(Configure::read('App.developer'))
            ->from($options['from'])
            ->emailFormat($options['emailFormat'])
            ->subject($options['subject'])
            ->template($options['template'], $options['layout'])
            ->viewVars(array(
                'model' => $this->modelClass,
                'user' => $userData
            ))
            ->send();

        $data = array();
        $this->Emails->create();
        $data['Emails']['emails_type_id'] = 1;
        $data['Emails']['from_email'] = ($options['from']);
        $data['Emails']['to_email'] = $userData[$this->modelClass]['email'];
        $data['Emails']['subject'] = Configure::read('App.defaultEmailRegistrationSuccess');
        $data['Emails']['content'] = $emailMessage['message'];
        $data['Emails']['dte_created'] = date("Y-m-d H:i:s");
        $this->Emails->save($data);
    }

    /*
     * @param $slug
     */
    public function index($slug = null) {

        if ($slug === null) {
            $slug = 'flueing-and-downdraught';
        }

        $modules = $this->Modules->findBySlug($slug);
        if (!$modules || $modules['Modules']['is_active'] < 1) {
            throw new NotFoundException();
        }

        $title_for_layout = "Registration - Living Flame";
        $name = $this->name;

        if (!empty($this->request->data)) {

            $this->request->data['User']['username']  = str_replace(" ","",$this->request->data['User']['first_name']) . $this->{$this->modelClass}->generateToken() . strtotime("now");
            $this->request->data['User']['password']  = str_replace(" ","",$this->request->data['User']['first_name']) . $this->{$this->modelClass}->generateToken() . strtotime("now");
            $this->request->data['User']['temppassword'] = $this->request->data['User']['password'];
            $this->request->data['User']['password_token'] = $this->request->data['User']['password'];
            $this->request->data['User']['modules_id'] = $modules['Modules']['id'];

            $user = $this->{$this->modelClass}->register($this->request->data);
            if ($user !== false) {

                $Event = new CakeEvent(
                    'Users.Controller.Users.afterRegistration',
                    $this,
                    array(
                        'data' => $this->request->data,
                    )
                );

                $this->getEventManager()->dispatch($Event);
                if ($Event->isStopped()) {
                    $this->redirect(array('action' => 'registration', $slug));
                }

                $this->_sendVerificationEmail($this->{$this->modelClass}->data);
                $this->Session->setFlash(__d('users', Configure::read('App.defaultRegistrationMessage')), 'default', array('class' => 'col-sm-7 col-sm-offset-2 alert alert-success fade in'));
                $this->redirect(array('controller' => 'registration', 'action' => 'index', $slug));
            } else {
                $this->set('validationErrorsArray', $this->{$this->modelClass}->invalidFields());
                unset($this->request->data[$this->modelClass]['password']);
                unset($this->request->data[$this->modelClass]['temppassword']);
                $this->Session->setFlash(__d('users', 'Your registration could not be completed. Please, try again.'), 'default', array('class' => 'col-sm-7 col-sm-offset-2 alert alert-danger fade in'));
            }
        }

		$action = 'registration';
        $this->set(compact('title_for_layout','name','modules','action'));
    }

    /**
     * Returns a CakeEmail object
     *
     * @return object CakeEmail instance
     * @link http://book.cakephp.org/2.0/en/core-utility-libraries/email.html
     */
    protected function _getMailInstance() {
        $emailConfig = Configure::read('Users.emailConfig');
        if ($emailConfig) {
            return new CakeEmail($emailConfig);
        } else {
            return new CakeEmail('default');
        }
    }

    public function success() {
        $title_for_layout = "Registration - Living Flame";
        $name = $this->name;
    }
}