<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('Modules', 'Surveys', 'SurveysChoices', 'Assessments');

    /**
     * @throws NotFoundException
     * @throws Exception|MissingViewException
     */
    public function display($moduleSlugId = null)
    {

        $path = func_get_args();
        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }

        $page = $subpage = $title_for_layout = null;
        if (!empty($path[0])) {
            $page = $path[0];
        }

        if (!empty($path[1])) {
            $subpage = $path[1];
        }

        if (!empty($path[$count - 1])) {
            $title_for_layout = Inflector::humanize($path[$count - 1]);
        }

        if ($moduleSlugId == null) {
            $moduleSlugId = 16;
        }

        $this->Modules->contain();
        $modules = $this->Modules->findByModuleSlugId($moduleSlugId);
        if (!$modules) {
            throw new NotFoundException();
        }

        $title_for_layout = "Home - Living Flame";
        $name = $this->name;
        $this->set(compact('page', 'subpage', 'title_for_layout', 'name', 'modules'));
    }

    public function index()
    {
        $this->Modules->contain();
        $modules = $this->Modules->find('all');
        if (!$modules) {
          throw new NotFoundException();
        }

        $title_for_layout = "Home - Living Flame";
        $name = $this->name;
        $this->set(compact('page', 'subpage', 'title_for_layout', 'name', 'modules'));
    }
}
