<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class SurveysController extends AppController {

    /**
     * Controller name
     *
     * @var string
     */
    public $name = 'Surveys';

    public $uses = array(
      'User',
      'Surveys',
      'SurveysChoices',
      'UsersSurveyedChoices',
      'UsersSurveyedComments',
      'Emails'
    );

    /**
     * If the controller is a plugin controller set the plugin name
     *
     * @var mixed
     */
    public $plugin = null;

    /**
     * Helpers
     *
     * @var array
     */
    public $helpers = array(
        'Html',
        'Form',
        'Session',
        'Time',
        'Text',
        'User'
    );

    /**
     * Components
     *
     * @var array
     */
    public $components = array(
        'Auth',
        'Session',
        'Cookie',
        'Paginator'
    );

    /**
     * Preset vars
     *
     * @var array $presetVars
     * @link https://github.com/CakeDC/search
     */
    public $presetVars = true;

    /**
     * Constructor
     *
     * @param CakeRequest $request Request object for this controller. Can be null for testing,
     *  but expect that features that use the request parameters will not work.
     * @param CakeResponse $response Response object for this controller.
     */
    public function __construct($request, $response) {
        parent::__construct($request, $response);
        $this->_reInitControllerName();
    }

    /**
     * Providing backward compatibility to a fix that was just made recently to the core
     * for users that want to upgrade the plugin but not the core
     *
     * @link http://cakephp.lighthouseapp.com/projects/42648-cakephp/tickets/3550-inherited-controllers-get-wrong-property-names
     * @return void
     */
    protected function _reInitControllerName() {
        $name = substr(get_class($this), 0, -10);
        if ($this->name === null) {
            $this->name = $name;
        } elseif ($name !== $this->name) {
            $this->name = $name;
        }
    }

    /**
     * Returns $this->plugin with a dot, used for plugin loading using the dot notation
     *
     * @return mixed string|null
     */
    protected function _pluginDot() {
        if (is_string($this->plugin)) {
            return $this->plugin . '.';
        }
        return $this->plugin;
    }

    /**
     * Wrapper for CakePlugin::loaded()
     *
     * @param string $plugin
     * @param bool $exception
     * @throws MissingPluginException
     * @return boolean
     */
    protected function _pluginLoaded($plugin, $exception = true) {
        $result = CakePlugin::loaded($plugin);
        if ($exception === true && $result === false) {
            throw new MissingPluginException(array('plugin' => $plugin));
        }
        return $result;
    }

    /**
     * beforeFilter callback
     *
     * @return void
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->_setupAuth();
        $this->set('model', $this->modelClass);
    }

    /**
     * Setup Authentication Component
     *
     * @return void
     */
    protected function _setupAuth() {
        $this->Auth->allow('survey');
        $this->Auth->deny('index');
        $this->Auth->loginRedirect = '/surveys';
        $this->Auth->loginAction = array('admin' => false, 'plugin' => Inflector::underscore($this->plugin), 'controller' => 'users', 'action' => 'login');
    }

    /**
     * Simple listing of all users
     *
     * @return void
     */
    public function graph() {
        $name = 'Survey';
        $action = $this->action;
        $this->SurveysChoices->contain();
        $surveysChoices = $this->SurveysChoices->find('all');
        $surveys = $this->Surveys->find('all');
        $isLoginRequired = 1;
        $model = 'Surveys';
        $this->set('surveys', $this->Paginator->paginate($this->Surveys));
        $this->set(compact('name','model','action','surveys','surveysChoices','isLoginRequired'));
    }

    public function index() {
      $name = 'Survey';
      $action = $this->action;
      $this->Paginator->settings = array(
          'joins' => array(
              array(
                  'table' => 'users_surveyed_choices',
                  'alias' => 'UsersSurveyedChoices',
                  'type' => 'INNER',
                  'conditions' => array(
                      'UsersSurveyedChoices.users_id = User.id'
                  )
              )
          ),
          'fields' => array('UsersSurveyedChoices.*', 'User.*, Modules.*'),
          'order'  => 'UsersSurveyedChoices.dte_created DESC',
          'group'  => 'UsersSurveyedChoices.users_id'
      );


      $this->set('users', $this->Paginator->paginate('User'));
      $isLoginRequired = 1;
      $model = 'Surveys';
      $surveys = $this->Surveys->find('list', array(
        'fields' => array('Surveys.id', 'Surveys.question'),
        'recursive' => 0
      ));

      $surveyChoices = $this->SurveysChoices->find('list', array(
        'fields' => array('SurveysChoices.id', 'SurveysChoices.choice'),
        'recursive' => 0
      ));
      $this->set(compact('name','model','action','isLoginRequired','surveys', 'surveyChoices'));
    }

    /**
     * Default isAuthorized method
     *
     * This is called to see if a user (when logged in) is able to access an action
     *
     * @param array $user
     * @return boolean True if allowed
     * @link http://book.cakephp.org/2.0/en/core-libraries/components/authentication.html#using-controllerauthorize
     */
    public function isAuthorized($user = null) {
        return parent::isAuthorized($user);
    }

    /**
     * Sends the survey email
     *
     * This method is protected and not private so that classes that inherit this
     * controller can override this method to change the verification mail sending
     * in any possible way.
     *
     * @param $userData
     * @param array $options
     * @internal param array $options EmailComponent options
     * @internal param string $to Receiver email address
     * @return void
     */
    protected function _sendSurveyEmail($userData, $options = array()) {
        $defaults = array(
            'from' => Configure::read('App.defaultEmail'),
            'subject' => 'New survey entered.',
            'template' => $this->_pluginDot() . 'survey',
            'layout' => 'default',
            'emailFormat' => CakeEmail::MESSAGE_HTML
        );
        $options = array_merge($defaults, $options);

        $Email = $this->_getMailInstance();
        $emailMessage = $Email->to(Configure::read('App.defaultEmail'))
            ->bcc(Configure::read('App.developer'))
            ->from($options['from'])
            ->emailFormat($options['emailFormat'])
            ->subject($options['subject'])
            ->template($options['template'], $options['layout'])
            ->viewVars(array(
                'user' => $userData
            ))
            ->send();

        $data = array();
        $this->Emails->create();
        $data['Emails']['emails_type_id'] = 3;
        $data['Emails']['from_email'] = $options['from'];
        $data['Emails']['to_email'] = $options['from'];
        $data['Emails']['subject'] = 'New survey entered.';
        $data['Emails']['content'] = $emailMessage['message'];
        $data['Emails']['dte_created'] = date("Y-m-d H:i:s");
        $this->Emails->save($data);
    }

    /**
     * Returns a CakeEmail object
     *
     * @return object CakeEmail instance
     * @link http://book.cakephp.org/2.0/en/core-utility-libraries/email.html
     */
    protected function _getMailInstance() {
        $emailConfig = Configure::read('Users.emailConfig');
        if ($emailConfig) {
            return new CakeEmail($emailConfig);
        } else {
            return new CakeEmail('default');
        }
    }

    /**
     *
     * Send an email to livingflames that certain person send a survey
     *
     */
    public function survey() {
        $slug = trim($this->request->query['token']);
        $user = $this->User->findById($slug);
        $this->UsersSurveyedChoices->contain();
        $userSurveyExist = $this->UsersSurveyedChoices->findByUsersId($slug);
        if (!$user || $userSurveyExist || $user['User']['has_paid'] < 1 || $user['User']['assessment_attempt'] < 1) {
            throw new NotFoundException();
        }

        $surveys = $this->Surveys->find('all');
        if (!empty($this->request->data)) {
            if (isset($this->request->data['UsersSurveyedChoices']['surveys_choices_id']) && count($this->request->data['UsersSurveyedChoices']['surveys_choices_id']) == count($surveys)) {
                foreach ($this->request->data['UsersSurveyedChoices']['surveys_choices_id'] as $choice_id => $choice_value ){
                    $data = array();
                    $this->UsersSurveyedChoices->create();
                    $data['UsersSurveyedChoices']['users_id'] = $this->request->data['UsersSurveyedChoices']['users_id'][0];
                    $data['UsersSurveyedChoices']['modules_id'] = $this->request->data['UsersSurveyedChoices']['modules_id'][0];
                    $data['UsersSurveyedChoices']['surveys_id'] = $choice_id;
                    $data['UsersSurveyedChoices']['surveys_choices_id'] = $choice_value;
                    $data['UsersSurveyedChoices']['dte_created'] = date("Y-m-d H:i:s");
                    $this->UsersSurveyedChoices->save($data);
                }

                $postDataComments = array();
                $this->UsersSurveyedComments->create();
                $postDataComments['UsersSurveyedComments']['users_id'] = $this->request->data['UsersSurveyedChoices']['users_id'][0];
                $postDataComments['UsersSurveyedComments']['modules_id'] = $this->request->data['UsersSurveyedChoices']['modules_id'][0];
                $postDataComments['UsersSurveyedComments']['comment'] = $this->request->data['UsersSurveyedChoices']['comment'];
                $postDataComments['UsersSurveyedComments']['dte_created'] = date("Y-m-d H:i:s");
                $this->UsersSurveyedComments->save($postDataComments, false);

                $this->_sendSurveyEmail($user);//send email to livingflames admin

                $postData = array();
                $postData['User']['id'] = $user['User']['id'];
                $postData['User']['has_surveyed'] = 1;
                $postData['User']['modified'] = date("Y-m-d H:i:s");
                $this->User->save($postData, false);
                $this->Session->setFlash(__d('users', Configure::read('App.defaultSurveyMessage')), 'default', array('class' => 'col-md-offset-3 add-padding alert alert-success fade in'));
                $this->redirect("/module/".$user['Modules']['module_slug_id']);
            } else {
                $this->Session->setFlash(__d('users', 'Survey form could not be completed. Please, try again.'), 'default', array('class' => 'col-md-9 col-md-offset-2 add-padding alert alert-danger fade in'));
            }
        }

        $name = 'Survey';
        $action = $this->action;
        $model = 'UsersSurveyedChoices';
        $surveysChoices = $this->SurveysChoices->find('all');
        $isLoginRequired = 0;
        $this->set(compact('name','model','user','action','surveys','surveysChoices','isLoginRequired'));
    }
}
