<?php

App::import('Core', 'Folder');
App::import('Core', 'File');
App::uses('AppController', 'Controller');

class DownloadController extends AppController {

    /**
     * Controller name
     *
     * @var string
     */
    public $name = 'Download';

    /**
     * @var array
     */
    public $uses = array('User', 'Modules');

    /**
     * If the controller is a plugin controller set the plugin name
     *
     * @var mixed
     */
    public $plugin = null;

    /**
     * Helpers
     *
     * @var array
     */
    public $helpers = array(
        'Html',
        'Form',
        'Session',
        'Time',
        'Text',
		'Pdf'
    );

    /**
     * Components
     *
     * @var array
     */
    public $components = array(
        'Session',
        'Cookie',
    );

    /**
     * Constructor
     *
     * @param CakeRequest $request Request object for this controller. Can be null for testing,
     *  but expect that features that use the request parameters will not work.
     * @param CakeResponse $response Response object for this controller.
     */
    public function __construct($request, $response) {
        parent::__construct($request, $response);
        $this->_reInitControllerName();
    }

    /**
     * Providing backward compatibility to a fix that was just made recently to the core
     * for users that want to upgrade the plugin but not the core
     *
     * @link http://cakephp.lighthouseapp.com/projects/42648-cakephp/tickets/3550-inherited-controllers-get-wrong-property-names
     * @return void
     */
    protected function _reInitControllerName() {
        $name = substr(get_class($this), 0, -10);
        if ($this->name === null) {
            $this->name = $name;
        } elseif ($name !== $this->name) {
            $this->name = $name;
        }
    }

    /**
     * beforeFilter callback
     *
     * @return void
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->set('model', $this->modelClass);
    }

    /**
     *
     */
    public function pdf() {
        $this->autoRender = false;
        $sourcePdf = Configure::read('downloadLink');
        $slug = trim($this->request->params['pass'][0]);
        $this->User->contain();
        $user = $this->User->findById($slug);
        if (!$user || count($user) < 1 || $user['User']['has_paid'] < 1) {
            throw new NotFoundException();
        }

        list($moduleName, $moduleSlug) = explode("_", $this->request->params['pass'][1]);
        $this->Modules->contain();
        $module = $this->Modules->findBySlug(strtolower(str_replace(".pdf", "",$moduleSlug)));
        if (!$module || count($module) < 1) {
            throw new NotFoundException();
        }

        $sourcePdf .= DS . $module['Modules']['id'] . '.pdf';
        $rawSize = filesize($sourcePdf);


        header("HTTP/1.1 200 OK");
        header("Accept-Ranges: bytes");
        header('Content-type: application/pdf');
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-disposition: attachment; filename=" . $this->request->params['pass'][1]);
        header("Content-Length: " . $rawSize);
        flush();
        $fp = fopen($sourcePdf, "r");
        while (!feof($fp)) {
            echo fread($fp, 65536);
            flush(); // this is essential for large downloads
        }
        fclose($fp);
        exit;
    }
}