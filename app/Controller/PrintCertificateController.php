<?php
error_reporting(E_ERROR);
App::uses('AppController', 'Controller');

class PrintCertificateController extends AppController {

    /**
     * Controller name
     *
     * @var string
     */
    public $name = 'PrintCertificate';

    /**
     * @var array
     */
    public $uses = array(
        'UsersCertificates'
    );

    /**
     * Helpers
     *
     * @var array
     */
    public $helpers = array(
        'Html',
        'Form',
        'Session',
        'Time',
        'Text',
        'Pdf'
    );

    /**
     * Components
     *
     * @var array
     */
    public $components = array(
        'Session',
        'Cookie',
    );

    /**
     * Constructor
     *
     * @param CakeRequest $request Request object for this controller. Can be null for testing,
     *  but expect that features that use the request parameters will not work.
     * @param CakeResponse $response Response object for this controller.
     */
    public function __construct($request, $response) {
        parent::__construct($request, $response);
        $this->_reInitControllerName();
    }

    /**
     * Providing backward compatibility to a fix that was just made recently to the core
     * for users that want to upgrade the plugin but not the core
     *
     * @link http://cakephp.lighthouseapp.com/projects/42648-cakephp/tickets/3550-inherited-controllers-get-wrong-property-names
     * @return void
     */
    protected function _reInitControllerName() {
        $name = substr(get_class($this), 0, -10);
        if ($this->name === null) {
            $this->name = $name;
        } elseif ($name !== $this->name) {
            $this->name = $name;
        }
    }

    /**
     * Returns $this->plugin with a dot, used for plugin loading using the dot notation
     *
     * @return mixed string|null
     */
    protected function _pluginDot() {
        if (is_string($this->plugin)) {
            return $this->plugin . '.';
        }
        return $this->plugin;
    }

    /**
     * Wrapper for CakePlugin::loaded()
     *
     * @param string $plugin
     * @param bool $exception
     * @throws MissingPluginException
     * @return boolean
     */
    protected function _pluginLoaded($plugin, $exception = true) {
        $result = CakePlugin::loaded($plugin);
        if ($exception === true && $result === false) {
            throw new MissingPluginException(array('plugin' => $plugin));
        }
        return $result;
    }

    /**
     * beforeFilter callback
     *
     * @return void
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->set('model', $this->modelClass);
    }

    public function index() {
		$this->layout = 'pdf';

        $slug = trim($this->request->query['token']);

        $this->UsersCertificates->bindModel(array(
            'hasOne' => array(
                'Modules' => array(
                    'foreignKey' => false,
                    'conditions' => array('Modules.id = User.modules_id')
                )
            )
        ));

        $user = $this->UsersCertificates->findByUsersId($slug);
        if (!$user || $user['User']['has_paid'] < 1) {
            throw new NotFoundException();
        }

        $token = $user['User']['id'];
        $this->set(compact('user','token'));
    }
}
