<?php

App::uses('AppController', 'Controller');

class ModulesController extends AppController {

    /**
     * Controller name
     *
     * @var string
     */
    public $name = 'Modules';

    /**
     * @var array
     */
    public $uses = array('Modules','Assessments');

    /**
     * If the controller is a plugin controller set the plugin name
     *
     * @var mixed
     */
    public $plugin = null;

    /**
     * Helpers
     *
     * @var array
     */
    public $helpers = array(
        'Html',
        'Form',
        'Session',
        'Time',
        'Text'
    );

    /**
     * Components
     *
     * @var array
     */
    public $components = array(
        'Auth',
        'Session',
        'Cookie',
        'Paginator',
        'Security',
        'Search.Prg'
    );

    /**
     * Preset vars
     *
     * @var array $presetVars
     * @link https://github.com/CakeDC/search
     */
    public $presetVars = true;

    /**
     * Constructor
     *
     * @param CakeRequest $request Request object for this controller. Can be null for testing,
     *  but expect that features that use the request parameters will not work.
     * @param CakeResponse $response Response object for this controller.
     */
    public function __construct($request, $response) {
        $this->_setupComponents();
        parent::__construct($request, $response);
        $this->_reInitControllerName();
    }

    /**
     * Providing backward compatibility to a fix that was just made recently to the core
     * for users that want to upgrade the plugin but not the core
     *
     * @link http://cakephp.lighthouseapp.com/projects/42648-cakephp/tickets/3550-inherited-controllers-get-wrong-property-names
     * @return void
     */
    protected function _reInitControllerName() {
        $name = substr(get_class($this), 0, -10);
        if ($this->name === null) {
            $this->name = $name;
        } elseif ($name !== $this->name) {
            $this->name = $name;
        }
    }

    /**
     * Returns $this->plugin with a dot, used for plugin loading using the dot notation
     *
     * @return mixed string|null
     */
    protected function _pluginDot() {
        if (is_string($this->plugin)) {
            return $this->plugin . '.';
        }
        return $this->plugin;
    }

    /**
     * Wrapper for CakePlugin::loaded()
     *
     * @param string $plugin
     * @param bool $exception
     * @throws MissingPluginException
     * @return boolean
     */
    protected function _pluginLoaded($plugin, $exception = true) {
        $result = CakePlugin::loaded($plugin);
        if ($exception === true && $result === false) {
            throw new MissingPluginException(array('plugin' => $plugin));
        }
        return $result;
    }

    /**
     * Setup components based on plugin availability
     *
     * @return void
     * @link https://github.com/CakeDC/search
     */
    protected function _setupComponents() {
        if ($this->_pluginLoaded('Search', false)) {
            $this->components[] = 'Search.Prg';
        }
    }

    /**
     * beforeFilter callback
     *
     * @return void
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->_setupAuth();
        $this->_setupPagination();
        $this->set('model', $this->modelClass);
    }

    /**
     * Sets the default pagination settings up
     *
     * Override this method or the index action directly if you want to change
     * pagination settings.
     *
     * @return void
     */
    protected function _setupPagination() {
        $this->Paginator->settings = array(
            'limit' => 12,
            'conditions' => array(
                $this->modelClass . '.is_active' => 1
            )
        );
    }

    /**
     * Setup Authentication Component
     *
     * @return void
     */
    protected function _setupAuth() {
        $this->Auth->deny('index','add','info');
        $this->Auth->loginRedirect = '/modules';
        $this->Auth->loginAction = array('admin' => false, 'plugin' => Inflector::underscore($this->plugin), 'controller' => 'users', 'action' => 'login');
    }

    /**
     * Simple listing of all users
     *
     * @return void
     */
    public function index() {
        $this->Modules->contain();
        $this->set('modules', $this->Paginator->paginate($this->Modules));
    }

    /**
     * Simple listing of all users
     *
     * @return void
     */
    public function add() {
        $this->Modules->contain();
        $this->set('modules', $this->Paginator->paginate($this->Modules));
    }

    /**
     * @param null $slug
     * @throws NotFoundException
     */
    public function info($slug = null) {
        $this->Modules->contain();
        $module = $this->Modules->findBySlug($slug);
        if (!$module) {
            throw new NotFoundException();
        }

        $this->Assessments->contain(array('AssessmentsChoices'));
        $assessments = $this->Assessments->find('all',
            array(
                'conditions' => array(
                    'Assessments.modules_id' => $module['Modules']['id'],
                    'Assessments.is_active' => 1
                ),
                'recursive' => 1,
                'order' => array('Assessments.id'),
                'limit' => 20
            )
        );
        $model = 'UsersAssessmentsChoicesAnswers';
        $name = $this->name;
        $action = $this->action;
        $this->set(compact('assessments', 'module'));
    }

    /**
     * Search - Requires the CakeDC Search plugin to work
     *
     * @throws MissingPluginException
     * @return void
     * @link https://github.com/CakeDC/search
     */
    public function search() {
        $searchTerm = $this->request->query['q'];


        $this->Paginator->settings = array(
            'limit' => 20,
            'conditions' => array(
                'AND' => array($this->modelClass . '.is_active' => 1),
                'OR' =>
                array(
                    $this->modelClass . ".name LIKE '%".addslashes($searchTerm)."%'",
                    $this->modelClass . ".description LIKE '%".addslashes($searchTerm)."%'",
                )
            )
        );
        $this->Modules->contain();
        $this->set('modules', $this->Paginator->paginate($this->modelClass));
        $this->set('searchTerm', $searchTerm);
    }

    /**
     * Default isAuthorized method
     *
     * This is called to see if a user (when logged in) is able to access an action
     *
     * @param array $user
     * @return boolean True if allowed
     * @link http://book.cakephp.org/2.0/en/core-libraries/components/authentication.html#using-controllerauthorize
     */
    public function isAuthorized($user = null) {
        return parent::isAuthorized($user);
    }
}
