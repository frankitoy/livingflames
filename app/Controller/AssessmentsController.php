<?php

App::uses('AppController', 'Controller');

class AssessmentsController extends AppController
{

    /**
     * Controller name
     *
     * @var string
     */
    public $name = 'Assessments';

    /**
     * @var array
     */
    public $uses = array(
        'Assessments',
        'User',
        'PassingGrade',
        'UsersAssessmentsChoicesAnswers',
        'UsersAssessmentsResults',
        'UsersCertificates'
    );

    /**
     * If the controller is a plugin controller set the plugin name
     *
     * @var mixed
     */
    public $plugin = null;

    /**
     * Helpers
     *
     * @var array
     */
    public $helpers = array(
        'Html',
        'Form',
        'Session',
        'Time',
        'Text'
    );

    /**
     * Components
     *
     * @var array
     */
    public $components = array(
        'Session',
        'Cookie',
        'Paginator'
    );

    /**
     * Constructor
     *
     * @param CakeRequest $request Request object for this controller. Can be null for testing,
     *  but expect that features that use the request parameters will not work.
     * @param CakeResponse $response Response object for this controller.
     */
    public function __construct($request, $response)
    {
        parent::__construct($request, $response);
        $this->_reInitControllerName();
    }

    /**
     * Providing backward compatibility to a fix that was just made recently to the core
     * for users that want to upgrade the plugin but not the core
     *
     * @link http://cakephp.lighthouseapp.com/projects/42648-cakephp/tickets/3550-inherited-controllers-get-wrong-property-names
     * @return void
     */
    protected function _reInitControllerName()
    {
        $name = substr(get_class($this), 0, -10);
        if ($this->name === null) {
            $this->name = $name;
        } elseif ($name !== $this->name) {
            $this->name = $name;
        }
    }

    /**
     * beforeFilter callback
     *
     * @return void
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->_setupPagination();
        $this->set('model', $this->modelClass);
    }

    /**
     * Sets the default pagination settings up
     *
     * Override this method or the index action directly if you want to change
     * pagination settings.
     *
     * @return void
     */
    protected function _setupPagination()
    {
        $this->Paginator->settings = array(
            'limit' => 12
        );
    }

    /**
     * @param null $slug
     * @throws NotFoundException
     */
    public function index($slug = null)
    {
        if ($slug === null) {
            $slug = trim($this->request->query['token']);
        }

        $user = $this->User->findById($slug);
        if (!$user || $user['User']['has_paid'] < 1) {
            throw new NotFoundException();
        }

        $title_for_layout = 'Living Flame: ' . $user['Modules']['name'] . ' ' . $user['Modules']['description'];
        $action = $this->action;
        $name = $this->name;
        $this->set(compact('title_for_layout', 'name', 'user', 'action'));
    }

    /**
     * @throws NotFoundException
     */
    public function start()
    {

        $slug = trim($this->request->query['token']);
        $user = $this->User->findById($slug);
        $total = 0;
        $total = $this->UsersAssessmentsChoicesAnswers->find('count', array(
                'conditions' => array(
                    "AND" => array(
                        'UsersAssessmentsChoicesAnswers.modules_id' => $user['Modules']['id'],
                        'UsersAssessmentsChoicesAnswers.users_id' => $user['User']['id']
                    )
                ),
                'group' => array('UsersAssessmentsChoicesAnswers.attempt_group')
            )
        );

        $passingGrade = $this->PassingGrade->find('first');
        $invalidLinkRedirect = '/assessments?token=' . $slug;
        if (!$user || $user['User']['has_paid'] < 1) {
            $this->Session->setFlash(__d('users', 'User has not paid the assessment.'), 'default', array('class' => 'col-sm-7 col-sm-offset-2 alert alert-danger fade in'));
            $this->redirect($invalidLinkRedirect);
        }

        if ($total > 2) {
            $this->Session->setFlash(__d('users', 'You are only allowed to take two assessment.'), 'default', array('class' => 'col-sm-7 col-sm-offset-2 alert alert-danger fade in'));
            $this->redirect($invalidLinkRedirect);
        }

        if ($user['User']['assessment_score'] >= $passingGrade['PassingGrade']['grade']) {
            $this->Session->setFlash(__d('users', 'You already passed the assessment.'), 'default', array('class' => 'col-sm-7 col-sm-offset-2 alert alert-danger fade in'));
            $this->redirect($invalidLinkRedirect);
        }

        if (!empty($this->request->data)) {
            $error = 0;
            foreach ($this->request->data['UsersAssessmentsChoicesAnswers']['modules_id'] as $key => $value) {
                if (!isset($this->request->data['UsersAssessmentsChoicesAnswers']['assessments_choices_id'][$key])) {
                    $this->set('error_' . $key, true);
                    $error++;
                } else {
                    $this->set('assessment_choices_id_' . $key, $this->request->data['UsersAssessmentsChoicesAnswers']['assessments_choices_id'][$key]);
                }
            }

            if (!$error) {
                $total = $total + 1;
                foreach ($this->request->data['UsersAssessmentsChoicesAnswers']['assessments_choices_id'] as $key => $value) {
                    $data = array();
                    $this->UsersAssessmentsChoicesAnswers->create();
                    $data['UsersAssessmentsChoicesAnswers']['assessments_id'] = $this->request->data['UsersAssessmentsChoicesAnswers']['assessments_id'][$key];
                    $data['UsersAssessmentsChoicesAnswers']['users_id'] = $this->request->data['UsersAssessmentsChoicesAnswers']['users_id'][$key];
                    $data['UsersAssessmentsChoicesAnswers']['assessments_choices_id'] = $value;
                    $data['UsersAssessmentsChoicesAnswers']['modules_id'] = $this->request->data['UsersAssessmentsChoicesAnswers']['modules_id'][$key];
                    $data['UsersAssessmentsChoicesAnswers']['attempt_group'] = $total;
                    $data['UsersAssessmentsChoicesAnswers']['dte_created'] = date("Y-m-d H:i:s");
                    $this->UsersAssessmentsChoicesAnswers->save($data);
                }

                $correctAnswers = $this->UsersAssessmentsChoicesAnswers->find('count', array(
                    'conditions' => array(
                        'AssessmentsChoices.is_correct_answer' => 1,
                        'UsersAssessmentsChoicesAnswers.users_id' => $user['User']['id'],
                        'UsersAssessmentsChoicesAnswers.attempt_group' => $total,
                    )
                ));

                $postData = array();
                $postData['User']['id'] = $user['User']['id'];
                $postData['User']['assessment_score'] = $correctAnswers;
                $postData['User']['assessment_attempt'] = $total;
                $postData['User']['modified'] = date("Y-m-d H:i:s");
                $postData['User']['has_generated_result'] = 0;
                $this->User->save($postData, false);

                $data = array();
                $this->UsersAssessmentsResults->create();
                $data['UsersAssessmentsResults']['users_id'] = $user['User']['id'];
                $data['UsersAssessmentsResults']['modules_id'] = $user['Modules']['id'];
                $data['UsersAssessmentsResults']['attempt_group'] = $total;
                $data['UsersAssessmentsResults']['score'] = $correctAnswers;
                $data['UsersAssessmentsResults']['dte_created'] = date("Y-m-d H:i:s");
                $this->UsersAssessmentsResults->save($data);

                if ($correctAnswers >= $passingGrade['PassingGrade']['grade']) {
                    $data = array();
                    $this->UsersCertificates->create();
                    $data['UsersCertificates']['users_id'] = $user['User']['id'];
                    $data['UsersCertificates']['dte_created'] = date("Y-m-d H:i:s");
                    $this->UsersCertificates->save($data);
                }

                $link = '/results?token=' . $user['User']['id'];
                $this->redirect($link);
            } else {
                $this->Session->setFlash(__d('users', 'Assessment answers is not complete. Please try again.'), 'default', array('class' => 'col-sm-7 col-sm-offset-2 alert alert-danger fade in'));
            }
        }

        $this->Assessments->contain(array('AssessmentsChoices'));
        $assessments = $this->Assessments->find('all',
            array(
                'conditions' => array(
                    'Assessments.modules_id' => $user['Modules']['id'],
                    'Assessments.is_active' => 1
                ),
                'recursive' => 1,
                'order' => array('Assessments.id'),
                'limit' => 20
            )
        );
        $model = 'UsersAssessmentsChoicesAnswers';
        $name = $this->name;
        $action = $this->action;
        $this->set(compact('name', 'action', 'user', 'assessments', 'model'));
    }

    public function download()
    {
        $slug = trim($this->request->query['token']);
        $user = $this->User->findById($slug);
        $passingGrade = $this->PassingGrade->find('first');
        if (!$user || $user['User']['has_paid'] < 1) {
            throw new NotFoundException();
        }

        $this->UsersAssessmentsResults->contain();
        $assessmentResults = $this->UsersAssessmentsResults->find('first', array(
                'conditions' => array(
                    "AND" => array(
                        'UsersAssessmentsResults.modules_id' => $user['Modules']['id'],
                        'UsersAssessmentsResults.users_id' => $user['User']['id']
                    )
                ),
                'order' => array('UsersAssessmentsResults.attempt_group' => 'DESC'),
                'limit' => 1
            )
        );

        $inCorrectAnswers = $this->UsersAssessmentsChoicesAnswers->find('all', array(
            'conditions' => array(
                'AssessmentsChoices.is_correct_answer' => 0,
                'UsersAssessmentsChoicesAnswers.users_id' => $user['User']['id'],
                'UsersAssessmentsChoicesAnswers.attempt_group' => $assessmentResults['UsersAssessmentsResults']['attempt_group'],
            )
        ));

        $name = 'Results';
        $action = 'results';
        $this->set(compact('name', 'action', 'user', 'inCorrectAnswers', 'assessmentResults'));
    }

    public function results()
    {

        $slug = trim($this->request->query['token']);
        $user = $this->User->findById($slug);
        $passingGrade = $this->PassingGrade->find('first');
        if (!$user || $user['User']['has_paid'] < 1 || $user['User']['has_generated_result'] > 0) {
            throw new NotFoundException();
        }

        $postData = array();
        $postData['User']['id'] = $user['User']['id'];
        $postData['User']['modified'] = date("Y-m-d H:i:s");
        $postData['User']['has_generated_result'] = 1;
        $this->User->save($postData, false);

        $assessmentResults = $this->UsersAssessmentsResults->find('all', array(
                'conditions' => array(
                    "AND" => array(
                        'UsersAssessmentsResults.modules_id' => $user['Modules']['id'],
                        'UsersAssessmentsResults.users_id' => $user['User']['id']
                    )
                ),
                'order' => array('UsersAssessmentsResults.attempt_group' => 'DESC'),
                'limit' => 1
            )
        );

        $name = 'Results';
        $action = 'view';
        $this->set(compact('name', 'action', 'user', 'passingGrade', 'assessmentResults'));
    }

    /**
     * @param null $slug
     */
    public function download_certificate($slug = null)
    {
        $this->autoRender = false;
        $this->layout = 'pdf';
        $this->render();
    }
}
