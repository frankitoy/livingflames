<div class="row" ng-controller="formController">
    <div class="col-md-8 col-md-offset-2 add-padding">
        <div id="tablecontent">
        <!--<p>Instructions: Please indicate your level of agreement with the statements listed below in #1 - #7</p>-->
        <?php
        echo $this->Form->create($model);
        echo $this->Form->hidden($model.'.users_id.', array('value' => $user['User']['id'],'security' => false,'hiddenField' => false));
        echo $this->Form->hidden($model.'.modules_id.', array('value' => $user['User']['modules_id'],'security' => false,'hiddenField' => false));
        ?>
        <table class="survey" width="1178" border="0" align="center" cellpadding="5" cellspacing="0">
           <tr>
             <td style="border:0px;">&nbsp;</td>
             <td style="border:0px;" height="10">&nbsp;</td>
             <td style="border:0px;" height="10" align="center" valign="top"><strong>5</strong></td>
             <td style="border:0px;" height="10" align="center" valign="top"><strong>4</strong></td>
             <td style="border:0px;" height="10" align="center" valign="top"><strong>3</strong></td>
             <td style="border:0px;" height="10" align="center" valign="top"><strong>2</strong></td>
             <td style="border:0px;" align="center" valign="top"><strong>1</strong></td>
             <td style="border:0px;" height="10" align="center" valign="top"><strong>N/A</strong></td>
           </tr>
           <tr>
             <td width="40">&nbsp;</td>
             <td width="478" height="10" align="right">&nbsp;</td>
             <?php
             foreach ($surveysChoices as $surveysChoice) {
             ?>
             <td width="80" height="10" align="center" valign="top"><?php echo $surveysChoice['SurveysChoices']['choice'];?></td>
             <?php
             }
             ?>
           </tr>
           <?php
           foreach ($surveys as $survey) {
           ?>
           <tr>
             <td><strong><?php echo $survey['Surveys']['id'];?>.</strong></td>
             <td height="10"><?php echo $survey['Surveys']['question'];?></td>
             <?php
             foreach ($surveysChoices as $surveysChoice) {

               ?>
               <td width="80" height="10" align="center" valign="top">
                 <?php
                 echo $this->Form->radio(
                   $model.'.surveys_choices_id.' . $survey['Surveys']['id'],
                   array(
                     'label' =>  '',
                   ),
                   array(
                     'id' => $surveysChoice['SurveysChoices']['id'],
                     'hiddenField' => false,
                     'value' => $surveysChoice['SurveysChoices']['id'],
                     'security' => false
                   )
                 );
                 ?></td>
             <?php
             }
             ?>
           </tr>
           <?php
           }
           ?>
          <tr>
            <td><strong>10.</strong></td>
            <td height="10">Please provide any other suggestions, comments or ideas you have for improving this online experience</td>
            <td height="10" colspan="6" align="center">
              <?php
              echo $this->Form->textarea(
              'comment',
              array('rows' => '5', 'cols' => '45')
              );
              ?>
            </td>
          </tr>
        </table>
        <br>
        <div class="clear:both"></div>
        <div class="form-group">
            <div class="col-md-9 col-md-offset-5">
            <?php echo $this->Form->button('Submit Survey', array('type' => 'submit', 'class' => 'btn button_example', 'ng-disabled' => 'false'));?>
            </div>
        </div>
        <?php
        echo $this->Form->end();
        ?>
        </div>
    </div>
</div>