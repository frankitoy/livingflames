<?php
$this->Html->addCrumb('Surveys', '/surveys');
$this->Html->addCrumb('Graph');
echo $this->element('Users.Users/sidebar');
echo $this->element('crumbs');
?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<div class="container-fluid">
    <div class="row">
        <div class="row">
          <?php
          foreach ($surveys as $survey) {
          ?>
          <script type="text/javascript">
            google.load("visualization", "1", {packages:["corechart"]});
            google.setOnLoadCallback(drawChart);
            function drawChart() {
              var data = google.visualization.arrayToDataTable([
                ['Score', 'Score'],
                <?php
                foreach ($surveysChoices as $key => $surveysChoice){
                ?>
                ["<?php echo $surveysChoice['SurveysChoices']['choice'];?>",  <?php echo $this->User->findUserSurvey($survey['Surveys']['id'],$surveysChoice['SurveysChoices']['id']);?>],
                <?php
                }
                ?>
              ]);

              var options = {
                title: '<?php echo str_replace("<br>", " ",$survey['Surveys']['question']);?>',
                hAxis: {title: 'Total Number of Respondents <?php echo count($survey["UsersSurveyedChoices"]);?>', titleTextStyle: {color: 'black'}}
              };

              var chart = new google.visualization.BarChart(document.getElementById('chart_div_<?php echo $survey['Surveys']['id'];?>'));
              chart.draw(data, options);
            }
          </script>
          <div id="chart_div_<?php echo $survey['Surveys']['id'];?>" class="col-xs-4 col-md-4" style="width: 500px; height: 300px;"></div>
          <?php
          }
          ?>
        </div>
    </div>
</div>