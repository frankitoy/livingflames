<?php
$this->Html->addCrumb('Surveys', '/surveys');
$this->Html->addCrumb('List');
echo $this->element('Users.Users/sidebar');
echo $this->element('crumbs');
?>
<div class="table-responsive">
    <?php //echo $this->element('search_form'); ?>
    <br><br>
    <br><br>
    <table class="table table-hover table-striped table-bordered table-condensed">
        <thead>
        <tr>
            <th width="10%"><a href=""><?php echo $this->Paginator->sort('Users.first_name','Name'); ?></a></th>
            <th width="10%"><?php echo $this->Paginator->sort('Modules.name','Module Name'); ?></th>
            <?php foreach( $surveys as $survey_id => $survey) {
            echo '<td width="15%">'. $survey.'</td>';
            }?>
            <td>Comments</td>
            <td><?php echo $this->Paginator->sort('UsersSurveyedChoices.dte_created','Date Created'); ?></td>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($users as $user_id => $user) {
        ?>
        <tr>
          <td><a href="<?php echo $this->Html->url(array("controller" => "users" ,'action' => 'info',$user["User"]['id']));?>"><?php echo ucfirst($user['User']['first_name']);?>&nbsp;<?php echo ucfirst($user['User']['last_name']);?></a></td>
          <td><a href="<?php echo str_replace("/index", "",$this->Html->url(array("controller" => "modules" ,"action" =>"info", $user['Modules']['slug'])));?>"><?php echo $user['Modules']['name'];?></a></td>

          <?php
          foreach( $surveys as $survey_id => $survey) {
            foreach($user['UsersSurveyedChoices'] as $userChoice) {
              if (is_array($userChoice) && isset($userChoice['surveys_id']) && $userChoice['surveys_id'] == $survey_id) {
                echo '<td>'.$surveyChoices[$userChoice['surveys_choices_id']].'</td>';
              }
            }
          }?>
          <td><?php echo $user['UsersSurveyedComments'][0]['comment'];?></td>
          <td><?php echo date("jS-M-y H:i",strtotime($user["UsersSurveyedChoices"]['dte_created']));?></td>
        </tr>
        <?php
        }
        ?>
        <tr><th colspan="13" style="text-align: center;">
                <?php
                echo $this->Paginator->counter(array(
                    'format' => __d('users', 'Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%')
                ));
                ?></th></tr>
        </tbody>
    </table>
</div>