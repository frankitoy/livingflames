<?php

App::import('Vendor','TCPDF',array('file' => 'tcpdf/tcpdf.php'));
App::uses('AppHelper', 'View/Helper');

class PdfHelper  extends AppHelper {

	/**
	 * @var TCPDF
	 */
	public $pdf;
 
    public function PdfHelper() {
		$this->pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	}
}