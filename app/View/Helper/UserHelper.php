<?php
App::uses('AppHelper', 'View/Helper');

class UserHelper  extends AppHelper {

    public function findUserSurvey($surveys_id = 0 , $surveys_choices_id = 0) {
        App::import("Model", "UsersSurveyedChoices");
        $usersSurveyedChoices = new UsersSurveyedChoices();
        $usersSurveyedChoices->contain();
        $total = $usersSurveyedChoices->find('count',
            array(
                'conditions' => array (
                    "AND" => array(
                        'UsersSurveyedChoices.surveys_id' => $surveys_id,
                        'UsersSurveyedChoices.surveys_choices_id' => $surveys_choices_id
                    )
                ),
                'group' => array('UsersSurveyedChoices.users_id')
            )
        );

        return ($total > 0) ? $total: 0;
    }
}