<div class="row-fluid">
    <div class="col-md-9 col-md-offset-2 add-padding"><h1 style="margin-left:-100px !important;">Results</h1>
<?php
/**
 * passed
 */

$assessmentScore = intval($assessmentResults[0]['UsersAssessmentsResults']['score']);
if ($assessmentScore >=  $passingGrade['PassingGrade']['grade']) {
?>

<ul class="col-md-6 col-md-offset-3 add-padding">
    <li>Click <a class=assessment_link href="<?php echo $this->Html->url(array("controller" => "print-certificate",'action' => 'index'));?>?token=<?php echo $user['User']['id'];?>" target="_blank">here</a> to download the certificate.</li>
    <?php
    if ($user['User']['assessment_score'] < 10){
    ?>
    <li>See your results <a href="<?php echo $this->Html->url(array("controller" => "download-result",'action' => 'index'));?>?token=<?php echo $user['User']['id'];?>" target="_blank" class="assessment_link">here</a>.</li>
    <?php
    }
    ?>
    <li><a class="assessment_link" href="<?php echo $this->Html->url(array("controller" => "take-a-survey",'action' => 'index'));?>?token=<?php echo $user['User']['id'];?>" target="_blank">Take a survey.</a></li>
</ul>
<?php
/**
 * failed
 */
} else {
?>
<p style="text-align:center">You have been incompetent this time.</p>
<p style="text-align:center"><a class="assessment_link" href="<?php echo $this->Html->url(array("controller" => "download-result",'action' => 'index'));?>?token=<?php echo $user['User']['id'];?>" target="_blank">Click here</a> to view the results or for another attempt click <a href="<?php echo $this->Html->url(array("controller" => "assessments", 'action' => 'index'));?>?token=<?php echo $user['User']['id'];?>" class="assessment_link" target="_blank">here</a>.</p>
<?php
}
?>
    </div>
</div>