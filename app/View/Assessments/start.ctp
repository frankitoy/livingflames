<div class="row-fluid" ng-controller="formController">
	<div class="col-md-10 col-md-offset-2 add-padding">
	<?php
	echo str_replace('id="UsersAssessmentsChoicesAnswersStartForm"','id="UsersAssessmentsChoicesAnswersStartForm" class="form-horizontal" role="form" novalidate name="UsersAssessmentsChoicesAnswersStartForm"',$this->Form->create($model));

	$i = 1;
	foreach ($assessments as $assessment) {
	?>
		<div class="col-md-12">
			<div class="form-group">
				<div class="col-sm-12"><span class="question"><?php echo $i. '. '. $assessment['Assessments']['question'];?></span></div>
				<div class="col-sm-12">
				<?php
				if (count($assessment['AssessmentsChoices']) > 0) {
					foreach ($assessment['AssessmentsChoices'] as $key => $choices) {
				?>
					<div class="radio col-md-offset-1">
						<?php
                        $selected = '';
                        if (isset(${'assessment_choices_id_' . $assessment['Assessments']['id']}) && ${'assessment_choices_id_' . $assessment['Assessments']['id']} == $choices['id']) {
                            $selected = 'checked';
                        }

                        echo $this->Form->radio('UsersAssessmentsChoicesAnswers.assessments_choices_id.' . $assessment['Assessments']['id'], array(
                            'label' =>  $choices['answer'],
                        ), array(
                            'id' => $choices['id'],
                            'hiddenField' => false,
                            'value' => $choices['id'],
                            'checked' => $selected,
                            'required' => true,
                            'security' => false
                        ));

                        echo $this->Form->hidden('UsersAssessmentsChoicesAnswers.users_id.' . $assessment['Assessments']['id'], array('value' => $user['User']['id']));
                        echo $this->Form->hidden('UsersAssessmentsChoicesAnswers.modules_id.' . $assessment['Assessments']['id'], array('value' => $user['Modules']['id']));
                        echo $this->Form->hidden('UsersAssessmentsChoicesAnswers.assessments_id.' . $assessment['Assessments']['id'], array('value' =>  $assessment['Assessments']['id']));
                        ?>
                    </div>
				<?php
					}

                    if (isset(${'error_' . $assessment['Assessments']['id']})){
                    ?>
                    <span class="help-inline">Please select your answer</span>
                    <?php
                    }
                }
				?>
				</div>
			</div>
			<hr>
		</div>
	<?php
		$i++;
	}
	?>
		<div class="col-md-10">
			<div class="form-group">
				<div class="col-sm-offset-1 col-sm-6">
					<?php echo $this->Form->button('Submit', array('type' => 'submit', 'class' => 'btn button_example', 'ng-disabled' => 'false'));?>
				</div>
			</div>
		</div>
	<?php
	echo $this->Form->end();
	?>
	</div>
</div>