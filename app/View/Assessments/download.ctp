<div class="row-fluid" ng-controller="formController">
	<div class="col-md-10 col-md-offset-2 add-padding">
	<div class="col-md-12"><?php if (count($inCorrectAnswers) > 0){ ?><h3 style="margin-left:15px;margin-bottom:20px">Below questions were incorrectly answered.</h3><?php }?></div>
	<?php
	$i = 1;
	foreach ($inCorrectAnswers as $assessment) {
	?>
		<div class="col-md-12">
			<div class="form-group">
				<div class="col-sm-12"><span class="question"><?php echo $i. '. '. $assessment['Assessments']['question'];?></span></div>
				<div class="col-sm-12"><ul><li class="question font-color-red">Your answer: <?php echo $assessment['AssessmentsChoices']['answer'];?></li></ul>			<hr></div>
			</div>

		</div>
		
	<?php
		$i++;
	}
	?>
<hr>
	</div>

</div>