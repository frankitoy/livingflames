<div class="row-fluid">
    <div>
        <h1><?php echo $user['Modules']['name'];?></h1>
        <h2><?php echo $user['Modules']['description'];?></h2>
    </div>
    <div id="button" align="center">
        <a href="<?php echo str_replace("/pdf", "", $this->Html->url(array("controller" => "download", 'action' => 'pdf')));?>/<?php echo $user['User']['id'];?>/<?php echo str_replace(" ", "-", trim(strtolower($user['Modules']['name'])));?>_<?php echo $user['Modules']['slug'];?>.pdf" target="_blank" class="button_example">Download Course Content</a>
        <a href="<?php echo $this->Html->url(array("controller" => "start-assessment"));?>/<?php echo $user['Modules']['slug'];?>?token=<?php echo $user['User']['id'];?>" class="button_example" style="margin-left:15px;">Start Assessment</a>
    </div>
</div>