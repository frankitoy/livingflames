<div id="footer">
    <div class="container">
        <p class="base">343B Church Street, Penrose, Auckland   |   Phone: +64 9 622 1148   |   Fax: +64 9 622 1179   |   Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a>   |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p>
    </div>
</div>
