<div class="col-sm-3 col-md-3 pull-right">
    <form class="navbar-form" role="search" action="<?php echo $this->Html->url(array("controller" => "modules" ,'action' => 'search'));?>">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" name="q" id="srch-term" value="<?php echo (isset($searchTerm) && $searchTerm) ? $searchTerm : null;?>">
            <div class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
            </div>
        </div>
    </form>
</div>