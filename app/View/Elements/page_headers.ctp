<?php
if (strtolower($name) == 'pages' ||
   (strtolower($name) == 'assessments' && strtolower($action) !== 'start') ||
   (strtolower($name) == 'results' && strtolower($action) == 'view')
) {
?>
    <div id="top"></div>
<?php
} else {
    if (!isset($isLoginRequired) || $isLoginRequired == 0){
?>

        <div id="topmodule">
            <?php
            if ((strtolower($action) !== 'start' && strtolower($action) !== 'results' && strtolower($action) !== 'survey')|| !isset($action)) {
            ?>
            <div id="moduleheader">Registration</div>
            <?php
            } elseif (strtolower($action) == 'results') {
            ?>
            <div id="moduleheader">Results</div>
            <?php        
            } elseif (strtolower($action) == 'survey') {
            ?>
            <div id="moduleheader">Survey</div>
            <?php
            }else {

            ?>
            <div id="moduleheader"><?php echo $user['Modules']['name'];?>: <span class="font-white"><?php echo $user['Modules']['description'];?></span></div>
            <?php
            }
            ?>
        </div>

<?php
    }
}
?>