<?php
/**
 *
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Pages
 * @since         CakePHP(tm) v 0.10.0.1076
 */

#if (!Configure::read('debug')):
#	throw new NotFoundException();
#endif;

App::uses('Debugger', 'Utility');
if (Configure::read('debug') > 0):
	Debugger::checkSecurityKeys();
endif;

?>
<div style="clear:both"></div>
<div class="row">
  <div class="row"><div class="col-md-4 col-md-offset-4"><h1 style="text-align: left">Online Course Modules</h1></div></div>
  <div class="row">
    <?php
    foreach ($modules as $module) {
    ?>
    <div class="row"><div class="col-md-4 col-md-offset-4">
    <h2 class="main-header"><?php
    echo str_replace("index/" , "",
      $this->Html->link(
        $module['Modules']['name'].': '.$module['Modules']['description'],
        array(
          'controller' => 'module',
          'action' => 'index',
          $module['Modules']['module_slug_id']
        )
      )
    );?>
    </h2><h3>Points:&nbsp;<?php echo $module['Modules']['points'];?></h3>
    </div></div>
    <?php
    }
    ?>
    <p class="col-md-4 col-md-offset-4">Certificate is valid for 2 years from the date of completion.</p>
  </div>
</div>
<div style="clear:both"></div>