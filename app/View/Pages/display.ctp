<?php
/**
 *
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Pages
 * @since         CakePHP(tm) v 0.10.0.1076
 */

#if (!Configure::read('debug')):
#	throw new NotFoundException();
#endif;

App::uses('Debugger', 'Utility');
if (Configure::read('debug') > 0):
	Debugger::checkSecurityKeys();
endif;

?>
<h1 style="clear:both"><?php echo $modules['Modules']['name'];?></h1>
<hr class="hr">
<h2><?php echo $modules['Modules']['description'];?></h2>
<p>Points: <?php echo $modules['Modules']['points'];?></p>
<div id="button" align="center"><?php
echo str_replace("index/" , "",
    $this->Html->link(
        __('Register Now', true),
        array(
            'controller' => 'registration',
            'action' => 'index',
            $modules['Modules']['slug']
        ),
        array(
            'class' => 'button_example'
        )
    )
);?></div>
