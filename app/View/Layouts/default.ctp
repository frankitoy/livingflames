<!DOCTYPE html>
<html lang="en" ng-app>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="Author" content="Web Graphics Design Studio - Geet Phanse" />
    <meta name="Copyright" content="Copyright 2014 Living Flame - All rights reserved" />
    <title><?php echo $title_for_layout; ?></title>
    <?php
    if (isset($description)) {
    ?>
    <meta name="description" content="<?php echo $description;?>">
    <?php
    }
    ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="fragment" content="!" />
    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">
    <?php
    echo $this->Html->meta('icon');
    echo $this->Html->css('bootstrap.min');
    echo $this->Html->css('styles.min');
    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->Html->script('jquery.min');
    echo $this->Html->script('bootstrap.min');
    echo $this->Html->script('angular.min');
    echo $this->Html->script('scripts');
    ?>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php //echo $this->element('sql_dump'); ?>
<div id="wrap">
    <?php
	$action = (isset($action)) ? $action : null;
	if (isset($name)) {echo $this->element('page_headers', array('name' => $name, 'action' => $action));}?>
    <div class="container">
        <div class="row"><?php echo $this->Session->flash(); ?></div>
        <?php echo $this->fetch('content'); ?>
    </div>
</div>
<?php if (isset($name) && (!isset($isLoginRequired) || $isLoginRequired == 0 ) && $action != 'registration' && $action !='start' && $action != 'results') {echo $this->element('footer', array(), array('cache' => true));}?>
</body>
</html>