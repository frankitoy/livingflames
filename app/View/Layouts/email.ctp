<!DOCTYPE html>
<html lang="en" ng-app>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><?php echo $title_for_layout; ?></title>
    <?php
    if (isset($description)) {
    ?>
    <meta name="description" content="<?php echo $description;?>">
    <?php
    }
    ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="fragment" content="!" />
    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet">
</head>
<body>
<?php //echo $this->element('sql_dump'); ?>
<div id="wrap">
    <?php
	$action = (isset($action)) ? $action : null;
	if (isset($name)) {echo $this->element('page_headers', array('name' => $name, 'action' => $action));}?>
    <div class="container">
        <?php echo $this->Session->flash(); ?>
        <?php echo $this->fetch('content'); ?>
    </div>
</div>
<?php if (isset($name) && (!isset($isLoginRequired) || $isLoginRequired == 0)) {echo $this->element('footer', array(), array('cache' => true));}?>
</body>
</html>