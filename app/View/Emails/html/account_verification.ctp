<?php
/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/*
echo __d('users', 'Hello %s,', $user[$model]['first_name'] . ' ' . $user[$model]['last_name']);
echo "\n";
echo __d('users', 'to validate your account, you must visit the URL below within 24 hours');
echo "\n";
echo Router::url(array('admin' => false, 'plugin' => 'users', 'controller' => 'users', 'action' => 'verify', 'email', $user[$model]['email_token']), true);
*/
?>
<p>Thank you for registering with Living Flame Online Training Course. </p>

<p>Please call us on 09 6221148 with your credit card details OR pay directly into our bank account with your name as a reference, and online training under particulars .</p>

<p>Bank account number: 02 0240 0271125 00</p>

<p>We will be in touch with you shortly.</p>
<p> </p>

<p>Team,</p>
<p>Living Flame</p>
<p>343B Church Street, Penrose, Auckland<br />
  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />
  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p>