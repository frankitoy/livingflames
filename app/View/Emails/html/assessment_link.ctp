<?php
$link  = Configure::read('baseUri') . str_replace(array("/users"),array(""),$this->Html->url(array("controller" => "assessments")));
$link .= '?token=' . $user['User']['id'];
?>
<p>Thank you for choosing Living Flame Online Course. We hope you enjoy the course and learn from its content. <br />
  Please click on the link to start the assessment <a href="<?php echo $link;?>" target="_blank"><?php echo $link;?></a> 
  <br />
  </p>
<p>Team,<br />
  Living Flame<br />
</p>
<p>343B Church Street, Penrose, Auckland<br />
  Phone: +64 9 622 1148 | Fax: +64 9 622 1179<br />
  Email: <a href="mailto:info@livingflame.co.nz" target="_top">info@livingflame.co.nz</a> |   Web: <a href="http://www.livingflame.co.nz" target="_blank">www.livingflame.co.nz</a></p>

