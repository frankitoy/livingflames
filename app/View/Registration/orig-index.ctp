<div class="row" ng-controller="formController">
    <div class="col-md-9 col-md-offset-2 add-padding">
        <?php
        $cssErrorClass = ' form-error ng-dirty ng-invalid ng-invalid-required';
        $cssFirstName = null;
        $cssLastName = null;
        $cssEmail = null;
        $cssAddress = null;
        $cssRegistrationNumber = null;
        $cssMobile = null;

        echo str_replace('id="UserIndexForm"','id="UserIndexForm" class="form-horizontal" role="form" novalidate name="UserIndexForm"',$this->Form->create($model));
        if (isset($validationErrorsArray)) {
            $cssFirstName = (isset($validationErrorsArray['first_name'])) ?  $cssErrorClass: $cssFirstName;
            $cssLastName = (isset($validationErrorsArray['last_name'])) ? $cssErrorClass : $cssLastName;
            $cssEmail = (isset($validationErrorsArray['email'])) ? $cssErrorClass : $cssEmail;
            $cssAddress = (isset($validationErrorsArray['address'])) ? $cssErrorClass : $cssAddress;
            $cssRegistrationNumber = (isset($validationErrorsArray['registration_number'])) ? $cssErrorClass : $cssRegistrationNumber;
            $cssMobile = (isset($validationErrorsArray['mobile'])) ? $cssErrorClass : $cssMobile;
        }
        ?>
        <div class="form-group">
            <label for="UserFirstName" class="col-sm-2 control-label">First Name</label>
            <div class="col-sm-7"><?php  echo $this->Form->text('first_name', array(
                    'class' => 'form-control' . $cssFirstName,
                    'required' => 'required',
                    'ng-model' => 'User.first_name',
                    'placeholder' => 'Enter your first name'));?>
                <?php
                if ($cssFirstName) {
                    ?>
                    <span class="help-inline"><?php echo $validationErrorsArray['first_name'][0];?></span>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="UserLastName" class="col-sm-2 control-label">Last Name</label>
            <div class="col-sm-7"><?php  echo $this->Form->text('last_name', array(
                    'class' => 'form-control' . $cssLastName,
                    'required' => 'required',
                    'ng-model' => 'User.last_name',
                    'placeholder' => 'Enter your last name'));?>
                <?php
                if ($cssLastName) {
                    ?>
                    <span class="help-inline"><?php echo $validationErrorsArray['last_name'][0];?></span>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="UserEmail" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-7"><?php  echo str_replace('type="text"','type="email"',$this->Form->text('email', array(
                    'class' => 'form-control' . $cssEmail,
                    'required' => 'required',
                    'ng-model' => 'User.email',
                    'placeholder' => 'Enter your email')));?>
                <span ng-show="!!UserIndexForm.$error.email" class="help-inline">Please enter a valid email address.</span>
                <?php
                if ($cssEmail) {
                    ?>
                    <span class="help-inline"><?php echo $validationErrorsArray['email'][0];?></span>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="UserCompanyName" class="col-sm-2 control-label">Company name<br>(if applicable)</label>
            <div class="col-sm-7"><?php  echo $this->Form->text('company_name', array(
                    'class' => 'form-control',
                    'placeholder' => 'Enter your company name'));?>
            </div>
        </div>
        <div class="form-group">
            <label for="UserAddress" class="col-sm-2 control-label">Address</label>
            <div class="col-sm-7"><?php echo $this->Form->textarea('address', array(
                    'class' => 'form-control' . $cssAddress,
                    'rows' => 5,
                    'cols' => 2,
                    'required' => 'required',
                    'ng-model' => 'User.address',
                    'placeholder' => 'Enter your address'));?>
                <?php
                if ($cssAddress) {
                    ?>
                    <span class="help-inline"><?php echo $validationErrorsArray['address'][0];?></span>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="UserRegistrationNumber" class="col-sm-2 control-label">Registration Number</label>
            <div class="col-sm-7"><?php  echo $this->Form->text('registration_number', array(
                    'class' => 'form-control' . $cssRegistrationNumber,
                    'required' => 'required',
                    'ng-model' => 'User.registration_number',
                    'placeholder' => 'Enter your registration number'));?>
                <?php
                if ($cssRegistrationNumber) {
                    ?>
                    <span class="help-inline"><?php echo $validationErrorsArray['registration_number'][0];?></span>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="UserMobile" class="col-sm-2 control-label">Mobile</label>
            <div class="col-sm-7"><?php  echo $this->Form->text('mobile', array(
                    'class' => 'form-control' . $cssMobile,
                    'required' => 'required',
                    'ng-model' => 'User.mobile',
                    'placeholder' => 'Enter your mobile number'));?>
                <?php
                if ($cssMobile) {
                    ?>
                    <span class="help-inline"><?php echo $validationErrorsArray['mobile'][0];?></span>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="UserPhone" class="col-sm-2 control-label">Phone No</label>
            <div class="col-sm-7"><?php  echo $this->Form->text('phone', array('class' => 'form-control', 'value' => '', 'placeholder' => 'Enter your phone number'));?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-7">
                <div class="checkbox padding-left-10px">
                    <?php
                    $tosLink = $this->Html->link(__d('users', 'Terms of Service'), array('controller' => '/', 'action' => 'terms-and-conditions')) ."\n";
                    echo $this->Form->input('tos', array(
                            'type'=>'checkbox',
                            'label'=> __d('users', 'I have read and agreed to ') . $tosLink,
                            'value' => '0',
                            'checked' => false),
                        array(
                            'required' => 'required',
                            'ng-model' => 'User.tos',
                        )
                    );
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-7">
                <?php echo $this->Form->button('Register Now', array('type' => 'submit', 'class' => 'btn button_example', 'ng-disabled' => 'false'));?>
            </div>
        </div>
        <?php
        echo $this->Form->end();
        ?>
    </div>
</div>