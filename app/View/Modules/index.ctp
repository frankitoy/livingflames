<?php
$this->Html->addCrumb('Modules');
echo $this->element('Users.Users/sidebar');
echo $this->element('crumbs');
?>
<div class="table-responsive">
    <?php echo $this->element('search_form'); ?>
    <br><br>
    <br><br>
    <table class="table table-hover table-striped table-bordered table-condensed">
        <thead>
        <tr>
            <!--<th width="10%"><?php echo $this->Paginator->sort('id','Unique Id'); ?></th>-->
            <th width="30%"><?php echo $this->Paginator->sort('name','Name'); ?></th>
            <th width="30%"><?php echo $this->Paginator->sort('description','Title'); ?></th>
            <th width="20%"><?php echo $this->Paginator->sort('slug','Url'); ?></th>
            <th width="10%"><?php echo $this->Paginator->sort('is_active','Active'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($modules as $module):
            ?>
            <tr>
                <!--<td><a href=""><?php echo $module['Modules']['id'];?></a></td>-->
                <td><a href="<?php echo $this->Html->url(array("controller" => "modules" ,'action' => 'info',$module['Modules']['slug']));?>"><?php echo $module['Modules']['name'];?></a></td>
                <td><?php echo $module['Modules']['description'];?></td>
                <td><a href="<?php echo str_replace("/index", "",$this->Html->url(array("controller" => "module" ,$module['Modules']['module_slug_id'])));?>"><?php echo str_replace("/index", "",$this->Html->url(array("controller" => "module" ,$module['Modules']['module_slug_id'])));?></a></td>
                <td><?php echo ($module['Modules']['is_active'] > 0) ? 'YES' : 'NO';?></td>
            </tr>
        <?php endforeach; ?>
        <tr><th colspan="5" style="text-align: center;">
                <?php
                echo $this->Paginator->counter(array(
                    'format' => __d('users', 'Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%')
                ));
                //echo $this->element('Users.pagination');
                ?></th></tr>
        </tbody>
    </table>
</div>