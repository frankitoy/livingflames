<?php
$this->Html->addCrumb('Modules', '/modules');
$this->Html->addCrumb($module['Modules']['name'] . '&nbsp;' . $module['Modules']['description']);
echo $this->element('Users.Users/sidebar');
echo $this->element('crumbs');
?>
<div class="table-responsive">
    <div class="row-fluid" ng-controller="formController">
        <div class="col-md-10 col-md-offset-2 add-padding">
            <div class="col-md-12">
            <h3><?php echo $module['Modules']['name'] . '&nbsp;' . $module['Modules']['description'];?></h3>
            <div>Choices highlighted as <span class="is_answer">like this</span> are correct answers.</div>
            </div>
            <?php
            $i = 1;
            foreach ($assessments as $assessment) {
            ?>
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-sm-12"><h4><?php echo $i. '. '. $assessment['Assessments']['question'];?></h4></div>
                    <div class="col-sm-12">
                        <?php
                        if (count($assessment['AssessmentsChoices']) > 0) {
                            foreach ($assessment['AssessmentsChoices'] as $key => $choices) {

                                $selected = '';
                                $css = '';
                                if ($choices['is_correct_answer'] == 1) {
                                    $selected = 'checked';
                                    $css = ' is_answer';
                                }
                                ?>
                                <div class="radio col-md-offset-1<?php echo $css;?>">
                                    <?php
                                    echo $this->Form->radio('UsersAssessmentsChoicesAnswers.assessments_choices_id.' . $assessment['Assessments']['id'], array(
                                        'label' =>  $choices['answer'],
                                    ), array(
                                        'id' => $choices['id'],
                                        'hiddenField' => false,
                                        'value' => $choices['id'],
                                        'checked' => $selected,
                                        'required' => true,
                                        'security' => false,
                                        'disabled' => 'disabled'
                                    ));
                                    ?>
                                </div>
                            <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php
                $i++;
            }
            ?>
        </div>
    </div>
</div>