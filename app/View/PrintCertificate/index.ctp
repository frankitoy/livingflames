<?php
/**
 * Document information
 */
$this->Pdf->pdf->SetCreator(PDF_CREATOR);
$this->Pdf->pdf->SetAuthor(Configure::read('Author'));
$this->Pdf->pdf->SetTitle($user['Modules']['description']);
$this->Pdf->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);// set default monospaced font
$this->Pdf->pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
/**
 * set margins
 *
 */
$this->Pdf->pdf->SetMargins(PDF_MARGIN_LEFT, 18, PDF_MARGIN_RIGHT);
$this->Pdf->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);// set auto page breaks
$this->Pdf->pdf->AddPage();// add a page

$app = APP;
$name = ucfirst(strtolower($user['User']['first_name'])) . '&nbsp;' . ucfirst(strtolower($user['User']['last_name']));
$description = $user['Modules']['description'];
$dateOfCompletion = date("F Y");
$html = $str = <<<EOD
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Certificate - Living Flame</title>
<style type="text/css">
html {
	height: 100%;
	margin: 0;
}

body {
	padding: 0;
	margin: 0;
	color: #666666;
	font-family: 'Noto Sans', sans-serif;
	font-size:13px;
	line-height:18px;
}

#wrap {
	width:100%;
	height:auto;
	margin: 0 auto;
}

#top {
	width: 100%;
	height: 360px;
	background: #333 url('$app/webroot/img/logo.png') center center no-repeat;
	border-bottom:5px solid #e14e17;
}

#topmodule {
	width: 100%;
	height: 160px;
	background: #333 url('$app/webroot/img/logosmall.png') right no-repeat;
	border-bottom:5px solid #e14e17;
}

#content {
	width:50%;
	height:auto;
	margin:80px auto;
	padding-bottom:30px;
}

h1 {
	font-size:40px;
	font-weight:bold;
	color: #e14e17;
	border-bottom:1px solid #ccc;
	padding-bottom:30px;
	text-align:center;
}

h2 {
	font-size:30px;
	color: #e14e17;
	line-height:30px;
	font-weight:bold;
	text-align:center;
}

#moduleheader {
	font-size:30px;
	font-weight:bold;
	color: #e14e17;
	padding:110px 0px 0px 20px;
}

li {
	font-size:20px;
	line-height:30px;
	color: #333;
}

p {
	font-size:20px;
	line-height:30px;
	color: #333;
	font-family: "Noto Sans", sans-serif;
}

hr {
    border:1px solid #eeeeee;
}

.nextbtn {
	padding:20px 0px 0px 16px;
}

#button {
	margin:30px auto;
	height:auto;
}

.button_example{
    border:1px solid #AE3C12 !important;
    -webkit-border-radius: 8px !important;
    -moz-border-radius: 8px !important;
    border-radius: 8px !important;
    font-size:18px !important;
    font-family:helvetica, sans-serif !important;
    padding: 10px 20px 10px 20px !important;
    text-decoration:none !important;
    display:inline-block !important;
    color: #FFFFFF !important;
    background-color: #E14E17 !important;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#E14E17), to(#E65B24)) !important;
    background-image: -webkit-linear-gradient(top, #E14E17, #E65B24) !important;
    background-image: -moz-linear-gradient(top, #E14E17, #E65B24) !important;
    background-image: -ms-linear-gradient(top, #E14E17, #E65B24) !important;
    background-image: -o-linear-gradient(top, #E14E17, #E65B24) !important;
    background-image: linear-gradient(to bottom, #E14E17, #E65B24);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#E14E17, endColorstr=#E65B24) !important;
}

.button_example:hover{
    border:1px solid #AE3C12 !important;
    background-color: #EC6D23 !important;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#EC6D23), to(#EC8A33)) !important;
    background-image: -webkit-linear-gradient(top, #EC6D23, #EC8A33) !important;
    background-image: -moz-linear-gradient(top, #EC6D23, #EC8A33) !important;
    background-image: -ms-linear-gradient(top, #EC6D23, #EC8A33) !important;
    background-image: -o-linear-gradient(top, #EC6D23, #EC8A33) !important;
    background-image: linear-gradient(to bottom, #EC6D23, #EC8A33);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#EC6D23, endColorstr=#EC8A33) !important;
}

#footer{
	width: 100%;
	margin: 0 !important;
	background:#eeeeee;
    position: fixed !important;
    bottom: 0 !important;

}

.base{
	color: #999999;
	font-size:11px;
	text-align:center;
}

#base a {
	color:#866801;
	text-decoration:none;
}

#base a:hover {
	color:#433608;
	text-decoration:none;
}

a {
	color: #09618a;
	text-decoration:none;
}

a:hover{
	color: #ffffff;
	text-decoration:none;
}

#certificate{
	border: 5px solid #e7783f;
	padding:1px;
	margin:30px auto;
}

.ch1{
	font-size:35px;
	line-height:40px;
	font-weight:bold;
}

.ch2 {
	font-size:35px;
	line-height:40px;
	font-weight:bold;
	color:#e7783f;
}

.cp {
	font-size:18px;
	margin-bottom:10px
}

.col-sm-2 {
    width: 22.667% !important;
}

.col-sm-offset-2 {
    margin-left: 22.6667% !important;
}

.form-horizontal .control-label {
    cursor: pointer !important;
}

.form-horizontal input.ng-invalid.ng-dirty, textarea.ng-invalid.ng-dirty {
    border-color: #FA787E !important;
}

.form-horizontal input.ng-valid.ng-dirty, textarea.ng-valid.ng-dirty {
    border-color: #78FA89 !important;
}

.help-inline {
    display: inline-block;
    vertical-align: middle;
    color: #FA787E;
}

.add-padding {
    margin-top: 20px;
}

.padding-left-10px {
    padding-left: 0px !important;
}

.alert {
    margin-top: 10px !important;
}

.navbar-static-top {
    margin-bottom: 19px;
}

.div-form-signin {
    padding-top: 40px !important;
    padding-bottom: 40px !important;
    color: #333333 !important;
}

.container {
    width: 100% !important;
    padding-left: 0 !important;
    padding-right: 0 !important;
    height: auto !important;
}

.form-signin {
    max-width: 330px;
    padding: 15px;
    margin: 0 auto;
    color: #333333 !important;
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
    margin-bottom: 10px;
}
.form-signin .checkbox {
    font-weight: normal;
}
.form-signin .form-control {
    position: relative;
    font-size: 16px;
    height: auto;
    padding: 10px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.form-signin .form-control:focus {
    z-index: 2;
}
.form-signin input[type="text"] {
    margin-bottom: -1px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
}
.form-signin input[type="password"] {
    margin-bottom: 10px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
}
.form-signin-heading {
    color: #333333 !important;
    text-align: left !important;
}

.message {
    opacity: 1;
    background-color: #F2DEDE !important;
    border-color: #EBCCD1 !important;
    color: #A94442 !important;
    border: 1px solid !important;
    border-radius: 4px !important;
    margin-bottom: 20px !important;
    padding: 15px !important;
    transition: opacity 0.15s linear 0s;
    width: 100%;
}

.navbar-brand {
    margin-left: 10px !important;
}

.navbar-nav-logout{
    margin-right: 10px !important;
}

.row {
    margin-left: 0;
    margin-right: 0;
}

.table-responsive {
    background-color: #FFFFFF !important;
}

a:hover {
    color: #000000 !important;
}

.font-white {
    color: #FFFFFF !important;
}

.col-md-offset-1 {
    margin-left: 3.333% !important;
}

.radio label{
    font-size: 15px !important;
}
</style>
</head>
<body>
    <table width="495px" border="0" align="center" cellpadding="0" cellspacing="0" id="certificate">
      <tr>
        <td>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"  style="text-align: center; border:3px solid #f5bd5a;">
                <tr><td bgcolor="#333333" style="padding:20px 0px; border:1px solid #FFF;"><p><img src="$app/webroot/img/logo.png" width="233" height="172" /></p></td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td valign="middle"><img style="padding-top:60px;" src="$app/webroot/img/certificate.png" width="360" height="62" /></td></tr>
                <tr><td><p class="cp" style="padding:30px 40px;">This is to certify that in $dateOfCompletion</p>
                      <p class="ch1">$name</p>
                      <p class="cp">has successfully completed the</p>
                      <p class="ch2">$description<br>Course Module</p>
                      <p class="cp">conducted by<br /><strong>Living Flame Ltd</strong></p>
                </td></tr>
                <tr><td>&nbsp;</td></tr>
            </table>
        </td>
      </tr>
    </table>
</body>
</html>
EOD;
// output the HTML content
$this->Pdf->pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$this->Pdf->pdf->lastPage();
//Close and output PDF document
$this->Pdf->pdf->Output($token . '.pdf', 'D');
die;
exit;