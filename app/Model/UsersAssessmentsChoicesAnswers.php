<?php
/**
 * Created by IntelliJ IDEA.
 * User: frankitoy
 * Date: 21/01/14
 * Time: 1:48 PM
 * To change this template use File | Settings | File Templates.
 */

App::uses('AppModel', 'Model');

class UsersAssessmentsChoicesAnswers extends AppModel {

    public $name = 'UsersAssessmentsChoicesAnswers';

    public $actsAs = array('Containable');

    public $cacheQueries = true;

    public $belongsTo = array(
        'Modules' => array(
            'className' => 'Modules',
            'type' => 'INNER',
            'conditions' => array(
                'Modules.is_active' => 1,
            )
        ),
        'User' => array(
            'className' => 'User',
            'type' => 'INNER',
            'foreignKey' => 'users_id',
            'dependent' => true
        ),
        'AssessmentsChoices' => array(
            'type' => 'INNER',
            'foreignKey' => 'assessments_choices_id',
            'dependent' => true
        ),
        'Assessments' => array(
            'className' => 'Assessments',
            'type' => 'INNER',
            'dependent' => true

        )
    );

    /**
     * Validation parameters
     *
     * @var array
     */
    public $validate = array(
        'assessments_choices_id' => array(
            'rule'     => 'numeric',
            'required' => true,
            'allowEmpty' => false
        )
    );
}