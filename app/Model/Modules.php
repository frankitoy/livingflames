<?php
/**
 * Created by IntelliJ IDEA.
 * User: frankitoy
 * Date: 9/01/14
 * Time: 1:55 PM
 * To change this template use File | Settings | File Templates.
 */

App::uses('AppModel', 'Model');

class Modules extends AppModel {

    public $name = 'Modules';

    public $actsAs = array('Containable');

    public $cacheQueries = true;

    public $hasMany   = array(
        'Assessments' => array(
            'type' => 'INNER',
            'foreignKey' => 'modules_id',
            'conditions' => array(
                'Assessments.is_active' => 1
            ),
            'dependent' => true
        ),
        'User' => array(
            'type' => 'INNER',
            'foreignKey' => 'modules_id',
            'conditions' => array(
                'User.active' => 1,
                'User.email_verified' => 1,
                'User.has_paid' => 1,
                'User.is_admin' => 0
            ),
            'dependent' => true
        )
    );
}