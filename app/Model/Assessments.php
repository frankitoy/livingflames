<?php
/**
 * Created by IntelliJ IDEA.
 * User: frankitoy
 * Date: 9/01/14
 * Time: 1:55 PM
 * To change this template use File | Settings | File Templates.
 */

App::uses('AppModel', 'Model');

class Assessments extends AppModel {

    public $name = 'Assessments';

    public $actsAs = array('Containable');

    public $cacheQueries = true;

    public $hasMany   = array(
        'AssessmentsChoices' => array(
            'type' => 'INNER',
            'foreignKey' => 'assessments_id',
            'conditions' => array(
                'AssessmentsChoices.is_active' => 1
            ),
            'order' => array( 'AssessmentsChoices.sort_order' => 'ASC'),
            'dependent' => true
        )
    );

    public $belongsTo = array(
        'Modules' => array(
            'className' => 'Modules',
            'type' => 'INNER',
            'conditions' => array(
                'Modules.is_active' => 1,
                'Assessments.is_active' => 1
            )
        )
    );
}