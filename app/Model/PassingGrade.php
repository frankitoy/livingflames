<?php
/**
 * Created by IntelliJ IDEA.
 * User: frankitoy
 * Date: 2/02/14
 * Time: 1:15 PM
 * To change this template use File | Settings | File Templates.
 */


class PassingGrade extends AppModel {

    public $name = 'PassingGrade';

    public $table = 'passing_grade';

    public $actsAs = array('Containable');

    public $cacheQueries = true;
}