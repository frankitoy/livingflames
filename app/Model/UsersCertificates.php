<?php
/**
 * Created by IntelliJ IDEA.
 * User: frankitoy
 * Date: 21/01/14
 * Time: 1:48 PM
 * To change this template use File | Settings | File Templates.
 */
App::uses('AppModel', 'Model');

class UsersCertificates extends AppModel {

    public $name = 'UsersCertificates';

    public $actsAs = array('Containable');

    public $cacheQueries = true;

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'type' => 'INNER',
            'foreignKey' => 'users_id',
            'dependent' => true
        ),
    );
}