<?php
/**
 * Created by IntelliJ IDEA.
 * User: frankitoy
 * Date: 2/02/14
 * Time: 11:20 PM
 * To change this template use File | Settings | File Templates.
 */
App::uses('AppModel', 'Model');

class UsersSurveyedComments extends AppModel {

    public $name = 'UsersSurveyedComments';

    public $actsAs = array('Containable');

    public $cacheQueries = true;

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'type' => 'INNER',
            'foreignKey' => 'users_id',
            'dependent' => true
        ),
        'Modules' => array(
            'className' => 'Modules',
            'type' => 'INNER',
            'conditions' => array(
               'Modules.is_active' => 1,
            )
        ),
    );
}