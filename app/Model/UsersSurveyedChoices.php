<?php
/**
 * Created by IntelliJ IDEA.
 * User: frankitoy
 * Date: 2/02/14
 * Time: 11:20 PM
 * To change this template use File | Settings | File Templates.
 */
App::uses('AppModel', 'Model');

class UsersSurveyedChoices extends AppModel {

    public $name = 'UsersSurveyedChoices';

    public $actsAs = array('Containable');

    public $cacheQueries = true;

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'type' => 'INNER',
            'foreignKey' => 'users_id',
            'dependent' => true
        ),
        'Surveys' => array(
            'className' => 'Surveys',
            'type' => 'INNER',
            'foreignKey' => 'surveys_id',
            'dependent' => true
        ),
        'SurveysChoices' => array(
            'className' => 'SurveysChoices',
            'type' => 'INNER',
            'dependent' => true
        ),
    );
}