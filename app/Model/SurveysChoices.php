<?php
/**
 * Created by IntelliJ IDEA.
 * User: frankitoy
 * Date: 9/01/14
 * Time: 1:55 PM
 * To change this template use File | Settings | File Templates.
 */

App::uses('AppModel', 'Model');

class SurveysChoices extends AppModel {

    public $name = 'SurveysChoices';

    public $actsAs = array('Containable');

    public $cacheQueries = true;
    /*
    public $hasMany   = array(
        'UsersSurveyedChoices' => array(
            'type' => 'INNER',
            'foreignKey' => 'surveys_choices_id',
            'dependent' => true
        )
    );
    */
}