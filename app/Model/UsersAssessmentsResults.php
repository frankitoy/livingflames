<?php
/**
 * Created by IntelliJ IDEA.
 * User: frankitoy
 * Date: 21/01/14
 * Time: 1:48 PM
 * To change this template use File | Settings | File Templates.
 */

App::uses('AppModel', 'Model');

class UsersAssessmentsResults extends AppModel {

    public $name = 'UsersAssessmentsResults';

    public $actsAs = array('Containable');

    public $cacheQueries = true;

    public $belongsTo = array(
        'Modules' => array(
            'className' => 'Modules',
            'type' => 'INNER',
            'conditions' => array(
                'Modules.is_active' => 1,
            )
        ),
        'User' => array(
            'className' => 'User',
            'type' => 'INNER',
            'foreignKey' => 'users_id',
            'dependent' => true
        ),
    );
}