<?php
/**
 * Created by IntelliJ IDEA.
 * User: frankitoy
 * Date: 9/01/14
 * Time: 1:55 PM
 * To change this template use File | Settings | File Templates.
 */

App::uses('AppModel', 'Model');

class AssessmentsChoices extends AppModel {

    public $name = 'AssessmentsChoices';

    public $actsAs = array('Containable');

    public $cacheQueries = true;

    public $hasMany   = array(
        'UserAssessmentsChoicesAnswers' => array(
            'type' => 'INNER',
            'dependent' => true
        )
    );

    public $belongsTo = array(
        'Assessments' => array(
            'className' => 'Assessments',
            'type' => 'INNER',
            'conditions' => array(
                'AssessmentsChoices.is_active' => 1,
            )
        )
    );
}