<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
Router::connect('/', array('controller' => 'pages', 'action' => 'index'));

Router::connect('/terms-and-conditions*', array('controller' => 'pages', 'action' => 'display', 1));

Router::connect('/terms-and-conditions/*', array('controller' => 'pages', 'action' => 'display', 1));

Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

Router::connect('/module/*', array('controller' => 'pages', 'action' => 'display'));

Router::connect('/registration/*', array('controller' => 'registration', 'action' => 'index'));

Router::connect('/start-assessment/*', array('controller' => 'assessments', 'action' => 'start'));

Router::connect('/assessments*', array('controller' => 'assessments', 'action' => 'index'));

Router::connect('/assessments/*', array('controller' => 'assessments', 'action' => 'index'));

Router::connect('/surveys', array('controller' => 'surveys', 'action' => 'index'));

Router::connect('/results*', array('controller' => 'assessments', 'action' => 'results'));

Router::connect('/results/*', array('controller' => 'assessments', 'action' => 'results'));

Router::connect('/take-a-survey*', array('controller' => 'surveys', 'action' => 'survey'));

Router::connect('/take-a-survey/*', array('controller' => 'surveys', 'action' => 'survey'));

Router::connect('/download/*', array('controller' => 'download', 'action' => 'pdf'));

Router::connect('/download-result/*', array('controller' => 'assessments', 'action' => 'download'));

Router::connect('/print-certificate*', array('controller' => 'print_certificate', 'action' => 'index'));

Router::connect('/print-certificate/*', array('controller' => 'print_certificate', 'action' => 'index'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
