<?php if ($this->Session->read('Auth.User.id')) : ?>
<style type="text/css">
    .container {
        margin-top: 0px !important;

    }
</style>
<div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo $this->Html->url(array("controller" => "users" ,'action' => ''));?>">Living Flames</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li <?php echo (strtolower($model)==='user') ? 'class="active"':'';?>><a href="<?php echo str_replace("/index","",$this->Html->url(array("controller" => "users" ,'action' => 'index')));?>">Registrations</a></li>
                <li class="dropdown<?php echo (strtolower($model)==='modules') ? ' active':'';?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Modules<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <!--<li><a href="<?php echo str_replace("/users","",$this->Html->url(array("controller" => "modules" ,'action' => 'add')));?>">Add</a></li>-->
                        <li><a href="<?php echo str_replace("/users","",$this->Html->url(array("controller" => "modules" ,'action' => 'index')));?>">List</a></li>
                    </ul>
                </li>
                <li class="dropdown<?php echo (strtolower($model)==='surveys' || strtolower($model)==='survey') ? ' active ':'';?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Surveys<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo str_replace("/users","",$this->Html->url(array("controller" => "surveys" ,'action' => 'index')));?>">List</a></li>
                        <li><a href="<?php echo str_replace("/users","",$this->Html->url(array("controller" => "surveys" ,'action' => 'graph')));?>">Graph</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right navbar-nav-logout">
                <li><?php echo $this->Html->link(__d('users', 'Logout'), array('controller' => 'users','action' => 'logout')); ?></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>
<?php endif; ?>