<?php
/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<div class="div-form-signin">
    <?php
    echo $this->Form->create($model, array('action' => 'login','id' => 'LoginForm','class' => 'form-signin', 'role' => 'form'));
    ?>
        <?php echo $this->Session->flash('auth');?>
        <h2 class="form-signin-heading">Please sign in</h2>
        <?php  echo str_replace('type="text"','type="email"',$this->Form->text('email', array(
        'class' => 'form-control',
        'required' => 'required',
        'autofocus' => true,
        'placeholder' => 'Email address')));?>
        <?php  echo str_replace('type="text"','type="password"',$this->Form->text('password', array(
        'class' => 'form-control',
        'required' => 'required',
        'placeholder' => 'Password')));?>
        <?php
        echo $this->Form->input('remember_me', array('type' => 'checkbox', 'label' =>  __d('users', 'Remember Me')));
        echo $this->Form->hidden('User.return_to', array('value' => $return_to));
        ?>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    <?php
    echo $this->Form->end();
    ?>
</div>