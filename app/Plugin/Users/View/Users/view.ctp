<?php
$this->Html->addCrumb('Registrations', '/users');
$this->Html->addCrumb(ucfirst($user['User']['first_name']) . '&nbsp;' . ucfirst($user['User']['last_name']));
echo $this->element('Users.Users/sidebar');
echo $this->element('crumbs');
?>
<div class="container">
    <div class="row">
        <?php echo $this->Form->create($model);
        echo $this->Form->hidden('id', array('value' => $user['User']['id']));
        echo $this->Form->hidden('has_paid', array('value' => 1));
        echo $this->Form->hidden('date_paid', array('value' => date("Y-m-d H:i:s")));
        echo $this->Form->hidden('assessment_link_sent', array('value' => 1));
        $assessmentLink = str_replace(array("/users"),array(""),$this->Html->url(array("controller" => "assessments")));
        $assessmentLink .= '?token=' . $user['User']['id'];
        //$assessmentLink = Router::url('/', true) . $assessmentLink;
        ?>
        <div class="form-group">
            <div class="col-md-10 col-md-push-2"><a href="<?php echo str_replace("/users","",$this->Html->url(array("controller" => "modules", 'action' => 'info', $user['Modules']['slug'])));?>"><?php echo $user['Modules']['name'];?>: <?php echo $user['Modules']['description'];?></a></div>
            <div class="col-md-2 col-md-pull-9"><strong>Module</strong></div>
        </div>
        <?php if ($user['User']['has_paid'] > 0){;?>
        <div class="form-group">
            <div class="col-md-10 col-md-push-2"><a href="<?php echo $assessmentLink;?>" target="_blank"><?php echo $assessmentLink;?></a></div>
            <div class="col-md-2 col-md-pull-9"><strong>Assessment Link</strong></div>
        </div>
        <div class="form-group">
            <div class="col-md-10 col-md-push-2"><?php echo (intval($user['User']['assessment_score'])>0) ? intval($user['User']['assessment_score']) :  '';?></div>
            <div class="col-md-2 col-md-pull-9"><strong>Assessment Score</strong></div>
        </div>
        <?php 
        if (sizeof($user['UsersCertificates']) > 0) {
        ?>    
        <div class="form-group">
            <div class="col-md-10 col-md-push-2"><a href="<?php echo 
            str_replace(array("/users"),array(""),$this->Html->url(array("controller" => "print-certificate")));?>?token=<?php echo $user['User']['id'];?>" target="_blank">Download</a></div>
            <div class="col-md-2 col-md-pull-9"><strong>Certificate</strong></div>
        </div>
        <?php
        }
        ?>

        <?php }?>
        <div class="form-group">
            <div class="col-md-10 col-md-push-2"><?php echo $user['User']['id'];?></div>
            <div class="col-md-2 col-md-pull-9"><strong>Id</strong></div>
        </div>
        <div class="form-group">
            <div class="col-md-10 col-md-push-2"><?php echo ucfirst($user['User']['first_name']);?>&nbsp;<?php echo ucfirst($user['User']['last_name']);?></div>
            <div class="col-md-2 col-md-pull-9"><strong>Full Name</strong></div>
        </div>
        <div class="form-group">
            <div class="col-md-10 col-md-push-2"><?php echo $user['User']['company_name'];?></div>
            <div class="col-md-2 col-md-pull-9"><strong>Company</strong></div>
        </div>
        <div class="form-group">
            <div class="col-md-10 col-md-push-2"><a href="mailto:<?php echo $user['User']['email'];?>"><?php echo $user['User']['email'];?></a></div>
            <div class="col-md-2 col-md-pull-9"><strong>Email</strong></div>
        </div>
        <div class="form-group">
            <div class="col-md-10 col-md-push-2"><?php echo $user['User']['address'];?></a></div>
            <div class="col-md-2 col-md-pull-9"><strong>Address</strong></div>
        </div>
        <div class="form-group">
            <div class="col-md-10 col-md-push-2"><?php echo $user['User']['registration_number'];?></a></div>
            <div class="col-md-2 col-md-pull-9"><strong>Registration Number</strong></div>
        </div>
        <div class="form-group">
            <div class="col-md-10 col-md-push-2"><?php echo $user['User']['mobile'];?></div>
            <div class="col-md-2 col-md-pull-9"><strong>Mobile</strong></div>
        </div>
        <div class="form-group">
            <div class="col-md-10 col-md-push-2"><?php echo $user['User']['phone'];?></div>
            <div class="col-md-2 col-md-pull-9"><strong>Phone</strong></div>
        </div>
        <div class="form-group">
            <div class="col-md-10 col-md-push-2"><?php echo date("jS-M-y H:i",strtotime($user['User']['created']));?></div>
            <div class="col-md-2 col-md-pull-9"><strong>Date Registered</strong></div>
        </div>
        <div class="form-group">
            <div class="col-md-10 col-md-push-2"><?php echo ($user['User']['has_paid'] > 0) ? date("jS-M-y H:i",strtotime($user['User']['date_paid'])) : '-';?></div>
            <div class="col-md-2 col-md-pull-9"><strong>Date Paid</strong></div>
        </div>
        <?php
        if ($user['User']['has_paid'] < 1) {
        ?>
        <div class="clearfix"></div>
        <div class="form-group" style="margin-top: 30px">
            <div class="col-sm-1 col-sm-offset-admin">
                <?php echo $this->Form->button('Set Registration As Paid', array('type' => 'submit', 'class' => 'btn button_example'));?>
            </div>
        </div>
        <?php
        }
        echo $this->Form->end();
        ?>
    </div>
</div>
