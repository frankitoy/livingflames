<?php
$this->Html->addCrumb('Registrations', '/users');
$this->Html->addCrumb('Search results for '. $searchTerm);
echo $this->element('Users.Users/sidebar');
echo $this->element('crumbs');
?>
<div class="table-responsive">
    <?php
    $message =  $this->Session->flash('good');
    if ($message) {
        echo $message . '<br><br><br><br>';
    }
    echo $this->element('Users.Users/search_form');
    ?>
    <br><br>
    <br><br>
    <center><h3>Search results for <?php echo $searchTerm;?></h3></center>
    <table class="table table-hover table-striped table-bordered table-condensed">
        <thead>
        <tr>
            <th rowspan="2" width="6%"><?php echo $this->Paginator->sort('id','Unique Id'); ?></th>
            <th rowspan="2" width="14%"><?php echo $this->Paginator->sort('Modules.description','Module'); ?></th>
            <th rowspan="2" width="10%"><?php echo $this->Paginator->sort('first_name','Name'); ?></th>
            <th rowspan="2" width="10%"><?php echo $this->Paginator->sort('email','Email'); ?></th>
            <th rowspan="2" width="10%"><?php echo $this->Paginator->sort('company_name','Company'); ?></th>
            <th rowspan="2" width="10%"><?php echo $this->Paginator->sort('registration_number','Registration No'); ?></th>
            <th rowspan="2" width="10%"><?php echo $this->Paginator->sort('mobile','Mobile'); ?>/<?php echo $this->Paginator->sort('phone','Phone'); ?></th>
            <th rowspan="2" width="3%"><?php echo $this->Paginator->sort('has_paid','Paid'); ?></th>
            <th colspan="2" style="text-align:center">Date</th>
        </tr>
        <tr><th>Registered</th><th>Paid</th></tr>
        </thead>
        <tbody>
        <?php
        foreach ($users as $user):
            ?>
            <tr>
                <td><a href="<?php echo $this->Html->url(array("controller" => "users" ,'action' => 'info',$user[$model]['id']));?>"><?php echo substr($user[$model]['id'],0,8). '...'; ?></a></td>
                <td><?php echo $user['Modules']['description']; ?></td>
                <td><?php echo ucfirst($user[$model]['first_name']) . '&nbsp;' . ucfirst($user[$model]['last_name']); ?></td>
                <td><a href="mailto:<?php echo $user[$model]['email'];?>" target="_top"><?php echo $user[$model]['email'];?></a></td>
                <td><?php echo $user[$model]['company_name'];?></td>
                <td><?php echo $user[$model]['registration_number'];?></td>
                <td><?php echo $user[$model]['mobile'];?>/<?php echo $user[$model]['phone'];?></td>
                <td><?php echo ($user[$model]['has_paid'] > 0 ) ? 'Yes' : 'No';?></td>
                <td><?php echo date("jS-M-y H:i",strtotime($user[$model]['created']));?></td>
                <td><?php if($user[$model]['has_paid']){echo date("jS-M-y H:i",strtotime($user[$model]['date_paid']));}else{ echo '-';};?></td>
            </tr>
        <?php endforeach; ?>
        <tr><th colspan="10" style="text-align: center;">
                <?php
                echo $this->Paginator->counter(array(
                    'format' => __d('users', 'Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%')
                ));
                echo $this->element('Users.pagination');
                ?></th></tr>
        </tbody>
    </table>
</div>